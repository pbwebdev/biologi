<?php

/**
 * Plugin Name: Biologi
 * Plugin URI:  https://www.biologi.com.au/
 * Author:      Gene Alyson Fortunado Torcende
 * Author URI:  mailto:genealyson.torcende@gmail.com
 * Description: Biologi functionalities
 * Version:     0.1.0
 * License:     Apache License 2.0
 * License URI: http://www.apache.org/licenses/
 *
 * @package ThemePlate
 * @since 0.1.0
 */

// Accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/* ==================================================
Global constants
================================================== */

if ( ! defined( 'BIOLOGI_FILE' ) ) {
	define( 'BIOLOGI_FILE', __FILE__ );
}

if ( ! defined( 'BIOLOGI_URL' ) ) {
	define( 'BIOLOGI_URL', plugin_dir_url( BIOLOGI_FILE ) );
}

if ( ! defined( 'BIOLOGI_PATH' ) ) {
	define( 'BIOLOGI_PATH', plugin_dir_path( BIOLOGI_FILE ) );
}

// Load the main plugin class
require_once BIOLOGI_PATH . 'class-' . basename( BIOLOGI_FILE );

// Instantiate
Biologi::init();

register_activation_hook( BIOLOGI_FILE, array( Biologi::class, 'add_roles' ) );
register_deactivation_hook( BIOLOGI_FILE, array( Biologi::class, 'remove_roles' ) );
