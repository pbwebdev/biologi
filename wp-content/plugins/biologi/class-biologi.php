<?php

/**
 * Biologi
 *
 * @package ThemePlate
 * @since 0.1.0
 */

class Biologi {

	private static $instance;

	const WHOLESALER = 'wholesale_customer';
	const INFLUENCER = array(
		'role' => 'biologi_influencer',
		'name' => 'Influencer',
	);


	public static function init() {

		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;

	}


	private function __construct() {

		add_action( 'init', function() {
			add_filter( 'wwof_variation_quantity_input_args', array( $this, 'wholesaler_quantity' ), PHP_INT_MAX, 2 );

			foreach ( wc_get_product_types() as $type => $label ) {
				if ( 'variable' !== $type && $label ) {
					add_filter( 'wwpp_before_save_' . $type . '_product_wholesale_minimum_order_quantity', array( $this, 'set_wholesaler_meta' ), PHP_INT_MAX, 3 );
					add_filter( 'wwpp_before_save_' . $type . '_product_wholesale_order_quantity_step', array( $this, 'unset_wholesaler_meta' ), PHP_INT_MAX, 3 );
				}
			}

			add_filter( 'wwpp_before_save_variable_level_wholesale_minimum_order_quantity', array( $this, 'set_wholesaler_meta' ), PHP_INT_MAX, 3 );
			add_filter( 'wwpp_before_save_variable_level_wholesale_order_quantity_step', array( $this, 'unset_wholesaler_meta' ), PHP_INT_MAX, 3 );
			add_filter( 'wwpp_filter_before_save_wholesale_minimum_order_quantity', array( $this, 'set_wholesaler_meta' ), PHP_INT_MAX, 3 );
			add_filter( 'wwpp_filter_before_save_wholesale_order_quantity_step', array( $this, 'unset_wholesaler_meta' ), PHP_INT_MAX, 3 );

			add_filter( 'wwpp_filter_set_product_quantity_value_to_minimum_order_quantity', array( $this, 'wholesaler_minimum_order' ), PHP_INT_MAX, 4 );

			add_filter( 'woocommerce_order_get_billing_company', array( $this, 'shortcircuit_for_xero' ), PHP_INT_MAX, 2 );
			add_filter( 'woocommerce_order_get_billing_first_name', array( $this, 'shortcircuit_for_xero' ), PHP_INT_MAX, 2 );
			add_filter( 'woocommerce_order_get_billing_last_name', array( $this, 'shortcircuit_for_xero' ), PHP_INT_MAX, 2 );
			add_filter( 'woocommerce_order_get_billing_email', array( $this, 'shortcircuit_for_xero' ), PHP_INT_MAX, 2 );
			add_filter( 'woocommerce_xero_line_item_product', array( $this, 'shortcircuit_for_xero_item' ), PHP_INT_MAX, 3 );
		}, PHP_INT_MAX );

	}


	public function wholesaler_quantity( $args, $product ) {

		if ( self::is_wholesaler() && $product->get_id() && ! $product->is_sold_individually() ) {
			foreach ( array( 'min_value', 'input_value' ) as $key ) {
				if ( empty( $args[ $key ] ) || 5 > $args[ $key ] ) {
					$args[ $key ] = 5;
				}
			}

			if ( $args['min_value'] > $args['input_value'] ) {
				$args['input_value'] = $args['min_value'];
			}
		}

		return $args;

	}


	public function set_wholesaler_meta( $value, $role, $product_id ) {

		if ( self::WHOLESALER !== $role ) {
			return $value;
		}

		$product = wc_get_product( $product_id );

		if ( ! $value && ! $product->is_sold_individually() ) {
			$value = 5;
		}

		if ( $value && $product->is_sold_individually() ) {
			$value = '';
		}

		return $value;

	}


	public function wholesaler_minimum_order( $filtered_args, $args, $product, $user_roles ) {

		if ( $product->is_sold_individually() ) {
			$filtered_args['input_value'] = 1;
			$filtered_args['min_value']   = 1;
			$filtered_args['max_value']   = 1;
			$filtered_args['step']        = 1;

			return $filtered_args;
		}

		if ( in_array( $product->get_type(), array( 'variable', 'variation' ), true ) || ! in_array( self::WHOLESALER, $user_roles, true ) ) {
			return $filtered_args;
		}

		global $wp_current_filter;

		if ( 'woocommerce_quantity_input_args' !== $wp_current_filter[ count( $wp_current_filter ) - 2 ] ) {
			return;
		}

		$min_value = get_post_meta( $product->get_id(), self::WHOLESALER . '_wholesale_minimum_order_quantity', true );
		$step = get_post_meta( $product->get_id(), self::WHOLESALER . '_wholesale_order_quantity_step', true );

		$filtered_args['min_value'] = $min_value ? $min_value : $args['min_value'];
		$filtered_args['step']      = $step ? $step : ( isset( $args['step'] ) ? $args['step'] : 1 );

		return $filtered_args;

	}


	public function unset_wholesaler_meta( $value, $role, $product_id ) {

		$product = wc_get_product( $product_id );

		if ( $value && self::WHOLESALER === $role && $product->is_sold_individually() ) {
			$value = '';
		}

		return $value;

	}


	public function shortcircuit_for_xero( $value, $object ) {

		global $wp_current_filter;

		$filters = array(
			'wc_xero_send_payment',
			'woocommerce_order_status_processing',
			'woocommerce_payment_complete',
			'woocommerce_order_status_completed',
			'woocommerce_checkout_order_processed',
			'woocommerce_order_action_xero_manual_invoice',
			'woocommerce_order_action_xero_manual_payment',
		);

		if ( ! in_array( $wp_current_filter[ count( $wp_current_filter ) - 2 ], $filters, true ) ) {
			return $value;
		}

		$classes = array(
			'WC_XR_Contact_Manager',
			'WC_XR_Invoice_Manager',
		);

		if ( empty( array_intersect( array_column( debug_backtrace( 2 ), 'class' ), $classes ) ) ) {
			return $value;
		}

		if ( self::is_wholesaler( $object->get_user() ) ) {
			return $value;
		}

		$values = array(
			'company'    => 'Biologi',
			'first_name' => 'Generic',
			'last_name'  => 'Customer',
			'email'      => 'enquiries@biologi.com.au',
		);

		return $values[ str_replace( 'woocommerce_order_get_billing_', '', end( $wp_current_filter) ) ];

	}


	public function shortcircuit_for_xero_item( $line_item, $item, $order ) {

		if ( self::is_wholesaler( $order->get_user() ) ) {
			$line_item->set_account_code( 201 );
		}

		return $line_item;

	}


	public static function is_wholesaler( $user = null ) {

		if ( ! $user ) {
			$user = wp_get_current_user();
		}

		return in_array( self::WHOLESALER, $user->roles, true );

	}


	public static function add_roles() {

		$capabilities = array(
			'read'    => true,
			'level_0' => true,
		);

		add_role( self::INFLUENCER['role'], self::INFLUENCER['name'], $capabilities );

	}


	public static function remove_roles() {

		remove_role( self::INFLUENCER['role'] );

	}

}
