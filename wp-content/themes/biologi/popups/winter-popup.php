<div id="winter-popup" class="popup" data-popup="popup-1">
	<div class="popup-mask"></div>
	<div class="popup-inner">
		<div class="popup-close">X</div>
		<div class="title">Take 20% Off</div>
		<div class="sub-title">with the code: biologibirthday</div>		
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/Biologi-logo.png"  class="site-logo" />
	</div>
</div>
<style>
#winter-popup.popup
{
	z-index:9999;
	background: none;
}
#winter-popup .popup-mask
{
	width: 100%;
    height: 100%;
    display: block;
    position: fixed;
    background: rgba(0,0,0,0.75);
    top: 0px;
    left: 0px;
}
#winter-popup .popup-close
{
    display: block;
    cursor: pointer;
    position: absolute;
    right: 5px;
    top: -5px;
    font-size: 35px;
}
#winter-popup .popup-inner .title
{
	font-family: trenda-bold !important;
    font-size: 55px;
    text-transform: uppercase;
    color: #fff;
}
#winter-popup .popup-inner .sub-title
{
	font-size: 24px;
    font-weight: 700;
    margin-top: 0px;
    color:#fff;	
}
#winter-popup .popup-inner
{
    max-width: 700px;
    width: 90%;
    padding: 200px 25px 25px 25px;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    background-color: #F4D9CE;
    text-align: center;
    background-image: url("/wp-content/themes/biologi/images/birthday-balloons.png");
    background-repeat: no-repeat;
    background-position: center top;	
}
#winter-popup .popup-inner .site-logo
{
	margin-top:60px;	
    width: 80px;
}	
@media only screen and (max-width: 480px) {
	#winter-popup .popup-inner {background-position: 10% top;}
	#winter-popup .popup-inner .site-logo {width:60px;}
	#winter-popup .popup-inner .title {font-size: 35px;}
	#winter-popup .popup-inner .sub-title {font-size: 16px;}
}
</style>

<script>
	function setCookie(name,value,days) {
        var expires;
        if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			expires = "; expires="+date.toGMTString();
        }
        else {
			expires = "";
		}
		document.cookie = name+"="+value+expires+"; path=/";
    }

	function getCookie(name) {
	    var nameEQ = name + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0;i < ca.length;i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1,c.length);
	        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	    }
	    return null;
	}
	
	function eraseCookie(name) {   
	    document.cookie = name+'=; Max-Age=-99999999;';  
	}

	var birthday_popup = getCookie('birthday_popup');
	if(birthday_popup != '1'){
		jQuery(window).on( "load", function() {
	        jQuery('#winter-popup').show();
	        setCookie('birthday_popup','1');		
	    });
	}

	jQuery('#winter-popup .popup-mask').click(function(){
		jQuery('#winter-popup').hide();
	});

	jQuery('#winter-popup .popup-close').click(function(){
		jQuery('#winter-popup').hide();
	});
</script>
