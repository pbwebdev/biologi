<div id="winter-popup" class="popup" data-popup="popup-1">
	<div class="popup-mask"></div>		
		<div class="popup-inner">
			<div class="popup-close">X</div>			
		</div>			
</div>
<style>
#winter-popup.popup
{
	z-index:9999;
	background: none;
}
#winter-popup a
{
	z-index:99;
}
#winter-popup .popup-mask
{
	width: 100%;
    height: 100%;
    display: block;
    position: fixed;
    background: rgba(0,0,0,0.75);
    top: 0px;
    left: 0px;
}
#winter-popup .popup-close
{
    display: block;
    cursor: pointer;
    position: absolute;
    right: 5px;
    top: -5px;
    font-size: 35px;
}

#winter-popup .popup-inner
{
    max-width: 700px;
    width: 90%;
	height:auto;
	cursor:pointer;
    padding: 440px 25px 25px 25px;
    position: absolute;
    top: 50%;
    left: 50%; 
    text-align: center;
    background-image: url("<?php echo get_stylesheet_directory_uri(); ?>/images/SMS_Sale_Popup.jpg");
    background-repeat: no-repeat;
    background-position: center top;
	background-size: 100% 100%;	
}

@media only screen and (max-width: 480px) {
	#winter-popup .popup-inner
	{
		padding: 200px 25px 25px 25px;
	}	
}
	
@media (min-width: 481px) and (max-width: 568px){
	#winter-popup .popup-inner
	{
		padding: 260px 25px 25px 25px;
	}	
}
@media (min-width: 569px) and (max-width: 767px){
	#winter-popup .popup-inner
	{
		padding: 320px 25px 25px 25px;
	}	
}
</style>

<script>
	function setCookie(name,value,days) {
        var expires;
        if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			expires = "; expires="+date.toGMTString();
        }
        else {
			expires = "";
		}
		document.cookie = name+"="+value+expires+"; path=/";
    }

	function getCookie(name) {
	    var nameEQ = name + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0;i < ca.length;i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1,c.length);
	        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	    }
	    return null;
	}
	
	function eraseCookie(name) {   
	    document.cookie = name+'=; Max-Age=-99999999;';  
	}

	var save_my_skin_sale_popup = getCookie('save_my_skin_sale_popup');
	if(save_my_skin_sale_popup != '1'){
		jQuery(window).on( "load", function() {
	        jQuery('#winter-popup').show();
	        setCookie('save_my_skin_sale_popup','1');		
	    });
	}

	jQuery('#winter-popup .popup-mask').click(function(){
		jQuery('#winter-popup').hide();
	});

	jQuery('#winter-popup .popup-close').click(function(e){
		jQuery('#winter-popup').hide();
		e.stopPropagation();
	});
	jQuery('#winter-popup .popup-inner').click(function(){
		window.location.href = '<?php echo site_url();?>/shop/serums/save-my-skin-bundle/';
	});
</script>
