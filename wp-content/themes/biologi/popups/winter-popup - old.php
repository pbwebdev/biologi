<div id="winter-popup" class="popup" data-popup="popup-1">
	<div class="popup-mask"></div>
	<div class="popup-inner">
		<div class="popup-close">X</div>
		<div class="title">Take 15% Off</div>
		<div class="sub-title">and save your skin this winter</div>
		<div class="use-code">use the code</div>
		<h3 class="goodbye">GOODBYEDRY</h3>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/Biologi-logo.png"  class="site-logo" />
	</div>
</div>
<style>
#winter-popup.popup
{
	z-index:9999;
	background: none;
}
#winter-popup .popup-mask
{
	width: 100%;
    height: 100%;
    display: block;
    position: fixed;
    background: rgba(0,0,0,0.75);
    top: 0px;
    left: 0px;
}
#winter-popup .popup-close
{
    display: block;
    cursor: pointer;
    position: absolute;
    right: 5px;
    top: -5px;
    font-size: 35px;
}
#winter-popup .popup-inner .title
{
	font-family: trenda-regular !important;
    font-size: 50px;
	text-transform:uppercase;
}
#winter-popup .popup-inner .sub-title
{
	font-size: 16px;
    font-weight: 700;
    margin-top: 10px;
}
#winter-popup .popup-inner .use-code
{
	font-size: 28px;
    margin-top: 50px;
    font-weight: 700;
}
#winter-popup .popup-inner h3.goodbye 
{
	font-size: 30px;
}
#winter-popup .popup-inner
{
    max-width: 700px;
    width: 90%;
    padding: 90px 25px 25px 25px;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    background:#E9D4E3;
    text-align: center;
}
#winter-popup .popup-inner .site-logo
{
	margin-top:20px;	
    width: 80px;
}	
@media only screen and (max-width: 480px) {
	#winter-popup .popup-inner .site-logo {width:60px;}
	#winter-popup .popup-inner .title {font-size: 35px;}
	#winter-popup .popup-inner .sub-title {font-size: 12px;}
}
</style>

<script>
	function setCookie(name,value,days) {
        var expires;
        if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			expires = "; expires="+date.toGMTString();
        }
        else {
			expires = "";
		}
		document.cookie = name+"="+value+expires+"; path=/";
    }

	function getCookie(name) {
	    var nameEQ = name + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0;i < ca.length;i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1,c.length);
	        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	    }
	    return null;
	}
	
	function eraseCookie(name) {   
	    document.cookie = name+'=; Max-Age=-99999999;';  
	}

	var winter_popup = getCookie('winter_popup');
	if(winter_popup != '1'){
		jQuery(window).on( "load", function() {
	        jQuery('#winter-popup').show();
	        setCookie('winter_popup','1');		
	    });
	}

	jQuery('#winter-popup .popup-mask').click(function(){
		jQuery('#winter-popup').hide();
	});

	jQuery('#winter-popup .popup-close').click(function(){
		jQuery('#winter-popup').hide();
	});
</script>
