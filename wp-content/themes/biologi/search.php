<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Biologi
 * @since 1.0
 * @version 1.0
 */
?>
<?php get_header(); ?>
<main id="content-container">	
	<div id="search" class="container">
		<?php if ( have_posts() ) : ?>	
			<div class="row">
				<div class="col-xs-12 page_title"> 	
					<h1 class="search-title"><?php printf( __( 'Search Results for: %s', 'biologi' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
				</div>
				<?php while ( have_posts() ) : the_post(); ?>
					<div class="col-xs-12">
						<h2 class="search-post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>				
						<div class="post-content excerpt">
							<?php the_excerpt(); ?>
						</div>					
					</div>	
				<?php endwhile; ?>
			</div>
			<div class="row">		
				<div class="numbers-pagination col-xs-12">
					<nav class="navigation pagination" role="navigation">					
						<div class="nav-links">
							<?php
								the_posts_pagination(array(
									'prev_text' =>  __( 'Previous', 'biologi' ),
									'next_text' =>  __( 'Next', 'biologi' ),								
								));
							?>
						</div>
					</nav>
				</div>
			</div>
		<?php else : ?>
			<div class="row">	
				<div class="col-xs-12 page_title"> 	
					<h1 class="search-title">Nothing Found</h1>
				</div>
				<div class="col-xs-12">
					<p>Sorry, but nothing matched your search terms. Please try again with some different keywords.</p>
					<?php get_search_form(); ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
</main>
<?php get_footer();