<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5" />
<title>Newsletter Subscription</title>
<link rel='stylesheet' href='css/popup.css' type='text/css' media='all' />
</head>
<body> 
<!-- Begin MailChimp Signup Form -->
<div id="mc_embed_signup">
	<form action="https://biologi.us16.list-manage.com/subscribe/post?u=e99fd5b6497b5b4338f798f2c&amp;id=d079d07031" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">
		<div id="mc_embed_signup_scroll">
			<h3 class="mc_h3">JOIN OUR SKINCARE MOVEMENT!</h3>
			<p class="mc_p">Subscribe to receive free weekly updates relating to health, beauty and wellness.</p>
			<div class="mc-field-group">	
				<input type="text" value="" name="NAME" class="" id="mce-NAME" placeholder="Name">
			</div>

			<div class="mc-field-group">	
				<input type="email" value="" name="EMAIL" class="required email" placeholder="Email" id="mce-EMAIL" required>
			</div>

			<div id="mce-responses" class="clear">
				<div class="response" id="mce-error-response" style="display:none"></div>
				<div class="response" id="mce-success-response" style="display:none"></div>
			</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
			<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e99fd5b6497b5b4338f798f2c_d079d07031" tabindex="-1" value=""></div>
			<div class="clear"><input type="submit" value="SUBSCRIBE HERE" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
		</div>
	</form>
</div>
<!--End mc_embed_signup-->
</body>
</html>