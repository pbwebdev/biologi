<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the content div and all content after
 *
 * @since 1.0.0
 */
?>
    <?php if(!is_front_page()): ?>
    <div id="subscribe" class="container">
		<div class="row">			
			<div class="col-xs-12 col-sm-4 col-sm-offset-2">
			    <div class="newsletter">Stay up to date on all things Biologi by joining our mailing list</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="join-button">Enter your email address</div>
			</div>
		</div>
    </div>
    <?php endif; ?>	

	<?php dynamic_sidebar('above-footer') ?>

	<div class="footer-wrapper">			
		<?php get_sidebar(); ?>			
	</div>
</div>
<!-- #root-container -->

<?php wp_footer(); ?>

<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Below Footer') ) : ?>
<?php endif; ?>

<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/5258800.js"></script>
<!-- End of HubSpot Embed Code -->

<!-- Newsletter home popup -->
<div id="home-newsletter-popup" class="popup home-popup" data-popup="popup-2">
	<div class="popup-inner">
		<a class="popup-close" data-popup-close="popup-2" href="#">x</a>
		<iframe src="<?php echo get_site_url(); ?>/wp-content/themes/biologi/blog-popup.php"></iframe>
	</div>
</div>

</body>
</html>