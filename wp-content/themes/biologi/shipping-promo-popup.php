<div id="promo-popup" class="popup" data-popup="popup-1">
	<div class="popup-mask"></div>		
		<div class="popup-inner">
			<div class="popup-close">X</div>			
		</div>			
</div>
<style>
#promo-popup
{
	z-index:9999;
	background: none;
}
#promo-popup a
{
	z-index:99;
}
#promo-popup .popup-mask
{
	width: 100%;
    height: 100%;
    display: block;
    position: fixed;
    background: rgba(0,0,0,0.75);
    top: 0px;
    left: 0px;
}
#promo-popup .popup-close
{
    display: block;
    cursor: pointer;
    position: absolute;
    right: 5px;
    top: -5px;
    font-size: 35px;
}

#promo-popup .popup-inner
{
    max-width: 700px;
    width: 90%;
	height:auto;
    padding: 440px 25px 25px 25px;
    position: absolute;
    top: 50%;
    left: 50%; 
    text-align: center;
    background-image: url("<?php echo get_stylesheet_directory_uri(); ?>/images/free-shipping-popup.jpg");
    background-repeat: no-repeat;
    background-position: center top;
	background-size: 100% 100%;	
	cursor: pointer;
}

@media only screen and (max-width: 480px) {
	#promo-popup .popup-inner
	{
		padding: 200px 25px 25px 25px;
	}	
}
	
@media (min-width: 481px) and (max-width: 568px){
	#promo-popup .popup-inner
	{
		padding: 260px 25px 25px 25px;
	}	
}
@media (min-width: 569px) and (max-width: 767px){
	#promo-popup .popup-inner
	{
		 padding: 379px 15px 15px 15px;
	}	
}
</style>

<script>
	function setCookie(name,value,days) {
        var expires;
        if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			expires = "; expires="+date.toGMTString();
        }
        else {
			expires = "";
		}
		document.cookie = name+"="+value+expires+"; path=/";
    }

	function getCookie(name) {
	    var nameEQ = name + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0;i < ca.length;i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1,c.length);
	        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	    }
	    return null;
	}
	
	function eraseCookie(name) {   
	    document.cookie = name+'=; Max-Age=-99999999;';  
	}

	var promo_popup = getCookie('promo_popup');
	if(promo_popup != '1'){
		jQuery(window).on( "load", function() {
	        jQuery('#promo-popup').show();
	        setCookie('promo_popup','1');		
	    });
	}

	jQuery('#promo-popup .popup-mask').click(function(){
		jQuery('#promo-popup').hide();
	});

	jQuery('#promo-popup .popup-close').click(function(e){
		jQuery('#promo-popup').hide();
		e.stopPropagation();
	});
	jQuery('#promo-popup .popup-inner').click(function(){
		window.location.href = '<?php echo site_url();?>/shop/';
	});
</script>
