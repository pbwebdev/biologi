<?php 
/**
 * The template for displaying all page content
 *
 * @since 1.0.0
 */

get_header(); ?>
<?php
	if(is_product()) {
		$class = "";
	}
	else {
		$class = "container";
	}
 ?>

<main id="content-container" class="<?php echo $class; ?>">	
	<div id="shop-content">
	   <?php if(!is_product()):?>
			<div id="breadcrumb"><?php woocommerce_breadcrumb(); ?></div>
		<?php endif; ?>
		<?php woocommerce_content(); ?>
	</div>
</main>	


<!-- Newsletter popup -->
<div id="shop-popup" class="popup" data-popup="popup-1">
	<div class="popup-inner">
		<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
		<iframe src="<?php echo get_site_url(); ?>/wp-content/themes/biologi/shop-popup.php"></iframe>
	</div>
</div>

<?php get_footer(); ?>