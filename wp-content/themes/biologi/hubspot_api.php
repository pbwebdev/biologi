<?php 
//CURL functions for HubSpot API integration
class CurlTransport 
{
	//Send POST request to create contact
	public function post($post_data){
		try
		{
			$hapikey = 'fe989384-9e21-4be6-8dce-6545e5a1e281';
			$url = 'https://api.hubapi.com/contacts/v1/contact?hapikey=' . $hapikey;
			$body = json_encode($post_data);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					"Authorization: Token token=".$this->appToken,
					'Content-Type: application/json',
					//'Accept: application/json',
				)
			);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response  = curl_exec($ch);
			$debug_ar = curl_getinfo($ch);
			$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

			if(curl_errno($ch))
			{
				throw new Exception(curl_error($ch));
			}
			
			curl_close($ch);
			if ($http_status != 200)
			{
			    $response_array = json_decode($response);
			    if (!isset($response_array->message))
			    {
    				throw new Exception("HubSpot encountered a POST error.".
    					"\n URL: " . $url .
    					"\n POST DATA: " . json_encode($post_data) .
    					"\n HTTP CODE: " . $http_status . 
    					"\n RESPONSE: " . $response);
			    }
			    else if ($response_array->message != "Contact already exists")
			    {
    				throw new Exception("HubSpot encountered a POST error.".
    					"\n URL: " . $url .
    					"\n POST DATA: " . json_encode($post_data) .
    					"\n HTTP CODE: " . $http_status . 
    					"\n RESPONSE: " . $response);
			    }
			}
		}
		catch(Exception $e)
		{
			$log = new WC_Logger();
			if(is_array($e) || is_object($e))
			{
				$log_entry = 'Exception Trace: ' . print_r( $e->getMessage(), true );
			}
			else
			{
				$log_entry = 'Exception Trace: ' . $e->__toString();
			}
			$log->add( 'hubspot-woocommerce-log', $log_entry );
		}

		return $response;
	}
}

//Create contact in Hubspot 
add_action( 'woocommerce_thankyou', 'hubspot_create_contact');
function hubspot_create_contact($order_id){
	$order = wc_get_order($order_id);
	$hubspot_newsletter = get_post_meta( $order->id, 'hubspot_woocommerce_newsletter', true );
	if(!empty($hubspot_newsletter))
	{
		$first_name = $order->data['billing']['first_name'];
		$last_name = $order->data['billing']['last_name'];
		$user_email = $order->data['billing']['email'];	
		$post_data = array(
			'properties' => array(
				array(
					'property' => 'email',
					'value' => $user_email
				),
        		array(
        			'property' => 'firstname',
        			'value' => $first_name
        		),
        		array(
        			'property' => 'lastname',
        			'value' => $last_name
        		),
        		array(
        			'property' => 'website_checkout_optin',
        			'value' => 'true'
        		)
			),
		);
		
		$api = new CurlTransport();
		$response = $api->post($post_data);
	}
}	
?>