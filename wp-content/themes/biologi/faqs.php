<?php 
/* 
Template Name: Faqs Page 
*/ 
?>

<?php get_header(); ?>
<main id="content-container">	
	<div id="testimonials" class="container">
	   <div class="testimonial row">
			<?php
			$args = array(
				'post_type' => 'videofaqs',
				'post_status' => 'publish',			
				'posts_per_page' => -1,			
				"order" => 'DESC'
			);
			
			$result = new WP_Query($args);
			$count = 0;

			while ($result->have_posts()) 
			{
				$result->the_post();
				?>		
				<div class="testimonial-item col-xs-12 col-sm-6 col-md-4 col-lg-3">
					<div class="featured-image">
						<?php
						$featured_image = get_image_url(get_post_thumbnail_id($post->ID), 'large');
						$youtube_video = get_field('faq_youtube_video');
						
						if (!empty($featured_image)) {
							if (!empty($youtube_video)) {										
						?>
								<span class="youtube-link" youtubeid="<?php echo $youtube_video;?>"><img src="<?php echo $featured_image; ?>" class="img-responsive" /></span>
						<?php } else { ?>					
								<img src="<?php echo $featured_image; ?>" class="img-responsive" />
						<?php } 
						} ?>						
					</div>
				</div>	
				<?php
					$count++;
					if($count == 4) {
						$count = 0;
						//echo '<div class="clear"></div>'; 
					}	
			}			
			wp_reset_postdata();
			?>
		</div>	
	</div>
</main>
<div class="clear"></div>
<?php get_footer(); ?>