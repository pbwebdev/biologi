<?php
/**
 * Single Product Up-Sells
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/up-sells.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $upsells ) : ?>

	<section class="up-sells upsells products">

		<h2><?php esc_html_e( 'Related Products', 'woocommerce' ) ?></h2>

		<?php woocommerce_product_loop_start(); ?>
          
			<?php foreach ( $upsells as $upsell ) : ?>

				<?php
				 	$post_object = get_post( $upsell->get_id() );
					remove_action('woocommerce_before_shop_loop_item_title', 'display_product_icon', 10);
					remove_action('woocommerce_after_shop_loop_item_title', 'display_product_use_for', 7);	
					remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_rating', 10);	
					remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);	
					remove_action('woocommerce_after_shop_loop_item', 'display_view_product_button', 6);
					setup_postdata( $GLOBALS['post'] =& $post_object );
					wc_get_template_part( 'content', 'product' ); ?>

			<?php endforeach; ?>

		<?php woocommerce_product_loop_end(); ?>

	</section>

<?php endif;

wp_reset_postdata();
