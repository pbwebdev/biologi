<?php 
$product_faqs = get_field('product_faqs');
if (isset($product_faqs) && !empty($product_faqs)): ?>
	<div id="product-faqs" class="container bottom-margin-60">	
		<div class="row">
			<div class="col-xs-12 col-md-10 col-md-offset-1">
				<div id="accordion">
					<h2 class="text-center">Frequently Asked Questions</h2>
					<?php foreach($product_faqs as $key => $faqs) { ?>	
						<div class="card">
							<div class="card-header" id="heading">
								<h3 class="faqs-title collapsed" data-toggle="collapse" data-target="#collapse_<?=$key?>" aria-expanded="true" aria-controls="collapse"><?php echo $faqs['title']; ?></h3>
							</div>
							<div id="collapse_<?=$key?>" class="collapse" aria-labelledby="heading" data-parent="#accordion">   	
								<div class="faqs-content card-body"><?php echo $faqs['description']; ?></div>
							</div>	
						</div>						
					<?php } ?>
				</div>	
			</div>		
		</div>		
	</div>
<?php endif; ?>