<?php
$flexible_content = get_field('product_content');	
if(isset($flexible_content) && !empty($flexible_content)):
?>
	<div id="flexible-content">
	<?php   
		foreach($flexible_content as $content) {
			if($content['acf_fc_layout'] == 'video') {
			?>	
				<div class="container-fluid video-container <?php echo $content["css_class"]; ?>">
					<div class="container">
						<div class="row">		
							<div class="col-xs-12 col-sm-3"><h3 class="white-heading"><?php  echo $content["title"]; ?></h3></div>
							<div class="col-xs-12 col-sm-9 youtube-video">
								<div class="embed-responsive embed-responsive-16by9">
									<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php  echo $content["youtube_video_id"]; ?>?rel=0" ></iframe>
								</div>
							</div>
						</div>
					</div>	
				</div>	
			<?php } else if($content['acf_fc_layout'] == '2_column_text') { ?>	
				<div class="container-fluid two-column-container <?php echo $content["css_class"]; ?>">
					<div class="container">
						<div class="row">		
							<div class="col-xs-12 col-sm-6 left-column"><?php  echo $content["left_column_text"]; ?> </div>
							<div class="col-xs-12 col-sm-6 right-column"> <?php  echo $content["right_column_text"]; ?></div>
						</div>
					</div>	
				</div>	
			<?php }	else if($content['acf_fc_layout'] == 'full_width_text') { ?>	
				<div class="container-fluid full-width-container <?php echo $content["css_class"]; ?>">
					<div class="container">
						<div class="row">	
							<?php  echo $content["full_column_text"]; ?>
						</div>
					</div>	
				</div>
			<?php	
			}
		}
	?>
	</div>
 <?php endif; ?>