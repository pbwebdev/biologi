<?php  
$product_tips = get_field('product_tips');
if (isset($product_tips) && !empty($product_tips)): ?>
<div id="product-tips" class="container bottom-margin-60">
	<?php foreach($product_tips as $key => $tips) { ?>	
		<div class="tips">
			<div class="tips-description">
				<?php echo $tips['description']; ?>
			</div>	
		</div>
	<?php } ?>	
</div>			
<?php endif; ?>

<script>
//Product Tips Slider
jQuery(document).ready(function($){
	$('#product-tips').slick({		
		infinite: true,						
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [	
            {
				breakpoint: 1024,
				settings: {								
					slidesToShow: 3,
					slidesToScroll: 3,
					dots: true,
					arrows: false
					
				}
			},			
			{
				breakpoint: 767,
				settings: {								
					slidesToShow:2,
					slidesToScroll: 2,
					dots: true,
					arrows: false
				}
			},          			
			{
				breakpoint: 480,
				settings: {							
					slidesToShow: 1,
					slidesToScroll: 1,
					dots: true,
					arrows: false
				}
			}
		]
	});
});
</script>