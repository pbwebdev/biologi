<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook Woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
	<div class="container">
		<?php
			/**
			 * Hook: woocommerce_before_single_product_summary.
			 *
			 * @hooked woocommerce_show_product_sale_flash - 10
			 * @hooked woocommerce_show_product_images - 20
			 */
			do_action( 'woocommerce_before_single_product_summary' );
		?>

		<div class="summary entry-summary">
			<?php
				/**
				 * Hook: Woocommerce_single_product_summary.
				 *
				 * @hooked woocommerce_template_single_title - 5
				 * @hooked woocommerce_template_single_rating - 10
				 * @hooked woocommerce_template_single_price - 10
				 * @hooked woocommerce_template_single_excerpt - 20
				 * @hooked woocommerce_template_single_add_to_cart - 30
				 * @hooked woocommerce_template_single_meta - 40
				 * @hooked woocommerce_template_single_sharing - 50
				 * @hooked WC_Structured_Data::generate_product_data() - 60
				 */
				do_action( 'woocommerce_single_product_summary' );
			?>
		</div>
	</div>
</div>	

<?php	
	$upsells = $product->get_upsells();	
	if(!empty($upsells)):
?>
<div id="related-products" class="container-fluid bottom-margin-60">	
	<div class="container">		
		<?php
			/**
			 * Hook: woocommerce_after_single_product_summary.
			 *
			 * @hooked woocommerce_output_product_data_tabs - 10
			 * @hooked woocommerce_upsell_display - 15
			 * @hooked woocommerce_output_related_products - 20
			 */
			do_action( 'woocommerce_after_single_product_summary' );
		?>
	</div>
</div>
<?php endif; ?>

<?php get_template_part('woocommerce/single-product/partials/product', 'flexible-content'); ?>	

<?php get_template_part('woocommerce/single-product/partials/product', 'tips'); ?>	

<?php get_template_part('woocommerce/single-product/partials/product', 'faqs'); ?>

<?php
	global $post;	 
    $post_slug = $post->post_name;	
	if($post_slug == 'save-my-skin-bundle' || $post_slug == 'save-my-skin-bundle-mini-pack'):
?>
<div id="skincare-routine" class="container-fluid bottom-margin-70">
	<div class="container">
		<div class="col-xs-12 col-sm-10 col-sm-offset-1 quantity-price">
		    <h3>Ready to simplify your skincare routine?</h3>
             <?php do_action('custom_afterpay_hook'); ?>
			<form method="post" enctype="multipart/form-data" class="cart cart_group bundle_form">				
				<span class="price"><?php echo $product->get_price_html(); ?> </span>
				<?php do_action( 'woocommerce_bundles_add_to_cart_wrap', $product ); ?>
			</form>	
		</div>
	</div>	
</div>
<?php endif; ?>

<div class="container-fluid">
	<div class="container">	      
		<?php do_action( 'woocommerce_after_single_product' ); ?>
	</div>
</div>	

<div id="product-instagram" class="container">
	<h2>Follow Instagram</h2>
	<?php echo do_shortcode('[instagram-feed num=4 cols=4]'); ?>
</div>	