<?php 
/*
Template Name: Skin Care Revolution Page
*/
?>

<?php get_header(); ?>

<div id="homepage" class="skin-care-revolution">
	<div class="container main-banner">
		<div class="row">
			<a href="<?php echo get_permalink( woocommerce_get_page_id('shop')); ?>">
				<img class="desktop-banner" src="<?php echo get_stylesheet_directory_uri(); ?>/images/skin-care-revolution/nz-desktop-banner.jpg"/>
				<img class="mobile-banner" src="<?php echo get_stylesheet_directory_uri(); ?>/images/skin-care-revolution/nz-mobile-banner.jpg"/>
			</a>
		</div>		
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 industry-deception">
				<?php the_content(); ?>
			</div>	
			<div class="col-xs-12 shop-now">
				<a href="<?php echo get_permalink( woocommerce_get_page_id('shop')); ?>" class="button">Shop Now</a>
			</div>	
		</div>	
	</div>
	<div class="container-fluid">
		<div class="row featured-products post-type-archive post-type-archive-product woocommerce">
			<?php
				$args = array(
					'post_type' => 'product',
					'posts_per_page' => 12,
					'orderby'   => 'menu_order',
					'order' => 'ASC',					
					'tax_query' => array(
							array(
								'taxonomy' => 'product_visibility',
								'field'    => 'name',					
								'terms'    => 'featured',
							),
						),
					);
				$products = new WP_Query( $args );			
				if ( $products->have_posts() ) {
					while ( $products->have_posts() ) : $products->the_post();
						wc_get_template_part( 'content', 'product' );
					endwhile;
				} 		
			?>
		</div>
	</div>
	<div class="container" id="instagram-feed">
		<div class="row">	
			<div class="col-xs-12">
				<?php echo do_shortcode('[instagram-feed]'); ?>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>