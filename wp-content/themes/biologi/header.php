<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the content div.
 *
 * @since 1.0.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5" />
    <meta name="p:domain_verify" content="d9ee9df35af9475d96bb9a9559dd6bff"/>	
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php esc_url( bloginfo( 'pingback_url' ) ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
	<!-- Start Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '818018198406831');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=818018198406831&ev=PageView&noscript=1" /></noscript>
	<!-- End Facebook Pixel Code -->
	<!-- Start Google Analytics Code -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-106327158-1', 'auto');
      ga('send', 'pageview');
    </script>
    <!-- End Google Analytics Code -->
	<!-- Start Hotjar Tracking Code-->
	<script>
		(function(h,o,t,j,a,r){
			h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
			h._hjSettings={hjid:1133468,hjsv:6};
			a=o.getElementsByTagName('head')[0];
			r=o.createElement('script');r.async=1;
			r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
			a.appendChild(r);
		})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
	<!-- End Hotjar Tracking Code--> 
	<script>
		document.addEventListener( 'wpcf7mailsent', function( event ) {
    		ga('send', 'event', 'Contact Form', 'submit');
		}, false );
		document.addEventListener( 'DOMContentLoaded', function( event ) {
			var x = document.getElementsByClassName('contactemail');
			var i;
			for (i = 0; i < x.length; i++) {
    			x[i].onclick=function() {
					// alert ('Opening your email software to send email');
					ga('send', 'event', 'Email', 'Clicked ENQUIRIES EMAIL Link');
				} 
			} 
		});
	</script>    
</head>
		
<body <?php body_class(); ?> >
    <div id="biologi-top-menu">
		<div class="top-menu-inner container">
			<?php
				date_default_timezone_set("Australia/Sydney");
				$now = date('Y-m-d H:i:s');
				$start_date_banner = '2020-05-03 00:00:00';	 
				$end_date_banner = '2020-05-10 23:59:00';
			?>	
			<div class="row">
				<div class="col-xs-4 col-sm-3 col-md-3 text-left"><?php get_search_form(); ?></div>	
				
				<div class="col-md-5 message  hidden-xs hidden-sm visible-md-* visible-lg-*"><?php //if(strtotime($now) >= strtotime($start_date_banner) && strtotime($now) <= strtotime($end_date_banner)): ?><a href="/covid-19/">COVID-19 UPDATE: WE ARE STILL SHIPPING</a><?php //endif; ?></div>
				<div class="col-xs-8  col-sm-5 message  visible-xs-* visible-sm-* hidden-md hidden-lg"><?php //if(strtotime($now) >= strtotime($start_date_banner) && strtotime($now) <= strtotime($end_date_banner)): ?><a href="/covid-19/">WE ARE STILL SHIPPING</a><?php //endif; ?></div>
				
				<div class="col-xs-4 col-sm-4 col-md-4 visible-sm-* visible-lg-*  hidden-xs">					
					<div class="menu-top-menu-container">
						<ul id="menu-top-menu" class="menu">
							<?php $user = wp_get_current_user();							 
							if(is_user_logged_in() && (in_array('administrator',$user->roles) || in_array('wholesale_customer',$user->roles))){ ?>
								<li class="menu-item hidden-sm"><a href="<?=site_url()?>/wholesale-ordering/">Wholesale </a></li>
							<?php } ?>
							<li class="menu-item"><a title="My Account" href="<?=site_url()?>/my-account/"><span>My Account</span></a></li>
							<li class="menu-item"><a title="Checkout" href="<?=site_url()?>/checkout/">Checkout</a></li>
							<li class="biologi-cart"><a href="<?=site_url()?>/cart/" title="View your shopping cart"><span>Cart</span></a></li>
						</ul>	
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="root-container">
		<?php
			$menu_type_class = get_theme_mod( 'enso_ctmzr_menu_section_styles_menu_display', 'vertical-menu' );		
		?>
		<div id="header-wrapper">
			<header id="header-container"  class="container <?php echo esc_attr( $menu_type_class ); ?>">
				<div class="header-content row">
					<!-- Site Menu -->
					<nav class="site-menu">
						<?php
                        $user = wp_get_current_user();
                        if(!in_array("wholesale_customer", $user->roles))
                        {
							// Main Menu - Left
							wp_nav_menu( array(
								'container'		 => 'ul',
								'theme_location' => 'main-menu-left',
								'menu_class'     => 'menu-list menu-style',
							));
                        }	
						?>
						<div class="visible-xs-* hidden-sm hidden-md hidden-lg">
							<ul id="menu-top-menu" class="menu">
								<li class="menu-item"><a title="My Account" href="<?=site_url()?>/my-account/"><span><i class="fa fa-user"></i></span></a></li>
								<li class="biologi-cart"><a href="<?=site_url()?>/cart/" title="View your shopping cart"><span><i class="fa fa-shopping-cart" aria-hidden="true"></i></span></a></li>
							</ul>	
						</div>	
						<!-- Logo and tagline -->
						<div class="site-logo">
						    <?php if(!in_array("wholesale_customer", $user->roles)) { ?>								
						 	    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
						 	<?php } ?>    
							<?php
								$custom_logo_id = get_theme_mod( 'custom_logo' );
								if(! empty( $custom_logo_id ))
								{
									$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
									$logo_url = $image[0];
									echo '<img src="' . esc_url( $logo_url ) . '" alt="' . esc_attr( get_bloginfo( 'name' ) ) . '" class="logo-image" />';
								}
							?>
							<?php if(!in_array("wholesale_customer", $user->roles)) { ?>
							    </a>
							<?php } ?>
						</div>
						<!-- .site-logo -->
						
						<?php
						if(!in_array("wholesale_customer", $user->roles))
                        {
							// Main Menu - Right
							wp_nav_menu( array(
								'container'		 => 'ul',
								'theme_location' => 'main-menu-right',
								'menu_class'     => 'menu-list menu-style',
							));
                        }	
						?>
                        <?php if(!in_array("wholesale_customer", $user->roles)) : ?>
                            <!-- Mobile menu container: the items will be generated in the JS -->
                            <div id="mobile-menu">
                            	<a id="mobile-menu-toggle" class="pull-right" href="#mobile-menu-entity">
                            		<span class="mobile-menu-text"><?php esc_html_e( 'Menu', 'enso' ); ?></span>
                            		<i class="ion-navicon"></i>
                            	</a>
                            	<div id="mobile-menu-entity"></div>
                            </div>
                        <?php endif; ?>
                		<?php 
                	    	$user = wp_get_current_user();							 
					        if(is_user_logged_in() && (in_array('administrator',$user->roles) || in_array('wholesale_customer',$user->roles))){ ?>
					    	    <div class="menu-li visible-xs-* visible-sm-* hidden-md hidden-lg"><a href="<?=site_url()?>/wholesale-ordering/"><strong>Wholesale ordering</strong></a></div>
						<?php } ?>
					</nav>
				</div><!-- end header-content -->	
			</header>

			<!-- Head line text -->
			<?php $tagline = get_bloginfo( 'description' ); ?>
			<div class="headline-text">
				 <div class="container">
					<div class="row">
						<?php echo esc_html( $tagline );?>
					</div>
				  </div> 					
			</div>			
		</div>
		<!-- end header-wrapper -->
			
	
		<?php if(is_front_page()) : ?>
		
            <!--<div class="videoWrapperOuter embed-responsive embed-responsive-16by9">
                <div class="videoWrapperInner">
                    <video name="media" controls autoplay="autoplay" class="embed-responsive-item">
                        <source src="<?php //echo get_stylesheet_directory_uri();?>/banner.mp4" type="video/mp4">
                    </video>	
                </div>
            </div>-->
                    
            <?php 
                if(strtotime($now) >= strtotime($start_date_banner) && strtotime($now) <= strtotime($end_date_banner)):
				    include('promo-popup.php');	
			?>
				<div id="home-promo-banner" class="container">
				    <div class="row">	
                        <div class="col-xs-12">
        					<a href="/shop/serums/bqk-radiance-face-serum/">
        						<img class="desktop-banner" src ="<?php echo get_stylesheet_directory_uri();?>/images/MothersDay-banner.jpg" />
        						<img class="mobile-banner" src ="<?php echo get_stylesheet_directory_uri();?>/images/MothersDay-mobile-banner.jpg" />
        					</a>
    					</div> 
    				</div>
    			</div>
			<?php else: ?>	
				<div id="home-promo-banner" class="container slider">
				    <div class="row slider-box">	
                        <div class="col-xs-12">
        					<a href="/shop/">
        						<img class="desktop-banner" src ="<?php echo get_stylesheet_directory_uri();?>/images/shop-range-website-banner.jpg" />
        						<img class="mobile-banner" src ="<?php echo get_stylesheet_directory_uri();?>/images/shop-range-mobile-banner.jpg" />
        					</a>
    					</div> 
    				</div>
    				<div class="row slider-box">	
                        <div class="col-xs-12">
        					<a href="/biologi-bottle-swap/">
        						<img class="desktop-banner" src ="<?php echo get_stylesheet_directory_uri();?>/images/TerraCyclebanner.jpg" />
        						<img class="mobile-banner" src ="<?php echo get_stylesheet_directory_uri();?>/images/TerraCycle-mobile-banner.jpg" />
        					</a>
    					</div> 
    				</div>
				</div>
			<?php endif; ?>		
	
		<?php elseif(is_home()): ?>

			<div id="blog-banner">
				<div class="container">
					<div class="blog-row row">						    
						<h1 class="banner-title col-xs-12 col-sm-8 col-md-8">Welcome to <br/>the Biologi <br/>Blog</h1>
						<div class="blog-newsletter col-xs-12 col-sm-4 col-md-4">
							<h2>Join our <br/>skincare <br/>movement!</h2>
							<a href="#" class="join-here">Join Here</a>
						</div>						
					</div>
				</div>
			</div>
			
		<?php elseif(is_shop()): ?>
					
			<?php /* if(strtotime($now) >= strtotime($start_date) && strtotime($now) <= strtotime($end_date)): ?>
				<a href="/donation/">
					<div class="container" style="margin-bottom: 20px;">								
						<img src="../wp-content/themes/biologi/images/BCNA_Banner_shop.jpg"/>
                            <div class="donation-amount shop" style="position: absolute; top: 13.7%; right: 32%; font-family: trenda-bold, serif;">
                            <?php echo '<p>Donations Collected </p><span class="amnt">'.' '. $donation_amount.'</span>'; ?>
                            </div>
					</div>
				</a>
			<?php endif; */ ?>
			
		<?php elseif(is_cart()): ?>
		
    		<?php 
				$start_date_banner2 = '2020-04-20 00:00:00';	
				$end_date_banner2 = '2020-04-25 23:59:00'; 
    		    if(strtotime($now) >= strtotime($start_date_banner2) && strtotime($now) <= strtotime($end_date_banner2)): ?>
                    <div id="home-promo-banner" class="container">
                        <div class="row">
                            <a href="/shop/">
                            	<img class="desktop-banner" src ="<?php echo get_stylesheet_directory_uri();?>/images/" />
                            	<img class="mobile-banner" src ="<?php echo get_stylesheet_directory_uri();?>/images/" />
                            </a>					 
                        </div>
                    </div>
            <?php endif; ?>
			
		<?php elseif(!is_single() && !is_product() && !is_search() && !is_category()): ?>

			<?php 
				$background_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); 
				if(!empty($background_img[0]))
				{	
			?>
				<div class="biologi-featured-image" style="background: url('<?php echo $background_img[0]; ?>') no-repeat; background-size: cover;">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-7 col-md-8">
								<h1 class="biologi-featured-image-title"><?php the_field('biologi_featured_image_title'); ?></h1>
							</div>				
							<?php 
								$testimonial_category_id = get_field('testimonial_category_id');									
								if (!empty($testimonial_category_id)) {
							?>  
									<div class="col-xs-12 col-sm-5 col-md-4 hidden-xs">	
										<div class="testimonial-slide">
											<?php echo do_shortcode('[sp_testimonials_slider limit="-1" slides_column="1"  slides_scroll="1" dots="false" arrows="false" category='.$testimonial_category_id.' autoplay="true" autoplay_interval="4500" speed="1500" ]'); ?>
										</div>
									</div>	
							<?php } ?>
						</div>			
					</div>		
				</div>
			<?php } ?>

		<?php endif; ?>
		