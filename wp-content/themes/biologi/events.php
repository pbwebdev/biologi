<?php 
/**
Template Name: Events Page 
*/
?>

<?php get_header(); ?>

<main id="content-container" class="container">	
	<?php while ( have_posts() ) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php get_template_part( 'template-parts/content', 'page' ); ?>
		</article>
	<?php endwhile; ?>	
</main>

<?php 
$images = get_field('event_image_gallery');
if($images): 
?>
    <ul id="event-slider">
        <?php foreach($images as $image): ?>
            <li>
                <img src="<?php echo esc_url($image['sizes']['large']); ?>" />
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

<script type="text/javascript">
jQuery(document).ready(function($){
	$('#event-slider').slick({
	  dots: false,
	  autoplay: false,
	  infinite: true,
	  slidesToShow: 3,
	  slidesToScroll: 3,
	  arrows: true,
	  responsive: [
        {
          breakpoint: 768,
          settings: {
        	  slidesToShow: 2,
        	  slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
        	  slidesToShow: 1,
        	  slidesToScroll: 1
          }
        }
      ]
	});	
});
</script>

<?php get_footer(); ?>