<?php
/**
 * Shared page content for page.php and portfolio-page-content.php
 *
 * @since 1.0.2
 */
?>

<div class="post-content-container clearfix">
	<div class="post-content-wrapper">
		<div class="post-content">
			<?php if(is_cart() || is_checkout()) : ?>
				<div class="page-title"><h3><?php echo the_title(); ?></h3></div>
			<?php endif; ?>

			<?php
				the_content();
				enso_print_post_pagination();		
			?>
		</div>
	</div>		
</div>
<!-- .post-content-container -->