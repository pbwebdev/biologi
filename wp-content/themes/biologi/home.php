<?php 
get_header();
global $wpdb, $post;
?>
<main id="content-container" class="container">	
	<div class="row">
		<?php 
			//show latest post as featured
			$args = array(   
				"post_type" => "post",
				'post_status'=>'publish',			
				"orderby" => "post_date",
				"posts_per_page" =>1,			
				"order" => 'DESC'
			);
			
			$result = new WP_Query($args);
			while ($result->have_posts()) {
				$result->the_post();
				if (has_post_format('video')) {						
					$post_youtube_video = get_field('post_youtube_video');
				?>					
					<div class="col-xs-12">					
						<div class="latest-post">
							<div class="latest-post-image youtube-link col-xs-12 col-sm-4" youtubeid="<?php echo $post_youtube_video;?>">
								<?php
									$featured_image = get_image_url(get_post_thumbnail_id($post->ID), 'blog_latest_post');
									if(!empty($featured_image)) {	  
								?>
									<img src="<?php echo $featured_image; ?>" class="img-responsive" />
								<?php } ?>	
							</div>	
							<div class="latest-post-text youtube-link col-xs-12 col-sm-8" youtubeid="<?php echo $post_youtube_video;?>">
								<div class="latest-post-date">Latest Post</div> 
								<h2><?php the_title(); ?></h2>
								<div class="post-text"><a class="read-more" style="border-bottom: 1px solid #000;">View Video</a></div>
							</div>
							<div class="clear"></div>
						</div>
					</div>	
				<?php } else { ?>
					<div class="col-xs-12">
						<div class="latest-post">
							<div class="latest-post-image col-xs-12 col-sm-4">
								<?php
									$latest_image = get_image_url(get_post_thumbnail_id($post->ID), 'blog_latest_post');
									if(!empty($latest_image)) {	  
								?>
									<a href="<?php the_permalink(); ?>"><img src="<?php echo $latest_image; ?>" class="img-responsive" /></a>
								<?php } ?>
							</div>	
							<div class="latest-post-text col-xs-12 col-sm-8">
								<div class="latest-post-date">Latest Post</div> 
								<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<div class="post-text"><a href="<?php the_permalink(); ?>"><?php the_content("CONTINUE READING"); ?></a></div>					
							</div>
							<div class="clear"></div>
						</div>
					</div>
			<?php
				}	
			}
			wp_reset_postdata();
		?>
	</div>	

	<div class="blog-posts row">
		<div class="col-xs-12 col-sm-8">
			<?php		
				//show posts grid
				$args1 = array(   
					'post_type' => 'post',
					'post_status'=>'publish',
					'posts_per_page' => 100,
					'offset' => 1,
					"orderby" => "post_date",				
					"order" => 'DESC'
				);
				$count = 0;
				$post_result = new WP_Query($args1);
				while ($post_result->have_posts()) {
					$post_result->the_post();
					if (has_post_format('video')) {						
						$post_youtube_video = get_field('post_youtube_video');
					?>					
						<div class="blog-item col-xs-12 col-sm-6">
							<span class="youtube-link" youtubeid="<?php echo $post_youtube_video;?>">
								<div class="featured-image">
									<?php
										$featured_image = get_image_url(get_post_thumbnail_id($post->ID), 'blog_thumbnail');
										if(!empty($featured_image)) {	  
									?>
										<img src="<?php echo $featured_image; ?>" class="img-responsive" />
									<?php } ?>					  
								</div>				
								<h4 class="widget-title"> <?php the_title(); ?></h4>
								<a class="read-more">View Video</a>	
							</span>
						</div>	
					<?php } else { ?>	
						<div class="blog-item col-xs-12 col-sm-6">
							<div class="featured-image">
								<?php
									$featured_image = get_image_url(get_post_thumbnail_id($post->ID), 'blog_thumbnail');
									if(!empty($featured_image)) {	  
								?>
									<a href="<?php the_permalink(); ?>"><img src="<?php echo $featured_image; ?>" class="img-responsive" /></a>
								<?php } ?>					  
							</div>				
							<h4 class="widget-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
							<a href="<?php the_permalink(); ?>" class="read-more">Read More</a>
						</div>	
						<?php							
						}	
						$count++;
							if($count == 2) {
								$count = 0;
								echo '<div class="clear"></div>'; 
							}
					}	
					wp_reset_postdata();
				?>
		</div>
		<div class="post-sidebar col-xs-12 col-sm-4">
		    <?php get_search_form(); ?>
			<?php dynamic_sidebar('Blog Sidebar') ?>	
		</div>
	</div>	
</main>
<?php get_footer(); ?>