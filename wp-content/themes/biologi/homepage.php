<?php 
/*
Template Name: Home Page
*/
?>

<?php get_header(); ?>

<div id="homepage">
    <div class="container">
        <?php
        $user=wp_get_current_user();
        if($user->roles[0] != 'wholesale_customer') {
        ?>    
            <div class="shipping-notes">
                <div class="shipping-truck"><i class="fa fa-truck" aria-hidden="true"></i></div>
                <div class="shipping-text"><p>Free shipping on all orders over $150 Australia Wide and $300 International</p></div>
            </div>
        <?php } ?>
    </div>   
	<div class="container main-banner">
		<div class="row">
		    <div class="col-xs-12">
    			<a href="/shop/">
    				<img class="desktop-banner" src="<?php echo get_stylesheet_directory_uri(); ?>/images/home/main-banner-desktop.jpg"/>
    				<img class="mobile-banner" src="<?php echo get_stylesheet_directory_uri(); ?>/images/home/main-banner-mobile.jpg"/>
    			</a>
			</div>	
		</div>		
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 industry-deception">
				<h3>One hundred percent.</h3>
				<p>In an industry rife with deception, we're only interested in what works. Our world-first technology is capturing 100% pure living plant nutrition to radically transform skin.</p>
			</div>		
		</div>	
	</div>
	<div class="container-fluid">
		<div class="row featured-products post-type-archive post-type-archive-product woocommerce">
			<?php
				$args = array(
					'post_type' => 'product',
					'posts_per_page' => 12,
					'orderby'   => 'menu_order',
					'order' => 'ASC',					
					'tax_query' => array(
							array(
								'taxonomy' => 'product_visibility',
								'field'    => 'name',					
								'terms'    => 'featured',
							),
						),
					);
				$products = new WP_Query( $args );			
				if ( $products->have_posts() ) {
					while ( $products->have_posts() ) : $products->the_post();
						wc_get_template_part( 'content', 'product' );
					endwhile;
				} 		
			?>
		</div>
	</div>
	<div class="container-fluid subscribe">
		<div class="container">
			<div class="row">			
				<div class="col-xs-12 col-sm-6 newsletter">Subscribe to Biologi</div>
				<div class="col-xs-12 col-sm-6">
					<div class="join-button">Enter your email address</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row plant">
			<div class="col-xs-4 col-sm-4 col-md-4 text-center">
				<div class="plant-image">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/home/innovative-extraction.jpg"/>
				</div>
				<div class="plant-dec">Innovative extraction method.</div>
			</div>	
			<div class="col-xs-4 col-sm-4 col-md-4 text-center">
				<div class="plant-image">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/home/plant-to-bottle.jpg"/>
				</div>
				<div class="plant-dec">Pure plant to bottle.</div>
			</div>	
			<div class="col-xs-4 col-sm-4 col-md-4 text-center">
				<div class="plant-image">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/home/100-active.jpg"/>
				</div>
				<div class="plant-dec">100% active.</div>
			</div>		
		</div>	
		<div class="row d-flex">
			<div class="col-xs-12 col-sm-6 col-md-6 my-auto home-dec">
				<div class="skincare-quiz">
					<h3>Take our skincare quiz</h3>
					<a href="/serum-quiz/">Which Serum is right for me?</a>	
				</div>
			</div>	
			<div class="col-xs-12 col-sm-6 col-md-6 padding-left home-img">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/home/skincare-quiz-feature.jpg"/>
			</div>
		</div>
		<div class="row d-flex">
			<div class="col-xs-12 col-sm-6 col-md-6 padding-right home-img">
				<video id="homepage-dropper-video" class="video-mp4" name="media" autoplay="autoplay" loop>
				    <source src="<?php echo get_stylesheet_directory_uri();?>/images/home/hand-dropper.mp4" type="video/mp4">
				</video>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 my-auto home-dec">
				<div class="biologi-blog">	
					<h3>Biologi Blog</h3>
					<p>Read the latest insights and get tips and tricks from our expert team</p>
					<a class="read-more" href="/blog/">Read More</a>
				</div>
			</div>
		</div>
		<div class="row d-flex">
			<div class="col-xs-12 col-sm-6 col-md-6 my-auto home-dec">
				<div class="fb-beauty-community">
					<h3>Join our Facebook Beauty Community</h3>
					<a class="read-more" href="https://www.facebook.com/groups/399973537316530/" target="_blank">Join Now</a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 padding-left home-img">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/home/facebook-community-feature.jpg"/>
			</div>
		</div>
	</div>	
	<div class="container" id="instagram-feed">
		<div class="row">	
			<div class="col-xs-12">
				<?php echo do_shortcode('[instagram-feed]'); ?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
//Homepage dropper video autoplay in loop
jQuery(document).ready(function($){   
    $("#homepage-dropper-video").prop('muted',true)[0].play();  
});
</script>

<?php get_footer(); ?>