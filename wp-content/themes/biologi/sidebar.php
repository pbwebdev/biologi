<?php
/**
 * For displaying selected widgets in the theme widget area
 *
 * @since 1.0.0
 */
?>

<?php
	$has_any_widgets = false;
	$display_widget_area = get_theme_mod( 'enso_ctmzr_footer_widget_area_styles_show_footer_widget_area', true );
	$widget_area_columns = intval( get_theme_mod( 'enso_ctmzr_footer_widget_area_styles_footer_widget_area_column_number', '4' ) );
	$widget_area_id_prefix = 'enso-widget-area-';
	
	for ( $i = 1; $i <= $widget_area_columns; $i++ ) {
		// The same ID as what registered in theme-functions.php
		$widget_area_id = $widget_area_id_prefix . $i;
		// Check if the current sidebar has any widgets
		if ( is_active_sidebar( $widget_area_id ) ) {
			$has_any_widgets = true;
		}
	}
?>

<?php if ( $display_widget_area && $has_any_widgets ) : ?>
	<!-- Widget area -->
	<div class="container">
		<footer class="theme-widget-area clearfix row">
			<?php
				$col_num = 12 / $widget_area_columns;
				for ( $i = 1; $i <= $widget_area_columns; $i++ ) {
					// The same ID as what registered in theme-functions.php
					$widget_area_id = $widget_area_id_prefix . $i;				
					?>
					<div class="col-xs-12 col-sm-6 col-md-3">
						<?php dynamic_sidebar( $widget_area_id ); ?>
					</div>
					<?php
				}
			?>
		</footer>
	</div>
<?php endif; ?>