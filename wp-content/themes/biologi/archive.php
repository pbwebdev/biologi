<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Biologi
 * @since 1.0
 * @version 1.0
 */
?>
<?php get_header(); ?>
<main id="content-container">	
	<?php
	$count = 0;
	if ( have_posts() ) : ?>
		<div id="category-post" class="container">	
			<div class="row">
				<?php the_archive_title( '<div class="col-xs-12 page_title"><h1>', '</h1></div>' ); ?>		
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-8">	
					<?php				
					while ( have_posts() ) : the_post(); 
					if (has_post_format('video')) {						
						$post_youtube_video = get_field('post_youtube_video');
					?>					
						<div class="blog-item col-xs-12 col-sm-6">
							<span class="youtube-link" youtubeid="<?php echo $post_youtube_video;?>">
								<div class="featured-image">
									<?php
										$featured_image = get_image_url(get_post_thumbnail_id($post->ID), 'blog_thumbnail');
										if(!empty($featured_image)) {	  
									?>
										<img src="<?php echo $featured_image; ?>" class="img-responsive" />
									<?php } ?>					  
								</div>				
								<h4 class="widget-title"> <?php the_title(); ?></h4>
								<a class="read-more">View Video</a>	
							</span>
						</div>	
					<?php
						$count++;
						if($count == 2) {
							$count = 0;
							echo '<div class="clear"></div>'; 
							}
					} else { ?>
						<div class="blog-item col-xs-12 col-sm-6">
							<div class="featured-image">
								<?php
									$featured_image = get_image_url(get_post_thumbnail_id($post->ID), 'blog_thumbnail');
									if(!empty($featured_image)) {	  
								?>
									<a href="<?php the_permalink(); ?>"><img src="<?php echo $featured_image; ?>"  class="img-responsive" /></a>
								<?php } ?>					  
							</div>				
							<h4 class="widget-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>							
							<a href="<?php the_permalink(); ?>" class="read-more">Read More</a>
						</div>
					<?php 
						$count++;
						if($count == 2) {
							$count = 0;
							echo '<div class="clear"></div>'; 
						}	
					}
					?>	
					<?php endwhile; ?>
					<div class="numbers-pagination col-xs-12">
						<nav class="navigation pagination" role="navigation">					
							<div class="nav-links">
								<?php
									the_posts_pagination(array(
										'prev_text' =>  __( 'Previous', 'biologi' ),
										'next_text' =>  __( 'Next', 'biologi' ),								
									));
								?>
							</div>
						</nav>
					</div>
				</div>
				<div class="post-sidebar col-xs-12 col-sm-4">
					<?php get_search_form(); ?>
					<?php dynamic_sidebar('Blog Sidebar') ?>	
				</div>
			</div>
		</div>   
	<?php else : ?>	
		<h3>		    
			<?php echo 'No content was found.';?>		
		</h3>	       
	<?php endif; ?>
</main>	
<?php get_footer();