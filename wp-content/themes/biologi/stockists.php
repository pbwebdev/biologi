<?php 
/**
Template Name: Stockists Page 
*/
?>

<?php get_header(); ?>

<main id="content-container" class="stockists-page container">	
	<?php while ( have_posts() ) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php get_template_part( 'template-parts/content', 'page' ); ?>
			<div class="stockists-list">
            <?php 
                $country_list = get_field('country_list');
                if($country_list) {
                	foreach($country_list as $country) {
                		echo '<h3 class="country-name">' . $country['country'] . '</h3>';
                		$state_list = $country['state_list'];
                		foreach($state_list as $state) {
                		    echo '<h4 class="state-name accordion">' . $state['state'] . '</h4>';
                		    $locations_list = $state['locations_list'];
                		    echo '<div class="locations accordion-panel">';
                		    foreach($locations_list as $location) {
                		        echo "<div class='col-xs-12 col-sm-6 col-md-4 stockist'><strong><a href='". $location['website'] ."' target='_blank'>" . $location['stockist_name']  . "</a></strong><br />" .
                		        $location['address_line_1'] .'<br />'. $location['address_line_2'] .'</div>';
                		    }
                		    echo '</div>';
                		}
                	}
                }
            ?>
            </div>
		</article>
	<?php endwhile; ?>	
</main>

<script type="text/javascript">
    var acc = document.getElementsByClassName("accordion");
    var i;
    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        }
      });
    }
</script>

<?php get_footer(); ?>