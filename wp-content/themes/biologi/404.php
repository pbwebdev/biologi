<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Biologi
 * @since 1.0
 * @version 1.0
 */
?>
<?php get_header(); ?>
<main id="content-container" class="container">	
	<div class="row">	
		<section class="error-404 not-found">
			<div class="col-sm-12 page_title"> 
				<h1>Oops! That page can&rsquo;t be found.</h1>
			</div>
			<div class="col-sm-12">
				<p>It looks like nothing was found at this location. Maybe try a search?</p>		
				<?php get_search_form(); ?>
				<br /><br /><br />
				<p><a href="<?php echo site_url();?>">Back to Home</a></p>
			</div>
		</section>
	</div>
</main>	
<?php get_footer(); ?>