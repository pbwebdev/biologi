<?php 
/* 
Template Name: Testimonial Page 
*/ 
?>

<?php get_header(); ?>
<main id="content-container">	
	<div id="testimonials" class="container">
	   <div class="testimonial row">
			<?php
			$args = array(
				'post_type' => 'video_testimonial',
				'post_status' => 'publish',			
				'posts_per_page' => -1,			
				"order" => 'DESC'
			);
			
			$result = new WP_Query($args);
			$count = 0;

			while ($result->have_posts()) 
			{
				$result->the_post();
				?>		
				<div class="testimonial-item col-xs-12 col-sm-4">
					<div class="featured-image">
						<?php
						$featured_image = get_image_url(get_post_thumbnail_id($post->ID), 'large');
						$youtube_video = get_field('youtube_video');
						
						if (!empty($featured_image)) {
							if (!empty($youtube_video)) {										
						?>
								<span class="youtube-link" youtubeid="<?php echo $youtube_video;?>"><img src="<?php echo $featured_image; ?>" class="img-responsive" /></span>
						<?php } else { ?>					
								<img src="<?php echo $featured_image; ?>" class="img-responsive" />
						<?php } 
						} ?>						
					</div>
					<div class="testimonial-content"><?php the_content(); ?></div>					
					<h4 class="testimonial-title"><?php the_title(); ?></h4>	
				</div>	
				<?php
					$count++;
					if($count == 3) {
						$count = 0;
						echo '<div class="clear"></div>'; 
					}	
			}			
			wp_reset_postdata();
			?>
		</div>	
	</div>
</main>
<div class="clear"></div>
<?php get_footer(); ?>