<?php 
/* 
Template Name: Press Page
*/ 
?>

<?php get_header(); ?>

<main id="content-container">	
	<div id="press" class="container">
	<?php
		$args = array(
			'post_type' => 'press',
			'post_status' => 'publish',
			"orderby" => "post_date",
			'posts_per_page' => -1,
			"order" => 'DESC'		
		);
		$the_query = new WP_Query( $args );
		$prev_year = null;

		if($the_query->have_posts()) 
		{
			while($the_query->have_posts()) 
			{
				$the_query->the_post();
				$publish_date = strtotime($post->post_date);
				$this_year = date('Y', $publish_date);
				if ($prev_year != $this_year) // Year boundary
				{
					if (!is_null($prev_year)) {
						echo '</div>'; // A div is already open, close it first
					}
					?>
					<div class="press-list row">	
						<div class="press-year col-xs-12"><?php echo $this_year; ?></div>
						<?php } //close year boundary ?>
						<div class="press-item col-xs-12 col-sm-6 col-md-3">
							<div class="featured-image">
							<?php
								$file = get_field('file_upload');
								$publication_url = get_field('publication_url');
								$featured_image = get_image_url(get_post_thumbnail_id($post->ID), 'large');
								if (!empty($featured_image))
								{	  
									if (!empty($file))
									{
										echo '<a href="'.$file.'" target="_blank">';
										echo "<img src='".$featured_image."' class='img-responsive' />";
										echo "</a>";
									
									} 
									elseif (!empty($publication_url))
									{
										echo '<a href="'.$publication_url.'" target="_blank">';							
										echo "<img src='".$featured_image."' class='img-responsive' />";
										echo "</a>";
									}
									else
									{
										echo "<img src='".$featured_image."' class='img-responsive' />";
									}
								}
							?>   											
							</div>						
							<h3 class="press-title"><span>Seen in</span><br/><?php the_title(); ?></h3>
							<div class="press-date"><?php echo date('F Y', $publish_date); ?></div>
							<div class="product-title">
							<?php 
								$product = get_field('associated_products');						
								if(isset($product))
								{	
									$product_name = $product[0]->post_title;
									echo $product_name;
								}
							?>
							</div>
						</div>
						<?php
								$prev_year = $this_year;
							}
						?>
					</div>
	<?php   
		}
		wp_reset_postdata();
	?>
	</div>
</main>
<div class="clear"></div>
<?php get_footer(); ?>