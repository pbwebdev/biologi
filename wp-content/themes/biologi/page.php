<?php 
/**
 * The template for displaying all page content
 *
 * @since 1.0.0
 */

get_header(); ?>
<main id="content-container" class="container">	
	<?php while ( have_posts() ) : the_post(); // Start the loop ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php get_template_part( 'template-parts/content', 'page' ); ?>
			<!-- Comment Section -->
			<?php 
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}
			?>
		</article>
	<?php endwhile; // End of the loop ?>	
</main>
<!-- #content-container -->
<?php if(is_front_page()): ?>
<!-- Newsletter home popup -->
<div class="popup home-popup" data-popup="popup-1">
	<div class="popup-inner">
		<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
		<iframe src="<?php echo get_site_url(); ?>/wp-content/themes/biologi/blog-popup.php"></iframe>
	</div>
</div>
<?php endif;?>	
<?php get_footer(); ?>