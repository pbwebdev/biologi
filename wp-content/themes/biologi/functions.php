<?php

//Add new image sizes
add_image_size( 'blog_latest_post', 400, 460, true );
add_image_size( 'blog_thumbnail', 300, 225, true );
add_image_size( 'single_post_featured', 625, 465, true );

//Add video post type
add_theme_support( 'post-formats', array( 'video') );

//Get image URL
function get_image_url($attachment_id, $size) {
    if(empty($size)){
        $size = 'large';
    }
    $image_url = wp_get_attachment_image_src($attachment_id, $size );
    $image_url = $image_url[0];
    return $image_url;
}

//Register custom post type
if ( ! function_exists( 'reg_post_types' ) ){
    add_action('init', 'reg_post_types');	
	
    function reg_post_types(){
		$press_labels = array(
    		'name' => _x('Press', 'post type general name'),
    		'singular_name' => _x('Press Item', 'post type singular name'),
    		'add_new' => _x('Add New Press Item', 'bdes'),
    		'add_new_item' => __('Add Press Item'),
    		'edit_item' => __('Edit Press Item'),
    		'new_item' => __('New Press Item'),
    		'view_item' => __('View Press Item'),
    		'search_items' => __('Search Press'),
    		'not_found' =>  __('Nothing found'),
    		'not_found_in_trash' => __('Nothing found in Trash'),
    		'parent_item_colon' => ''
    	);
         
		$press_args = array(
    		'labels' => $press_labels,
    		'label'	=> 'Press',
    		'public' => true,
    		'publicly_queryable' => true,
    		'description' => 'Press',
    		'show_ui' => true,
    		'query_var' => true,
    		'capability_type' => 'post',
    		'hierarchical' => false,
            'menu_icon' => 'dashicons-id-alt',
    		'supports' => array('title','editor','revisions', 'thumbnail', 'page-attributes')
        );
        register_post_type( 'press' , $press_args );
		
		$testimonial_labels = array(
    		'name' => _x('Video Testimonial', 'post type general name'),
    		'singular_name' => _x('Video Testimonial', 'post type singular name'),
    		'add_new' => _x('Add New Video Testimonial', 'bdes'),
    		'add_new_item' => __('Add Video Testimonial'),
    		'edit_item' => __('Edit Video Testimonial'),
    		'new_item' => __('New Video Testimonial'),
    		'view_item' => __('View Video Testimonial'),
    		'search_items' => __('Search Video Testimonial'),
    		'not_found' =>  __('Nothing found'),
    		'not_found_in_trash' => __('Nothing found in Trash'),
    		'parent_item_colon' => ''
    	);
         
		$testimonial_args = array(
    		'labels' => $testimonial_labels,
    		'label'	=> 'Video Testimonial',
    		'public' => true,
    		'publicly_queryable' => true,
    		'description' => 'Video Testimonial',
    		'show_ui' => true,
    		'query_var' => true,
    		'capability_type' => 'post',
    		'hierarchical' => false,
            'menu_icon' => 'dashicons-id-alt',
    		'supports' => array('title','editor','revisions', 'thumbnail', 'page-attributes')
        );
        register_post_type( 'Video_Testimonial' , $testimonial_args );
        
		$faqs_labels = array(
    		'name' => _x('Video Faqs', 'post type general name'),
    		'singular_name' => _x('Video Faqs', 'post type singular name'),
    		'add_new' => _x('Add New Video Faqs', 'bdes'),
    		'add_new_item' => __('Add Video Testimonial'),
    		'edit_item' => __('Edit Video Faqs'),
    		'new_item' => __('New Video Faqs'),
    		'view_item' => __('View Video Faqs'),
    		'search_items' => __('Search Video Faqs'),
    		'not_found' =>  __('Nothing found'),
    		'not_found_in_trash' => __('Nothing found in Trash'),
    		'parent_item_colon' => ''
    	);
		
		$faqs_args = array(
		    'labels' => $faqs_labels,
    		'label'	=> 'Video Faqs',
    		'public' => true,
    		'publicly_queryable' => true,
    		'description' => 'Video Faqs',
    		'show_ui' => true,
    		'query_var' => true,
    		'capability_type' => 'post',
    		'hierarchical' => false,
            'menu_icon' => 'dashicons-id-alt',
    		'supports' => array('title','editor','revisions', 'thumbnail', 'page-attributes')
        );
        register_post_type( 'Video Faqs' , $faqs_args );        
	}
}

//Adds a css class to the body element
add_filter( 'body_class', 'body_class_page' );
function body_class_page($page_class)
{
	if(is_singular('page')) {
		global $post;
		$page_class[] = $post->post_name;
	}
	return $page_class;
}

//Remove archive labels
add_filter( 'get_the_archive_title', function ( $title ) {
	if( is_category() ) {
		$title = single_cat_title( '', false );
	}
	return $title;
});

// Adjust posts per page of the search template
add_action( 'pre_get_posts', 'enso_adjust_search_posts_per_page', 10);	
if ( ! function_exists( 'enso_adjust_search_posts_per_page')){
	function enso_adjust_search_posts_per_page( $query ) {
		
		if ( $query->is_search() ) {
			$query->set( 'posts_per_page', get_theme_mod( 'enso_ctmzr_general_options_number_of_posts_search_results', 5 ) );
		}
	}
}

//Adjust excerpt length
add_filter( 'excerpt_length', 'enso_custom_excerpt_length', 999 );
if ( ! function_exists('enso_custom_excerpt_length') ) {
	function enso_custom_excerpt_length( $length ) {
		global $options;
		$excerpt_length = (!empty($options['blog_excerpt_length'])) ? intval($options['blog_excerpt_length']) : 30; 
	    return $excerpt_length;
	}
	
}

//Custom excerpt ending
if(!function_exists('excerpt_more')){
	function excerpt_more( $more ) {
		return '...';
	}
}
add_filter('excerpt_more', 'excerpt_more');

// Load child theme styles
function biologi_enqueue_styles() {
	wp_register_style( 'bootstrap', get_theme_file_uri( '/css/bootstrap.min.css' ) );
	wp_enqueue_style( 'bootstrap' );
	wp_register_style( 'popup', get_theme_file_uri('/css/popup.css'), array(), cachebust() );
	wp_enqueue_style( 'popup' );
	wp_register_style( 'slick', get_theme_file_uri( '/css/slick.css' ) );
	wp_enqueue_style( 'slick' );
	wp_enqueue_style( 'child-style', get_stylesheet_uri(), array(), cachebust() );
}
add_action( 'wp_enqueue_scripts', 'biologi_enqueue_styles', 11 );

// Load child theme scripts
function biologi_enqueue_scripts() {
	wp_enqueue_script( 'biologi-js', get_stylesheet_directory_uri() . '/js/biologi.js', array('jquery'), cachebust());
	wp_register_script( 'bootstrap-min', get_theme_file_uri( '/js/bootstrap.min.js' ));
	wp_enqueue_script( 'bootstrap-min');
	wp_register_script( 'slick', get_theme_file_uri( '/js/slick.js' ));
	wp_enqueue_script( 'slick');
}
add_action( 'wp_enqueue_scripts', 'biologi_enqueue_scripts' );

function cachebust()
{
    // return wp_get_theme()->get('Version');
    return '2.8989.345';
}


// Register sidebar areas
if ( function_exists('register_sidebar') ) {
   register_sidebar(array(
   'name' => 'Below Header',
   'before_widget' => '<div id="biologi-below-header">',
   'after_widget' => '</div>',
   'before_title' => '<h2>',
   'after_title' => '</h2>'
    ));

   register_sidebar(array(
   'name' => 'Above Footer',
   'before_widget' => '<div id="biologi-above-footer">',
   'after_widget' => '</div>',
   'before_title' => '<h2>',
   'after_title' => '</h2>'
   ));

   register_sidebar(array(
   'name' => 'Below Footer',
   'before_widget' => '<div id="biologi-below-footer">',
   'after_widget' => '</div>',
   'before_title' => '<h2>',
   'after_title' => '</h2>'
   ));
   
   register_sidebar(array(
   'name' => 'Blog Sidebar',
   'before_widget' => '<div class="blog-side-widget">',
   'after_widget' => '</div>',
   'before_title' => '<h2>',
   'after_title' => '</h2>'
   ));
}

// Add new menu areas
function enso_register_menus() {
  register_nav_menus(
    array(
      'main-menu-left' => __( 'Main Menu - Left' ),
      'main-menu-right' => __( 'Main Menu - Right' ),
    )
  );
}
add_action( 'init', 'enso_register_menus' );

// Add Biologi logo to login page
function biologi_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/Biologi-logo.png);
            padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'biologi_login_logo' );

//Change Gravity forms validation message for serum quiz
add_filter( 'gform_validation_message_2', 'change_message', 10, 2 );
function change_message( $message, $form ) {
    return "<div class='validation_error'>Please select an option.</div>";
}

// Declare WooCommerce support
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php).
// Used in conjunction with https://gist.github.com/DanielSantoro/1d0dc206e242239624eb71b2636ab148
// Compatible with 3.0.1+, for lower versions, remove "woocommerce_" from the first mention on Line 4
add_filter('woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
function woocommerce_header_add_to_cart_fragment( $fragments ) {
  global $woocommerce;
  
  ob_start();
  
  ?>
  <li class="biologi-cart"><a href="<?php echo wc_get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><span class="hidden-xs visible-sm-* visible-md-* visible-lg-*">Cart</span> <span><i class="fa fa-shopping-cart visible-xs-* hidden-sm hidden-md hidden-lg" aria-hidden="true"></i></span> (<?php echo $woocommerce->cart->cart_contents_count;?>)</a></li>
  <?php
  
  $fragments['li.biologi-cart'] = ob_get_clean();
  
  return $fragments;
}

// Change number of products per row to 3
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
  function loop_columns() {
    return 3; // 3 products per row
  }
}

// Remove parent editor styles and add child editor stylesheet
add_action( 'admin_init', 'my_remove_parent_styles' );
function my_remove_parent_styles() {
  remove_editor_styles();
  add_editor_style( 'css/biologi-editor-style.css' );
}

// Move rating above title of product
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_rating', 10);

// Display product icon above title if set
add_action('woocommerce_single_product_summary', 'display_product_icon', 4);
add_action('woocommerce_before_shop_loop_item_title', 'display_product_icon', 10);
function display_product_icon() {
    $product_icon = get_field('product_icon');
    
    if($product_icon) {
      echo '<img src="'.$product_icon.'" class="product-icon" alt="product icon" />';
    }
}

// Display product use for text after title if set
add_action('woocommerce_after_shop_loop_item_title', 'display_product_use_for', 7);
function display_product_use_for() {
    $use_for = get_field('use_for');
    
    if($use_for) {
      echo "<div class='product-use-for'><strong>Use for:</strong>".$use_for."</div>";
    }
}

// Display product ingredient after title if set
add_action('woocommerce_after_shop_loop_item_title', 'display_product_ingredient', 6);
add_action('woocommerce_single_product_summary', 'display_product_ingredient', 6);
function display_product_ingredient() {
    $product_ingredient = get_field('product_ingredient');
    
    if($product_ingredient) {
      echo '<h5 class="product-ingredient">'.$product_ingredient.'</h5>';
    }
}

// Display product volume after price if set
add_action('woocommerce_single_product_summary', 'display_product_volume', 7);
function display_product_volume() {
    $product_volume = get_field('product_volume');
    if($product_volume) {
      echo '<span class="product-volume">'.$product_volume.'</span>';
    }
}

// Display view product button with add to cart button
add_action('woocommerce_after_shop_loop_item', 'display_view_product_button', 6);
function display_view_product_button() {
    global $product;
    
    echo '<a class="button view_product_button" href="'.$product->get_permalink().'">Learn More</a>';
}

// Add currency letters before price
function addPriceSuffix($format, $currency_pos) {
  switch ( $currency_pos ) {
    case 'left' :
      $currency = get_woocommerce_currency();
      $format = '<span class="currency-code">'.$currency.'</span>'. '%1$s%2$s&nbsp;';
    break;
  }
 
  return $format;
}
add_action('woocommerce_price_format', 'addPriceSuffix', 1, 2);

// Move product short description up
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 7);

// Move related products out of product div
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
//add_action('woocommerce_after_single_product', 'woocommerce_output_related_products', 5);

// Move description out of tabs and insert below summary
function woocommerce_template_product_description() {
  echo '<div class="full-description">';
  woocommerce_get_template( 'single-product/tabs/description.php' );
  echo '</div>';
}
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_product_description', 35 );

// Move tabs below main section
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
add_action( 'woocommerce_after_single_product', 'woocommerce_output_product_data_tabs', 4 );

// Add extra image section to product page
add_action( 'woocommerce_after_single_product', 'woocommerce_output_extra_image_section', 3 );
function woocommerce_output_extra_image_section() {
  $product_extra_image = get_field('product_extra_image');
    if($product_extra_image) {
      echo '<div id="product-extra-image"><img alt="Biologi product" src="'.$product_extra_image.'" /></div>';
    }
}

// Remove description tab
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
function woo_remove_product_tabs( $tabs ) {
 unset( $tabs['description'] );
 unset( $tabs['additional_information']);
 return $tabs;
}

// Edit WooCommerce breadcrumb separator
add_filter( 'woocommerce_breadcrumb_defaults', 'change_breadcrumb_delimiter' );
function change_breadcrumb_delimiter( $defaults ) {
  // Change the breadcrumb delimeter from '/' to '>'
  $defaults['delimiter'] = ' <span class="separator">&gt;</span> ';
  return $defaults;
}

// Add wrapper around review meta and rating
add_action('woocommerce_review_before_comment_meta', 'wrap_rating_meta_start', 9);
function wrap_rating_meta_start() {
  echo '<div class="rating-meta-wrapper">';
}
add_action('woocommerce_review_before_comment_text', 'wrap_rating_meta_end', 9);
function wrap_rating_meta_end() {
  echo '</div>';
}


// Enable Woocommerce features
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );

// Change "Place Order" button text on  Woocommerce checkout page
add_filter( 'woocommerce_order_button_text', 'woo_custom_order_button_text' ); function woo_custom_order_button_text() { return __( 'Proceed to payment', 'woocommerce' ); }

//change 'Ship to a different address' option on checkout form to 'Is this a gift'
add_filter('gettext', 'custom_strings_translation', 20, 3);
function custom_strings_translation( $translated_text, $text, $domain ){
  switch ( $translated_text ){
    case 'Ship to a different address?' : 
      $translated_text =  __( 'Ship to a different address / Is this a gift?', '__x__' ); 
      break;
  }
  return $translated_text;
}


//Create new authority to leave field in checkout form
add_action( 'shipping_authority_to_leave', 'ag_authority_to_leave_field' );
function ag_authority_to_leave_field( $checkout ) {	
	woocommerce_form_field( 'authority_to_leave', array(
		'type'          => 'checkbox',
		'class'         => array('authority_to_leave_field'),
		'label'         => __('Authority to leave'),
		'placeholder'   => __(''),
		'required'      => false,
		), $checkout->get_value( 'authority_to_leave' ));	
}


//Add authority to leave field to order data
add_action( 'woocommerce_checkout_update_order_meta', 'ag_authority_to_leave_field_update_order_meta' );
function ag_authority_to_leave_field_update_order_meta( $order_id ) {
	if ( !empty( $_POST['authority_to_leave'] ) ) {
		update_post_meta( $order_id, 'authority_to_leave', sanitize_text_field( $_POST['authority_to_leave'] ) );
	}
}
  
	
//Display authority to leave field in admin view order
add_action( 'woocommerce_admin_order_data_after_shipping_address', 'ag_authority_to_leave_field_display_admin_order_meta', 10, 1 );
function ag_authority_to_leave_field_display_admin_order_meta($order){
	$ag_authority_to_leave = get_post_meta( $order->get_id(), 'authority_to_leave', true );
	if($ag_authority_to_leave == 1 || $ag_authority_to_leave == true){
		$ag_authority_to_leave = 'Accepted';
	}
	else {
		$ag_authority_to_leave = 'Not accepted';
	}
	echo '<p><strong>'.__('Authority to Leave').':</strong><br /> ' . $ag_authority_to_leave . '</p>';
}


//Display authority to leave field in admin email
add_action( 'woocommerce_email_after_order_table', 'ag_authority_to_leave_field_display_email_order_meta', 10, 1 );
function ag_authority_to_leave_field_display_email_order_meta($order){
	$ag_authority_to_leave = get_post_meta( $order->get_id(), 'authority_to_leave', true );
	if($ag_authority_to_leave == 1 || $ag_authority_to_leave == true){
		$ag_authority_to_leave = 'Accepted';
	}
	else {
		$ag_authority_to_leave = 'Not accepted';
	}
	echo '<p><strong>'.__('Authority to Leave').':</strong> ' . $ag_authority_to_leave . '</p>';
}


//Create gift wrap and product brochure checkboxes on checkout form
add_action( 'shipping_gift_to_wrap', 'ag_gift_to_wrap_field' );
function ag_gift_to_wrap_field( $checkout ) {
	$user = wp_get_current_user();
	if(!in_array("wholesale_customer", $user->roles))
	{	
		echo '<div class="packing-options">';
		echo '<h3>Packing Options</h3>';
			$gift_to_wrap = !empty( WC()->session->get('gift_to_wrap') ) ? '1': '0';
			woocommerce_form_field( 'gift_to_wrap', array(
				'type'          => 'checkbox',
				'label'         => __('Do you want your order to be gift wrapped with Biologi tissue paper and box (<strong>$5 fee</strong>)?'),
				'required'  => false,		
			), $gift_to_wrap);	
			
			 woocommerce_form_field( 'product_brochure', array(
				'type'          => 'checkbox',
				'label'         => __('I am a returning customer so please don’t send the Biologi Product Brochure.'),
				'required'  => false,		
			), $checkout->get_value( 'product_brochure' ));
		echo '</div>';
	}
}


//Add gift wrap and product brochure fields to order data
add_action( 'woocommerce_checkout_update_order_meta', 'ag_gift_to_wrap_field_update_order_meta' );
function ag_gift_to_wrap_field_update_order_meta( $order_id ) {
	if ( isset( $_POST['gift_to_wrap'] ) ) {
		update_post_meta( $order_id, 'gift_to_wrap', sanitize_text_field( $_POST['gift_to_wrap'] ) );
	}
	if ( isset( $_POST['product_brochure'] ) ) {
		update_post_meta( $order_id, 'product_brochure', sanitize_text_field( $_POST['product_brochure'] ) );
	}
}
  
	
//Display gift wrap and product brochure fields in admin view order
add_action( 'woocommerce_admin_order_data_after_shipping_address', 'ag_gift_to_wrap_field_display_admin_order_meta', 10, 1 );
function ag_gift_to_wrap_field_display_admin_order_meta($order){
	$user = $order->get_user();
	if ($user != false && in_array("wholesale_customer", $user->roles)) {
		return;
	}
	else {	
		$gift_to_wrap = get_post_meta( $order->get_id(), 'gift_to_wrap', true );
		$product_brochure = get_post_meta( $order->get_id(), 'product_brochure', true );
		
		if($gift_to_wrap == 'Yes') {	
			echo '<p><strong>Gift wrap order:</strong><br />Yes</p>';
		}
		else if($gift_to_wrap == 'No') {	
			echo '<p><strong>Gift wrap order:</strong><br />No</p>';
		}
		else if($gift_to_wrap == '1') {	
			echo '<p><strong>Gift wrap order:</strong><br />Yes</p>';
		}
		else {
			echo '<p><strong>Gift wrap order:</strong><br />No</p>';
		}
		
		if($product_brochure == '1') {
			echo '<p><strong>Product brochure:</strong><br />Not required</p>';
		}		
		else {
			echo '<p><strong>Product brochure:</strong><br />Required</p>';
		}
	}
}


//Display gift wrap and product brochure fields in admin email
add_action( 'woocommerce_email_after_order_table', 'ag_gift_to_wrap_field_display_email_order_meta', 10, 1 );
function ag_gift_to_wrap_field_display_email_order_meta($order){
	$user = $order->get_user();
	if ($user != false && in_array("wholesale_customer", $user->roles)) {
		return;
	}
	else {	
		$gift_to_wrap = get_post_meta( $order->get_id(), 'gift_to_wrap', true );
		$product_brochure = get_post_meta( $order->get_id(), 'product_brochure', true );
		if(!empty($gift_to_wrap)) {	
			echo '<p><strong>Gift wrap order: </strong>Yes</p>';
		}
		else {
			echo '<p><strong>Gift wrap order: </strong>No</p>';
		}
		if(!empty($product_brochure)) {
			echo '<p><strong>Product brochure: </strong>Not required</p>';
		}		
		else {
			echo '<p><strong>Product brochure: </strong>Required</p>';
		}
	}
}	


//Add gift wrap and product brochure fields to order notes
add_action( 'woocommerce_new_order', 'update_gift_wrap_to_order_notes',  10, 1  );
function update_gift_wrap_to_order_notes($order_id){ 
	$user = wp_get_current_user();	
	if(!in_array("wholesale_customer", $user->roles)){
		$order = wc_get_order($order_id);
		if ($order !== false) {
    		$gift_to_wrap = sanitize_text_field($_POST['gift_to_wrap']);
    		$product_brochure = sanitize_text_field($_POST['product_brochure']);
    		
    		if(!empty($gift_to_wrap)){	
    			$gift_wrap = 'Gift wrap order: Yes'; 
    		}
    		else {
    			$gift_wrap = 'Gift wrap order: No'; 
    		}
    		
    		if(!empty($product_brochure)){
    			$product_brochure = 'Product brochure: Not required'; 
    		}	
    		else {
    			$product_brochure = 'Product brochure: Required'; 
    		}
    		
    		$order_data = array(
    			'order_id' => $order_id,
    			'customer_note' => $order->get_customer_note() .'<br/>'. $gift_wrap .'<br/>'. $product_brochure
    		);	
    		wc_update_order($order_data);
		}
	}
}

//Disable Plugin Update 
function disable_plugin_updates( $value ) {
  if ( isset($value) && is_object($value) ) {
    if ( isset( $value->response['birthday-discount-vouchers/birthday-discount-vouchers.php'] ) ) {
      unset( $value->response['birthday-discount-vouchers/birthday-discount-vouchers.php'] );
    }
  }
  return $value;
}
add_filter( 'site_transient_update_plugins', 'disable_plugin_updates' );


//Add custom class to Gravity Form widget
add_filter( 'dynamic_sidebar_params', 'blog_sidebar_widgets' );
function blog_sidebar_widgets($params) {	 
	if ($params[0]['widget_name'] == 'Form') {
		$params[0] = array_replace($params[0], array('before_widget' => str_replace("blog-side-widget", "blog-side-widget skincare-mistakes-form", $params[0]['before_widget'])));
	}
	return $params;
}


//Only show cheapest shipping method
add_filter( 'woocommerce_package_rates', 'show_cheapest_shipping_rate' , 10, 1 );
function show_cheapest_shipping_rate( $rates ) {
	if (!empty($rates)) {	
		$cost_array = array();	
		
		foreach ( $rates as $rate_id => $rate ) {			
			$cost_array[] = array("cost"=>$rate->cost, "id" => $rate->id);
		}
		
		$min_cost = min($cost_array);
		$cheapest_method = array();
	
		foreach ( $rates as $rate_id => $rate ) {	
			if(!empty($min_cost['cost']) && $min_cost['id'] === $rate->id)
			{
				$cheapest_method[ $rate_id ] = $rate;
				break;
			}
		}

		return ! empty( $cheapest_method ) ? $cheapest_method : $rates;
	}
	else {
		return $rates;
	}
}

// Add product Quantity Label
add_action( 'woocommerce_before_add_to_cart_quantity', 'product_qty_label' );
function product_qty_label() {
 echo '<div class="qty">QTY </div>'; 
}

//Add post slug to body class
function add_slug_body_class( $classes ) {
    global $post;
    if ( isset( $post ) ) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }
    return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

//Remove SKU from product page
add_filter( 'wc_product_sku_enabled', 'remove_product_page_sku' );
function remove_product_page_sku( $enabled ) {
    if ( !is_admin() && is_product() ) {
        return false;
    }
    return $enabled;
}

//Add Afterpay payment info to custom hook on individual product pages
if (class_exists(WC_Gateway_Afterpay)) {
	add_action( 'custom_afterpay_hook', array(WC_Gateway_Afterpay::getInstance(), 'print_info_for_product_detail_page'), 10, 0 );
}

/*
//Display 3 upsell products on product page  
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
add_action( 'woocommerce_after_single_product_summary', 'display_three_upsell_products', 5 ); 
function display_three_upsell_products() {
	woocommerce_upsell_display( 3,3 ); // Display max 3 products, 3 per row
}
*/

//Show latest product reviews on top
add_filter( 'woocommerce_product_review_list_args', 'newest_reviews_first' );
function newest_reviews_first($args) {
    $args['reverse_top_level'] = true;
    return $args;
}

//Show first review in product summary on product page
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating',10,0);
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating',8,0);
add_action('woocommerce_single_product_summary', 'show_first_review', 9, 0);
function show_first_review() {
	global $product;
	$id = $product->get_id();
	$reviews = get_comments( array( 'number' => 1, 'status' => 'approve', 'post_status' => 'publish', 'post_type' => 'product', 'post_id' => $id,'orderby' => 'comment_date', 'order' => 'DESC' ) );	
	if(!empty($reviews) && comments_open())
	{	
		foreach ( (array) $reviews as $review ) {
			echo "<div class='comment-box'>";
			echo "<div class='comment-more'>".'<strong>'.get_comment_author($review->comment_ID).'</strong>'."<span class='comment-date'>" . date("F d Y", strtotime($review->comment_date))."</span>".'<br/>'.$review->comment_content."</div>";		
			echo "</div>";	
		}
	}	
}

//Setup custom fields options page
if (function_exists('acf_add_options_sub_page')){
    acf_add_options_sub_page(array(
        'title' => 'Site Settings',
        'parent' => 'options-general.php',
        'capability' => 'edit_posts'
    ));
}

//Add custom text on checkout form
function add_checkout_shipping_notes() {
	echo '<p style="margin-top:20px;"><strong>Shipping Information</strong></p>'; 
	echo '<ol>';	
	echo '<li>For international orders you may be charged duties or tax on your order. We are not responsible for those fees and these will be payable by you.</li>';
	echo '<li>Orders will be sent to the delivery address that you have submitted with your order. We cannot be held responsible if that delivery address is incorrect.</li>';
	echo '<li>In a bush fire impacted area? <a href="https://auspost.com.au/about-us/news-media/important-updates/bushfire-impacts-nsw" target="_blank">Check impacted delivery areas here</a>.</li>';
	echo '</ol>';
}
add_action( 'woocommerce_checkout_shipping', 'add_checkout_shipping_notes', 10, 1 ); 

/*
//Add custom text before billing details on checkout form
function billing_custom_text() {
	date_default_timezone_set("Australia/Sydney");
	$now = date('Y-m-d H:i:s');
	$start_date = '2019-07-15 00:00:00';	
	$end_date = '2019-07-24 23:59:00';	
	if(strtotime($now) >= strtotime($start_date) && strtotime($now) <= strtotime($end_date)){
		echo 'If you are eligible for the complimentary travel pack it will automatically be added to your order.';
	}	
}
add_filter( 'woocommerce_checkout_billing', 'billing_custom_text' );	

//Add custom text before cart table on cart page
function cart_table_custom_text() {
	date_default_timezone_set("Australia/Sydney");
	$now = date('Y-m-d H:i:s');
	$start_date = '2019-07-15 00:00:00';	
	$end_date = '2019-07-24 23:59:00';	
	if(strtotime($now) >= strtotime($start_date) && strtotime($now) <= strtotime($end_date)){
		echo '<p>Travel mini pack will be automatically added to your order if eligible, no coupon is required.</p>';
	}	
}
add_filter( 'woocommerce_before_cart_table', 'cart_table_custom_text' );	
*/

//Change the default country on Wholesale Lead Capture registration form
function wwsSetRegistrationDefaultCountry() {
    if ( is_page( 'wholesale-registration' ) ) {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery('select#wwlc_country').val('AU');
                jQuery('select#wwlc_country').trigger('change');
            });
        </script>
        <?php
    }
}
add_action( 'wp_footer', 'wwsSetRegistrationDefaultCountry', 99 );

/*
//Add Hubspot newsletter signup checkbox on checkout page
add_action('woocommerce_after_checkout_billing_form', 'hubspot_newsletter');
function hubspot_newsletter( $checkout ) {
    $checked = $checkout->get_value( 'hubspot_woocommerce_newsletter' ) ? $checkout->get_value( 'hubspot_woocommerce_newsletter' ) : 1;
	woocommerce_form_field( 'hubspot_woocommerce_newsletter', array(
		'type'          => 'checkbox',
		'label'         => __("We'd love to stay in touch!"),
		'required'  => false,		
	), $checked);	
}

//Add Hubspot newsletter field to order data
add_action( 'woocommerce_checkout_update_order_meta', 'hubspot_newsletter_update_order_meta' );
function hubspot_newsletter_update_order_meta( $order_id ) {
	if (isset( $_POST['hubspot_woocommerce_newsletter'] ) ) {
		update_post_meta( $order_id, 'hubspot_woocommerce_newsletter', sanitize_text_field( $_POST['hubspot_woocommerce_newsletter'] ) );
	}
}
require_once('hubspot_api.php');
*/

//Set contact name in Xero for new retail customers
add_filter( 'woocommerce_xero_invoice_contact', 'set_xero_invoice_contact_name' );
function set_xero_invoice_contact_name($contact)
{
	$user = wp_get_current_user();
	if(!in_array("wholesale_customer", $user->roles))
	{	
		$contact->set_name('Woocommerce Online Sales');		
	}
	return $contact;
}

//Set contact name in Xero for existing retail customers
add_filter( 'woocommerce_xero_contact_name', 'set_xero_contact_name' );
function set_xero_contact_name($name)
{
	$user = wp_get_current_user();
	if(!in_array("wholesale_customer", $user->roles))
	{	
		$name = 'Woocommerce Online Sales';				
	}
	return $name;
}

//Create Reference/PO Number textfield on checkout form
add_action( 'woocommerce_after_checkout_billing_form', 'ag_reference_number_field' );
function ag_reference_number_field($checkout) {
	$user = wp_get_current_user();
	if(in_array("wholesale_customer", $user->roles)){
		echo '<div class="reference_number">';		
			woocommerce_form_field( 'reference_number', array(
				'type'          => 'text',
				'class'  => array('my-field-class form-row-wide'),
				'label'         => __('Reference/PO Number'),
				'required'  => false
			), $checkout->get_value( 'reference_number' ));
		echo '</div>';	
	}	
}

//Add Reference/PO Number to order data
add_action( 'woocommerce_checkout_update_order_meta', 'ag_reference_number_field_update_order_meta' );
function ag_reference_number_field_update_order_meta( $order_id ) {
	if (isset( $_POST['reference_number'] ) ) {
		update_post_meta( $order_id, 'reference_number', sanitize_text_field( $_POST['reference_number'] ) );
	}
}  
	
//Display Reference/PO Number in admin view order
add_action( 'woocommerce_admin_order_data_after_shipping_address', 'ag_reference_number_field_display_admin_order_meta', 10, 1 );
function ag_reference_number_field_display_admin_order_meta($order){
	$reference_number = get_post_meta( $order->get_id(), 'reference_number', true );	
	if(!empty($reference_number)) {	
		echo '<p><strong>Reference/PO Number:</strong><br />'.$reference_number.'</p>';
	}		
}

//Display Reference/PO Number in admin email
add_action( 'woocommerce_email_after_order_table', 'ag_reference_number_field_display_email_order_meta', 10, 1 );
function ag_reference_number_field_display_email_order_meta($order){
	$reference_number = get_post_meta( $order->get_id(), 'reference_number', true );
	if(!empty($reference_number)){	
		echo '<p><strong>Reference/PO Number:</strong> '.$reference_number . '</p>'; 
	}
}

//Add loading icon to woocommerce checkout form
/* 
add_action( 'woocommerce_checkout_before_order_review', 'checkout_selected_loading', 20, 1 ); 
function checkout_selected_loading() {
	echo '<div class="blockUI blockOverlay overlay-background"></div>';
}	
*/

//Add Fee and Calculate Total
//Based on session's "radio_chosen" Calculate New Total 
add_action( 'woocommerce_cart_calculate_fees', 'bbloomer_checkout_radio_choice_fee', 20, 1 ); 
function bbloomer_checkout_radio_choice_fee( $cart ) {
  	if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;

	$gift_to_wrap = WC()->session->get( 'gift_to_wrap' );

	if ( "gift_to_wrap" == $gift_to_wrap ) {
		$gift_wrap_fee = 5;		
		$cart->add_fee( __('Gift Wrap Fee', 'woocommerce'), $gift_wrap_fee);  
	}
}

//Refresh Checkout if Radio Changes Uses jQuery 
add_action( 'wp_footer', 'checkbox_choice_refresh' ); 
function checkbox_choice_refresh() {
if ( is_checkout() ) {
?>
    <script type="text/javascript">
    jQuery( function($){
		$('.woocommerce form .form-row #gift_to_wrap').click(function() {				
			if($(this).is(":checked") == true) {	
				var checkbox_id = 'gift_to_wrap'; 
			}
			else {
				var checkbox_id = 0;  
			}
			//$('.woocommerce .overlay-background').show();	     
			$.ajax({
				type: 'POST',
				url: wc_checkout_params.ajax_url,
				data: {
					'action': 'woo_get_ajax_data',
					'checkbox1': checkbox_id,
				},
				success: function (result) {	
					//$('.woocommerce .overlay-background').hide();	
					$('body').trigger('update_checkout');
				},
				error: function(result){
				   //$('.woocommerce .overlay-background').hide();	
				}	
			}); 	
		});	
	});	
    </script>
    <?php  
	} //if
}

//Add Radio Choice to Session Uses Ajax 
add_action( 'wp_ajax_woo_get_ajax_data', 'bbloomer_checkout_radio_choice_set_session' );
add_action( 'wp_ajax_nopriv_woo_get_ajax_data', 'bbloomer_checkout_radio_choice_set_session' );
function bbloomer_checkout_radio_choice_set_session() {
   if ( isset($_POST['checkbox1']) ){
        $gift_to_wrap = sanitize_key( $_POST['checkbox1'] );
		if($gift_to_wrap == 'gift_to_wrap'){	
        	WC()->session->set('gift_to_wrap', $gift_to_wrap);
		}
		else{	
        	WC()->session->set('gift_to_wrap', null);
		}
	}	
    die();
}

//Add gift wrap checkbox to cart page
add_action( 'woocommerce_cart_collaterals', 'ag_cart_gift_to_wrap_field',5);
function ag_cart_gift_to_wrap_field() {
	$user = wp_get_current_user();
	if(!in_array("wholesale_customer", $user->roles))
	{	
		echo '<div class="gift-grey-box">';
		echo '<h2>GIFT WRAPPING</h2>';		
		echo '<img src="'.get_stylesheet_directory_uri().'/images/gift-box-open-grey.jpg" class="gift-wrap-img" />';
		$gift_to_wrap = !empty( WC()->session->get('gift_to_wrap') ) ? '1': '0';
		woocommerce_form_field( 'gift_to_wrap', array(
			'type'          => 'checkbox',
			'label'         => __('<strong>Gift wrap your order with tissue paper and box for an additional $5</strong>'),
			'required'  	=> false,		
		), $gift_to_wrap);				
		echo '</div>';
	}
}

//Refresh cart if gift wrap checkbox clicked 
add_action( 'wp_footer', 'cart_gift_wrap_checkbox_refresh' ); 
function cart_gift_wrap_checkbox_refresh() {
if ( is_cart() ) {
?>
    <script type="text/javascript">
    jQuery( function($){
		$('.woocommerce-cart #gift_to_wrap').click(function() {				
			if($(this).is(":checked") == true) {	
				var checkbox_id = 'gift_to_wrap';				  					
			}
			else {
				var checkbox_id = 0; 				
			}
			$.ajax({
				type: 'POST',
				url:  wc_add_to_cart_params.ajax_url,
				data: {
					'action': 'woo_get_ajax_data',
					'checkbox1': checkbox_id,
				},
				success: function (result) {					    
					jQuery("[name='update_cart']").prop("disabled", false);
				    jQuery("[name='update_cart']").trigger("click"); 	
				},
				error: function(result){
				   jQuery("[name='update_cart']").prop("disabled", true);	
				}	
			}); 	
		});	
	});	
    </script>
    <?php  
	} //if
}

/*
//Hide coupon code for Matt & Nat Limited Edition Gift Set
add_filter( 'woocommerce_coupons_enabled','hide_coupon_on_cart' );
function hide_coupon_on_cart( $enabled ) {
	$productid = 83102;
	$cart = WC()->cart->get_cart();
	foreach ( $cart as $id => $cart_item ) {
		if($cart_item['product_id'] == $productid)	
			return false;
	}	
	return $enabled;
}
*/

//Make coupons invalid at product level
add_filter('woocommerce_coupon_is_valid_for_product', 'set_coupon_validity_for_excluded_products', 12, 4);
function set_coupon_validity_for_excluded_products($valid, $product, $coupon, $values ){    
	$disabled_product = 4215; //Gift voucher	
    if($product->get_id() == $disabled_product)
		$valid = false;

    return $valid;
}

//Set the product discount amount to zero
add_filter( 'woocommerce_coupon_get_discount_amount', 'zero_discount_for_excluded_products', 12, 5 );
function zero_discount_for_excluded_products($discount, $discounting_amount, $cart_item, $single, $coupon ){   	
   	$disabled_product = 4215; //Gift voucher
	if( $cart_item['product_id'] == $disabled_product )
        $discount = 0;

    return $discount;
}

/*
//Hide coupon field 
function disable_coupon_field_on_cart( $enabled ) {
	$enabled = false;
	return $enabled;
}
add_filter( 'woocommerce_coupons_enabled', 'disable_coupon_field_on_cart' );
*/


//Add Google Customer Reviews survey opt-in on thank you page 
add_action( 'woocommerce_thankyou', 'add_google_customer_review_optin' );
function add_google_customer_review_optin($order_id){
	$order = wc_get_order($order_id);
	if ($order !== false){ 	
		$country_code = $order->get_shipping_country();	
		$order_date = $order->order_date;
		$delivery_date = date_create($order_date);
		if($country_code == 'AU'){
			date_add($delivery_date, date_interval_create_from_date_string('2 days'));
		}
		else {
			date_add($delivery_date, date_interval_create_from_date_string('8 days'));
		}
		$date = date_format($delivery_date, 'Y-m-d');
		?>
		<script src="https://apis.google.com/js/platform.js?onload=renderOptIn" async defer></script>
		<script>
		  window.renderOptIn = function() {
			window.gapi.load('surveyoptin', function() {
			  window.gapi.surveyoptin.render(
				{
				  "merchant_id": 140947956,
				  "order_id": "<?php echo $order_id; ?>",
				  "email": "<?php echo $order->get_billing_email(); ?>",
				  "delivery_country": "<?php echo $country_code; ?>",
				  "estimated_delivery_date": "<?php echo $date; ?>"
				});
			});
		  }
		</script>
	<?php
	}
}

//Add custom note to cart page
function add_cart_notes(){
    $user = wp_get_current_user();	
	if($user->roles[0] != 'wholesale_customer')
	{   
        echo '<div class="cart-shipping-notes ">';
        echo '<div class="shipping-truck"><i class="fa fa-truck" aria-hidden="true"></i></div>';
        echo '<div class="shipping-text"><p>Free shipping on all orders over $150 Australia Wide and $300 International</p></div>';
    	echo '</div>';
	}
}
add_action('woocommerce_after_cart_table', 'add_cart_notes', 9, 0);


//Prevent searching for wholesale word
add_action('wp', 'check_search');
function check_search() {
    global $wp_query;

    if (!$s = get_search_query())
        return false;
    
    if (preg_match('/wholesale/i', $s)) {
        $wp_query->set_404();
        status_header(404);
        get_template_part(404);
        exit();
    }
}

//add free product to cart if sub total is more than $100
/*
add_action('woocommerce_review_order_before_cart_contents', 'add_promo_product_to_cart', 10, 0);
add_action('woocommerce_before_cart_table', 'add_promo_product_to_cart', 10, 0);
function add_promo_product_to_cart(){
    $user = wp_get_current_user();	
	if($user->roles[0] != 'wholesale_customer')
	{
        $free_product_id = '85475';
    	$cart_subtotal = WC()->cart->get_subtotal();
    	$min_total = '100.00';
    	$has_coupons = count(WC()->cart->applied_coupons) > 0 ? true:false;
    	
    	if($cart_subtotal >= $min_total) {
    	    if(!$has_coupons) {
    	        add_free_product_to_cart($free_product_id);
    	    }
    	    else {
    	        remove_free_product_from_cart($free_product_id);
    	    }
    	}
    	else {
    	    remove_free_product_from_cart($free_product_id);
    	}
	}
}
*/

//add product to cart
function add_free_product_to_cart($free_product_id){
    if(!check_product_in_cart($free_product_id)) {
        WC()->cart->add_to_cart($free_product_id);
	}
}

//check if product exists in cart
function check_product_in_cart($product_id) {
   $in_cart = false;
   foreach( WC()->cart->get_cart() as $cart_item ) {
        if ( $cart_item['product_id'] == $product_id ) {
            $in_cart = true;
        }
    }
    
    return $in_cart;
}

//remove product from cart
function remove_free_product_from_cart($free_product_id){
    foreach( WC()->cart->get_cart() as $cart_item ) {
        if ( $cart_item['product_id'] == $free_product_id ) 
            WC()->cart->remove_cart_item($cart_item['key']);
    }    	
}

//Show 20% off message on Bqk product
add_action( 'woocommerce_after_shop_loop_item', 'shop_sale_price', 1);
add_action( 'woocommerce_single_product_summary', 'shop_sale_price', 15);
function shop_sale_price() {
	global $product;
	if ( $product->is_on_sale() && $product->get_id() =='44117') {
		echo '<p class="sale-message">20% Off</p>';
	}		
}



