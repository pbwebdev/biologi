<?php 
/*
Template Name: About Us Page
*/
?>

<?php get_header(); ?>

<main class="container">	
	<article id="post-572" class="post-572 page type-page status-publish has-post-thumbnail hentry post-item">
		<div class="post-content-container clearfix">
			<div class="post-content-wrapper">
				<div class="post-content">
			
				<div id="pl-572" class="panel-layout">
					<div id="pg-572-0" class="panel-grid panel-has-style">
						<div class="panel-row-style panel-row-style-for-572-0">
							<div id="pgc-572-0-0" class="panel-grid-cell">
							<div id="panel-572-0-0-0" class="so-panel widget widget_sow-editor panel-first-child panel-last-child" data-index="0">
							<div class="so-widget-sow-editor so-widget-sow-editor-base">
							<div class="siteorigin-widget-tinymce textwidget">
							<p><strong>We want to change the way people view skincare. Our serums are unprecedented in the marketplace and challenge conventional skincare. Supported by certification and proven results, Biologi simply delivers radically improved skin.</strong></p>
<p>Biologi Founder Ross Macdougald has been creating plant extracts for inclusion in cosmetic products since 2012. Frustrated by the levels at which his serums were being used in commercial products, he created Biologi, the world’s first 100% active natural plant serum. It contains no harmful chemicals, no additives, perfumes, not even water.</p>
<p>Biologi is the first skincare company to master the entire production process from plant to bottle. Our products are entirely traceable and sustainable. Biologi is extracted, produced and packaged in Byron Bay, Australia, using organic native botanical’s.</p>
</div>
</div></div></div><div id="pgc-572-0-1" class="panel-grid-cell"><div id="panel-572-0-1-0" class="so-panel widget widget_sow-editor panel-first-child panel-last-child" data-index="1"><div class="white-section-heading panel-widget-style panel-widget-style-for-572-0-1-0"><div class="so-widget-sow-editor so-widget-sow-editor-base">
<div class="siteorigin-widget-tinymce textwidget">
	<p>A RADICAL<br> LY DIFFER<br> ENT APPR<br> OACH TO<br> SKINCARE</p>
</div>
</div></div></div></div></div></div><div id="pg-572-1" class="panel-grid panel-has-style"><div class="panel-row-style panel-row-style-for-572-1"><div id="pgc-572-1-0" class="panel-grid-cell"><div id="panel-572-1-0-0" class="so-panel widget widget_sow-editor panel-first-child panel-last-child" data-index="2"><div class="headline-text panel-widget-style panel-widget-style-for-572-1-0-0"><div class="so-widget-sow-editor so-widget-sow-editor-base">
<div class="siteorigin-widget-tinymce textwidget">
	<p>WHO IS BIOLOGI?</p>
</div>
</div></div></div></div></div></div><div id="pg-572-2" class="panel-grid panel-has-style"><div class="page-widget panel-row-style panel-row-style-for-572-2"><div id="pgc-572-2-0" class="panel-grid-cell"><div id="panel-572-2-0-0" class="so-panel widget widget_sow-editor panel-first-child panel-last-child" data-index="3"><div class="so-widget-sow-editor so-widget-sow-editor-base"><h3 class="widget-title">ROSS MACDOUGALD - FOUNDER</h3>
<div class="siteorigin-widget-tinymce textwidget">
<p>Ross Macdougald is an Australian creative scientific chemist who is the founder of Biologi, Phytoverse and Plant Extracts. As an experienced industry leader, Ross is on a mission is to prove through science that 'plant phyto nutrients' can not only work but are better than synthetic man-made chemicals.</p>
<p>Born and educated in Sydney, Ross was approached to head up the new product development division at Thursday Plantation and later became head of product development at a private research institute located at Southern Cross University developing multiple products for various multi nationals and local companies in the fields of cosmetic, skin & hair care, pharmaceutical, veterinary, oral hygiene and neutral ceuticals. Throughout his career, Ross has developed over many products and formulated active ingredients. Ross can be credited with many revolutionary formulations and active ingredients which are currently sold in many brands globally.</p>
<p>In 2000, Ross launched Phytoverse, a wholesale essential oil and vegetable oil supplier to the cosmetic, pharmaceutical and aromatherapy industries (among others). Phytoverse is Australia’s leading ethical natural materials wholesaler and is regarded as the industry benchmark for quality and scientific validity. Ross’ work with Phytoverse further proves that plant-based materials can create more efficient products without the use of synthetic additives.</p>
<p>Driven by demand from the industry for similar ethical products, Ross, 2008 commenced his journey in pioneering a revolutionary plant extraction method CLECS (Cold Liquid Extraction Closed System). This signaled the launch of his company Plant Extracts which supplies 100% active plant extracts to various industries. These extracts are revolutionary, not only because they significantly outperform synthetic alternatives, but for their purity and ethical production. </p>
<p>For Ross, the journey did not stop there. He became frustrated at the low levels that these plant actives were added to products and often only for marketing and labeling claims. Brands were increasingly diminishing the levels of active ingredients included in their products, meaning that what was found of the shelves contained so little percentages that it rendered the product useless.</p>
<p>Wanting to change the game, Ross launched Biologi in 2017, the world’s first ever 100% active single ingredient, pure plant serum. Using his revolutionary extraction system (CLECS) which mimics the plant’s internal closed process to extract serums that work in the bottle as they do in their unique cellular environment. Biologi is the first skincare product in the world that contains natural, stable, active vitamin C. Absorption of the beneficial active plant nutrients is fast and effective allowing the serum to penetrate deeper into the epidermis to repair, restore and hydrate skin cells.</p>
<p>Ross is committed to revolutionising not only the skincare industry but many other industries that use synthetic in-active ingredients as the active principles of their products. Today Ross continues to explore the development of ground breaking technology and game changing ingredients, ensuring that people around the world have access to natural, superior and effective phyto nutrients that truly works.</p>
</div>
</div></div></div><div id="pgc-572-2-1" class="panel-grid-cell"><div id="panel-572-2-1-0" class="so-panel widget widget_sow-image panel-first-child" data-index="4"><div class="so-widget-sow-image so-widget-sow-image-default-2ec334f0a340">

<div class="sow-image-container">
	<img src="https://www.biologi.com.au/wp-content/uploads/2019/10/AboutUs-Ross-1.jpg" width="4333" height="6500" srcset="https://www.biologi.com.au/wp-content/uploads/2019/10/AboutUs-Ross-1.jpg 4333w, https://www.biologi.com.au/wp-content/uploads/2019/10/AboutUs-Ross-1-200x300.jpg 200w, https://www.biologi.com.au/wp-content/uploads/2019/10/AboutUs-Ross-1-768x1152.jpg 768w, https://www.biologi.com.au/wp-content/uploads/2019/10/AboutUs-Ross-1-683x1024.jpg 683w, https://www.biologi.com.au/wp-content/uploads/2019/10/AboutUs-Ross-1-1000x1500.jpg 1000w, https://www.biologi.com.au/wp-content/uploads/2019/10/AboutUs-Ross-1-1280x1920.jpg 1280w, https://www.biologi.com.au/wp-content/uploads/2019/10/AboutUs-Ross-1-400x600.jpg 400w, https://www.biologi.com.au/wp-content/uploads/2019/10/AboutUs-Ross-1-600x900.jpg 600w" sizes="(max-width: 4333px) 100vw, 4333px" alt="" class="so-widget-image">
</div>

</div></div><div id="panel-572-2-1-1" class="so-panel widget widget_sow-editor panel-last-child" data-index="5"><div class="white-section-heading panel-widget-style panel-widget-style-for-572-2-1-1"><div class="so-widget-sow-editor so-widget-sow-editor-base">
<div class="siteorigin-widget-tinymce textwidget">
	<p>ROSS<br> MAC<br> DOUG<br> ALD</p>
</div>
</div></div></div></div></div></div><div id="pg-572-3" class="panel-grid panel-has-style"><div class="page-widget panel-row-style panel-row-style-for-572-3"><div id="pgc-572-3-0" class="panel-grid-cell"><div id="panel-572-3-0-0" class="so-panel widget widget_sow-image panel-first-child" data-index="6"><div class="panel-widget-style panel-widget-style-for-572-3-0-0"><div class="so-widget-sow-image so-widget-sow-image-default-2ec334f0a340">

<div class="sow-image-container">
	<img src="https://www.biologi.com.au/wp-content/uploads/2019/10/AboutUs-Lucy-1.jpg" width="4333" height="6500" srcset="https://www.biologi.com.au/wp-content/uploads/2019/10/AboutUs-Lucy-1.jpg 4333w, https://www.biologi.com.au/wp-content/uploads/2019/10/AboutUs-Lucy-1-200x300.jpg 200w, https://www.biologi.com.au/wp-content/uploads/2019/10/AboutUs-Lucy-1-768x1152.jpg 768w, https://www.biologi.com.au/wp-content/uploads/2019/10/AboutUs-Lucy-1-683x1024.jpg 683w, https://www.biologi.com.au/wp-content/uploads/2019/10/AboutUs-Lucy-1-1000x1500.jpg 1000w, https://www.biologi.com.au/wp-content/uploads/2019/10/AboutUs-Lucy-1-1280x1920.jpg 1280w, https://www.biologi.com.au/wp-content/uploads/2019/10/AboutUs-Lucy-1-400x600.jpg 400w, https://www.biologi.com.au/wp-content/uploads/2019/10/AboutUs-Lucy-1-600x900.jpg 600w" sizes="(max-width: 4333px) 100vw, 4333px" alt="" class="so-widget-image">
</div>

</div></div></div><div id="panel-572-3-0-1" class="so-panel widget widget_sow-editor panel-last-child" data-index="7"><div class="white-section-heading panel-widget-style panel-widget-style-for-572-3-0-1"><div class="so-widget-sow-editor so-widget-sow-editor-base">
<div class="siteorigin-widget-tinymce textwidget">
	<p>LUCY<br>
MAC<br>
DOUG<br>
ALD</p>
</div>
</div></div></div></div><div id="pgc-572-3-1" class="panel-grid-cell"><div id="panel-572-3-1-0" class="so-panel widget widget_sow-editor panel-first-child panel-last-child" data-index="8"><div class="so-widget-sow-editor so-widget-sow-editor-base"><h3 class="widget-title">LUCY MACDOUGALD - MANAGING DIRECTOR</h3>
<div class="siteorigin-widget-tinymce textwidget">
	<p>Lucy Macdougald is the global Managing Director of Biologi, an Australian skincare brand. Based in Byron Bay, New South Wales, Lucy leads the company’s focus on challenging conventional skincare, whilst also overseeing the day-to-day operations of the Biologi brand. Equipped with years of experience in the skincare industry, Lucy is passionate about ethical skincare and educating consumers on skincare solutions.</p>
<p>Calling for industry-wide transparency about product ingredients and existing extraction techniques, Lucy is committed to helping women make informed choices about what they are putting on their skin. She is dedicated to providing insights that will enable consumers to cut through the marketing hype so they then can make informed decisions based on the true facts.</p>
<p>As a qualified and experienced dermal specialist and senior laser technician, Lucy leads her team to advise consumers on how best to use Biologi to address individual skin concerns for their skin type. Prior to becoming Managing Director of Biologi, Lucy practiced as a dermal specialist and so is privy to the common nuances and issues women face when trying to navigate the world of skincare. These insights, combined with Biologi’s world-first serums, are literally changing the way women feel about their skin.</p>
<p>Lucy was born in New Zealand and now lives and works in Byron Bay, New South Wales. She dedicates herself to developing the Biologi brand by creating ethically-sourced and manufactured products that are revolutionising the industry. In addition to her day-to-day, Lucy volunteers her time speaking at events and writing articles, all with one mission in mind – helping women to be informed and empowered when it comes to skincare.</p>
</div>
</div></div></div></div></div></div>		</div>
	</div>		
</div>
</article>
</main>

<?php get_footer(); ?>