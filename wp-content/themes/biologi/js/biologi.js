"use strict";

jQuery(document).ready(function(){
	// Push down content if slider is present
	if(jQuery('#biologi-slider').length) {
		jQuery('body').addClass('biologi-slider');
	}
});

jQuery(function($)
{
	//show blog newsletter popup
	$('.join-here').on('click', function(e)  {		
		$('.popup').fadeIn(350);
		e.preventDefault();
	});

	//show shop newsletter popup
	$('.shop-newsletter').on('click', function(e)  {		
		$('#shop-popup').fadeIn(350);
		e.preventDefault();
	});

	//show home newsletter popup
	$('.join-button').on('click', function(e)  {		
		$('#home-newsletter-popup').fadeIn(350);
		e.preventDefault();
	});

	//close newsletter popup
	$('[data-popup-close]').on('click', function(e)  {
		var targeted_popup_class = jQuery(this).attr('data-popup-close');
		$('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
		e.preventDefault();
	});
});

//YouTube video popup
(function ( $ ) {
	$.fn.grtyoutube = function(options){
		return this.each(function() {
			// Get video ID
			var getvideoid = $(this).attr("youtubeid");
			// Default options
			var settings = $.extend({
				videoID: getvideoid,
				autoPlay: true
			}, options );

			// Convert some values
			if(settings.autoPlay === true) { settings.autoPlay = 1 } else { settings.autoPlay = 0 }
			// Initialize on click
			if(getvideoid) {
				$(this).on( "click", function() {
					 $("body").append('<div class="grtvideo-popup">'+
								'<div class="grtvideo-popup-content">'+
									'<span class="grtvideo-popup-close">X</span>'+
									'<iframe class="grtyoutube-iframe" src="https://www.youtube.com/embed/'+settings.videoID+'?rel=0&wmode=transparent&autoplay='+settings.autoPlay+'&iv_load_policy=3" allowfullscreen frameborder="0"></iframe>'+
								'</div>'+
							'</div>');
				});
			}

			// Close the box on click or escape
			$(this).on('click', function (event) {
				event.preventDefault();
				$(".grtvideo-popup-close, .grtvideo-popup").click(function(){
					$(".grtvideo-popup").remove();
				});
			});

			$(document).keyup(function(event) {
				if (event.keyCode == 27){
					$(".grtvideo-popup").remove();
				}
			});
		});
	};
}( jQuery ));

jQuery(document).ready(function($) {
	$(".youtube-link").grtyoutube({
		autoPlay:true
	});
});	

// home page main slider
jQuery(document).ready(function($){
	$('.home #home-promo-banner.slider').slick({
	  dots: false,
	  autoplay: true,
	  arrows: true,
	  infinite: true,
	  speed: 300,
	  autoplaySpeed: 4000,
	  slidesToShow: 1,
	  adaptiveHeight: false
	});	
});

//Product Quantity Plus and Minus Buttons
jQuery( function( $ ) {
	if ( ! String.prototype.getDecimals ) {
		String.prototype.getDecimals = function() {
			var num = this,
				match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
			if ( ! match ) {
				return 0;
			}
			return Math.max( 0, ( match[1] ? match[1].length : 0 ) - ( match[2] ? +match[2] : 0 ) );
		}
	}
	// Quantity "plus" and "minus" buttons
	$( document.body ).on( 'click', '.plus, .minus', function() {
		var $qty        = $( this ).closest( '.quantity' ).find( '.qty'),
			currentVal  = parseFloat( $qty.val() ),
			max         = parseFloat( $qty.attr( 'max' ) ),
			min         = parseFloat( $qty.attr( 'min' ) ),
			step        = $qty.attr( 'step' );

		// Format values
		if ( ! currentVal || currentVal === '' || currentVal === 'NaN' ) currentVal = 0;
		if ( max === '' || max === 'NaN' ) max = '';
		if ( min === '' || min === 'NaN' ) min = 0;
		if ( step === 'any' || step === '' || step === undefined || parseFloat( step ) === 'NaN' ) step = 1;

		// Change the value
		if ( $( this ).is( '.plus' ) ) {
			if ( max && ( currentVal >= max ) ) {
				$qty.val( max );
			} else {
				$qty.val( ( currentVal + parseFloat( step )).toFixed( step.getDecimals() ) );
			}
		} else {
			if ( min && ( currentVal <= min ) ) {
				$qty.val( min );
			} else if ( currentVal > 0 ) {
				$qty.val( ( currentVal - parseFloat( step )).toFixed( step.getDecimals() ) );
			}
		}

		// Trigger change event
		$qty.trigger( 'change' );
	});
 });

//Expand and collapse latest review on single product page
jQuery(document).ready(function($) { 	
    var showChar = 120;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Read More";
    var lesstext = "Hide";
    $('.woocommerce.single-product .product .summary .comment-box .comment-more').each(function() {
        var content = $(this).html();
        if(content.length > showChar) {
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
            var html = c + '<span class="moreellipses">' + ellipsestext+ '</span><span>' + h + '</span> <a href="" class="morelink">' + moretext + '</a>';
            $(this).html(html);
        }
    });
 
    $(".woocommerce.single-product .product .summary .comment-box .comment-more .morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
			$('.woocommerce.single-product .product .summary .comment-box .comment-more span.moreellipses').css('display','inline');
			$('.woocommerce.single-product .product .summary .comment-box .see-all').css('display','none');
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
			$('.woocommerce.single-product .product .summary .comment-box .comment-more span.moreellipses').css('display','none');
			$('.woocommerce.single-product .product .summary .comment-box .see-all').css('display','inline-block');	
        }        
        $(this).prev().toggle();
        return false;
    });
}); 

//Enable smooth scrolling
jQuery(document).ready(function($){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {
	  
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "" && (this.getAttribute('data-toggle')!='tab') ) {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top-150
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        //window.location.hash = hash;
      });
    }
  });
});

//Insert terms and conditions checkbox on wholesale registration page
jQuery(document).ready(function($){
	$('.wholesale-registration .terms-and-condition-container').before().append('<input type="checkbox" name="terms-and-condition" class="wholesale-terms" />');
	$(".wholesale-registration .register-button-container #wwlc-register").click(function(){
		if ($('.wholesale-registration .terms-and-condition-container .wholesale-terms').prop('checked')!=true){ 
        	alert('Please tick the Terms & Conditions checkbox.');
			return false;
    	}
		else {
			return true;
		}	
	});
});	

//Homepage featured products slider
jQuery(document).ready(function($){
	var slick_options = {
	  	infinite: true,
	  	slidesToShow: 4,
	  	slidesToScroll: 1,
	  	arrows: true,
	  	dots: false,
	  	autoplay: false,
	  	fade:false,
	  	responsive: [
		    {
		      	breakpoint: 1024,
		      	settings: {
		        	slidesToShow: 3,
		      	}
		    },
		    {
		      	breakpoint: 600,
		      	settings: {
		        	slidesToShow: 2,
		      	}
		    },
		    {
		      	breakpoint: 480,
		      	settings: {
		        	slidesToShow: 1,
		      	}
		    }
	  	]
	};
	$('#homepage .featured-products').slick(slick_options);
});	
