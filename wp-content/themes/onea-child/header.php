<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php
	/**
	 * onea_elated_action_header_meta hook
	 *
	 * @see onea_elated_header_meta() - hooked with 10
	 * @see onea_elated_user_scalable_meta - hooked with 10
	 * @see onea_core_set_open_graph_meta - hooked with 10
	 */
	do_action( 'onea_elated_action_header_meta' );
    wp_head();
	?>
	
	<script src="https://kit.fontawesome.com/33aba96e4d.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/modal-video.min.css">
<script>
jQuery( document ).ready(function() {
jQuery('input:radio[name="payment_method"]').change(
    function(){
        if (jQuery(this).is(':checked') && jQuery(this).val() == 'afterpay') {
          alert('Ji');
jQuery('.payment_method_afterpay').show();
        }
    });
 });
 </script>
	
	<!-- 	Klaviyo -->
<!-- 	<script type="application/javascript" async src="https://static.klaviyo.com/onsite/js/klaviyo.js?company_id=pk_6fc500f3850d11524194c2f00847a2424a"></script> -->
	<?php 
		if ( is_product() ): 
            global $product;
            $ImageUrl = wp_get_attachment_image_src( get_post_thumbnail_id( $product->ID ), 'single-post-thumbnail' )[0];
            $ItemId = $product->id;
            $Title = $product-> get_title();
            $ProductUrl = get_permalink( $product->ID );;
            $Price = $product->get_price();
            $RegularPrice = $product->get_regular_price();
            $DiscountAmount = $RegularPrice - $Price;
            $terms = get_terms( 'product_tag' );
	?>
	
	<script>
        var Title = "<?php echo $Title; ?>";
        var ItemId = "<?php echo $ItemId; ?>";
        var ImageUrl = "<?php echo $ImageUrl; ?>";
        var ProductUrl = "<?php echo $ProductUrl; ?>";
        var Price = "<?php echo $Price; ?>";
        var DiscountAmount = "<?php echo $DiscountAmount; ?>";
        var RegularPrice = "<?php echo $RegularPrice; ?>";
        var _learnq = _learnq || [];
        _learnq.push(['track', 'Viewed Product', {
            Title: Title,
            ItemId: ItemId,
            ImageUrl: ImageUrl,
            Url: ProductUrl,
            Metadata: {
            Price: Price,
            DiscountAmount: DiscountAmount,
            RegularPrice: RegularPrice
        }
        }]);
		
		var _item = {
            Title: Title,
            ItemId: ItemId,
            ImageUrl: ImageUrl,
            Url: ProductUrl,
            Metadata: {
            Price: Price,
            DiscountAmount: DiscountAmount,
            RegularPrice: RegularPrice
        };
		
		document.getElementsByClassName("single_add_to_cart_button").addEventListener('click',function (){
			_learnq.push(['track', 'Added to Cart', _item]);
		});
	</script>
	
	<?php endif; ?> 
	<!-- 	@end Klaviyo -->
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106327158-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
		
  gtag('config', 'UA-106327158-1', { 'send_page_view': false, 'custom_map': {'dimension1': 'ecomm_prodid', 'dimension2': 'ecomm_pagetype', 'dimension3': 'ecomm_totalvalue'} });
  gtag('config', 'AW-831219242');
</script>
	
</head>
<body <?php body_class(); ?>itemscope itemtype="http://schema.org/WebPage">
	
	
	<?php
	/**
	 * onea_elated_action_after_body_tag hook
	 *
	 * @see onea_elated_get_side_area() - hooked with 10
	 * @see onea_elated_smooth_page_transitions() - hooked with 10
	 */
	do_action( 'onea_elated_action_after_body_tag' ); ?>

    <div class="eltdf-wrapper">
        <div class="eltdf-wrapper-inner">
            <?php
            /**
             * onea_elated_action_after_wrapper_inner hook
             *
             * @see onea_elated_get_header() - hooked with 10
             * @see onea_elated_get_mobile_header() - hooked with 20
             * @see onea_elated_back_to_top_button() - hooked with 30
             * @see onea_elated_get_header_minimal_full_screen_menu() - hooked with 40
             * @see onea_elated_get_header_bottom_navigation() - hooked with 40
             */
            do_action( 'onea_elated_action_after_wrapper_inner' ); ?>
	        
            <div class="eltdf-content" <?php onea_elated_content_elem_style_attr(); ?>>
                <div class="eltdf-content-inner">