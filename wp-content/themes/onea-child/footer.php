<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-modal-video.min.js"></script>
<script>
		jQuery(".js-video-button").modalVideo({
			youtube:{
				controls:1,
				autoplay:1,
				nocookie: true
			}
		});
	
	document.addEventListener( 'wpcf7mailsent', function( event ) {
  if ( '96041' == event.detail.contactFormId ) {
    window.location.href = "https://www.biologi.com.au/contact-thank-you/";
    // do something productive
  }
	  if ( '96043' == event.detail.contactFormId ) {
    window.location.href = "https://www.biologi.com.au/mailing-list-thank-you/";
    // do something productive
  }
}, false );
	
	</script>
<?php do_action( 'onea_elated_get_footer_template' );