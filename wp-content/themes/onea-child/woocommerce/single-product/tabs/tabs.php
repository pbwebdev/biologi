<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 *
 * @see woocommerce_default_product_tabs()
 */
$product_tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $product_tabs ) ) : ?>

	<div class="woocommerce-tabs wc-tabs-wrapper nn">
		<ul class="tabs wc-tabs" role="tablist">
			<?php foreach ( $product_tabs as $key => $product_tab ) : ?>
				<li class="<?php echo esc_attr( $key ); ?>_tab" id="tab-title-<?php echo esc_attr( $key ); ?>" role="tab" aria-controls="tab-<?php echo esc_attr( $key ); ?>">
					<a href="#tab-<?php echo esc_attr( $key ); ?>">
						<?php echo wp_kses_post( apply_filters( 'woocommerce_product_' . $key . '_tab_title', $product_tab['title'], $key ) ); ?>
					</a>
				</li>
			<?php endforeach; ?>
			<div class="leave-review-li">
				<a href="JavaScript:Void(0)" class="leave-review-button" style="font-size: 14px !important;font-size: 12px !important;font-weight: 700 !important;color: #fff;letter-spacing: 3px;">LEAVE A REVIEW</a>
			</div>
		</ul>
		
		<?php foreach ( $product_tabs as $key => $product_tab ) : ?>
			<div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--<?php echo esc_attr( $key ); ?> panel entry-content wc-tab" id="tab-<?php echo esc_attr( $key ); ?>" role="tabpanel" aria-labelledby="tab-title-<?php echo esc_attr( $key ); ?>">
				<?php
				if ( isset( $product_tab['callback'] ) ) {
					call_user_func( $product_tab['callback'], $key, $product_tab );
				}
				?>
			</div>
		<?php endforeach; ?>

		<?php do_action( 'woocommerce_product_after_tabs' ); ?>
	</div>

<?php endif; ?>
<style>
.leave-review-li{
float: right;
position: relative;
top: 5px;
}
.leave-review-button{
font-size: 14px !important;
padding: 10px 15px !important;
line-height: 14px !important;
position: relative;
    display: inline-block;
    vertical-align: middle;
    width: auto;
    margin: 0;
    font-family: Poppins,sans-serif;
    font-size: 13px;
    line-height: 2.1em;
    font-weight: 600;
    outline: 0;
    box-sizing: border-box;
    padding: 9px 46px;
    color: #fff;
    background-color: #000;
    border: 1px solid transparent;
    white-space: nowrap;
    cursor: pointer;
    z-index: 3;
}

/*.woocommerce p.stars span a:before, 
.woocommerce-page p.stars span a:before {
    content: "\f4b3";
    font-family: "star";
    opacity: 1;
    visibility: visible;
	color: #a8a8a8 !important;
}

.woocommerce p.stars span a, 
.woocommerce-page p.stars span a{
    opacity: 1;
    visibility: hidden;
    max-width: 20px;
}*/
</style>
