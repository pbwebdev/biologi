<?php

/**
 * Template Name: Wholesaler Page
 *
 * @package ThemePlate
 * @since 0.1.0
 */
			
global $wc_wholesale_order_form;

$user_has_access = $wc_wholesale_order_form->_wwof_permissions->wwof_user_has_access();

$eltdf_sidebar_layout  = onea_elated_sidebar_layout();
$eltdf_grid_space_meta = onea_elated_get_meta_field_intersect( 'page_grid_space' );
$eltdf_holder_classes  = ! empty( $eltdf_grid_space_meta ) ? 'eltdf-grid-' . $eltdf_grid_space_meta . '-gutter' : '';

get_header();
onea_elated_get_title();
get_template_part( 'slider' );
do_action('onea_elated_action_before_main_content');
?>

<div class="eltdf-container eltdf-default-page-template">
	<?php do_action( 'onea_elated_action_after_container_open' ); ?>
	
	<div class="eltdf-container-inner clearfix">
        <?php do_action( 'onea_elated_action_after_container_inner_open' ); ?>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="eltdf-grid-row <?php echo esc_attr( $eltdf_holder_classes ); ?>">
				<div <?php echo onea_elated_get_content_sidebar_class(); ?>>
					<?php
						if ( $user_has_access ) {
							the_content();
						} else {
			
							// User don't have permission
							$title      = trim( stripslashes( strip_tags( get_option( 'wwof_permissions_noaccess_title' ) ) ) );
							$message    = trim( stripslashes( get_option( 'wwof_permissions_noaccess_message' ) ) );
							$login_url  = trim( get_option( 'wwof_permissions_noaccess_login_url' ) );
			
							if ( empty( $title ) )
								$title = __( 'Access Denied' , 'woocommerce-wholesale-order-form' );
			
							if ( empty( $message ) )
								$message = __( "You do not have permission to view wholesale product listing" , 'woocommerce-wholesale-order-form' );
			
							if ( empty( $login_url ) )
								$login_url = wp_login_url(); ?>
			
							<div id="wwof_access_denied">
								<h2 class="content-title"><?php echo $title; ?></h2>
								<?php echo do_shortcode( html_entity_decode( $message ) ); ?>
								<p class="login-link-container"><a class="login-link" href="<?php echo $login_url; ?>"><?php _e( 'Login Here' , 'woocommerce-wholesale-order-form' ); ?></a></p>
							</div><?php
						}
						do_action( 'onea_elated_action_page_after_content' );
					?>
				</div>
				<?php if ( $eltdf_sidebar_layout !== 'no-sidebar' ) { ?>
					<div <?php echo onea_elated_get_sidebar_holder_class(); ?>>
						<?php get_sidebar(); ?>
					</div>
				<?php } ?>
			</div>
		<?php endwhile; endif; ?>
        <?php do_action( 'onea_elated_action_before_container_inner_close' ); ?>
	</div>
	
	<?php do_action( 'onea_elated_action_before_container_close' ); ?>
</div>

<?php get_footer(); ?>