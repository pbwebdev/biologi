(function ($) {

    // products
    $('#slideBelowProducts .wpb_wrapper').append('<span class="prodSlideControls"><i id="prodScrollLeft" class="fas fa-chevron-left" aria-hidden="true"></i><i id="prodScrollRight" class="fas fa-chevron-right" aria-hidden="true"></i></span>');

    $("#prodScrollLeft").click(function (e) {
        e.preventDefault();
        $("#productsRowRR").animate({
            scrollLeft: "-=300px"
        }, "slow");
    });

    $("#prodScrollRight").click(function (e) {
        e.preventDefault();
        $("#productsRowRR").animate({
            scrollLeft: "+=300px"
        }, "slow");
    });

    //as mentioned in test
    $('#homePressBottom .wpb_content_element .wpb_wrapper').append('<span class="prodSlideControls"><i id="amiScrollLeft" class="fas fa-chevron-left" aria-hidden="true"></i><i id="amiScrollRight" class="fas fa-chevron-right" aria-hidden="true"></i></span>');

    $("#amiScrollLeft").click(function (e) {
        e.preventDefault();
        $("#homePress").animate({
            scrollLeft: "-=250px"
        }, "slow");
    });

    $("#amiScrollRight").click(function (e) {
        e.preventDefault();
        $("#homePress").animate({
            scrollLeft: "+=250px"
        }, "slow");
    });

})(jQuery);