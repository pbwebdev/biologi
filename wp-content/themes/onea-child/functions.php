<?php

/*** Child Theme Function  ***/
// Create and display the custom field in product general setting tab
add_action( 'woocommerce_product_options_general_product_data', 'add_custom_field_general_product_fields' );
function add_custom_field_general_product_fields(){
    global $post;

    echo '<div class="product_custom_field">';

    // Custom Product Checkbox Field
    woocommerce_wp_checkbox( array(
        'id'        => '_disabled_for_coupons',
        'label'     => __('Disabled for coupons', 'woocommerce'),
        'description' => __('Disable this products from coupon discounts', 'woocommerce'),
        'desc_tip'  => 'true',
    ) );

    echo '</div>';;
}

// Save the custom field and update all excluded product Ids in option WP settings
add_action( 'woocommerce_process_product_meta', 'save_custom_field_general_product_fields', 10, 1 );
function save_custom_field_general_product_fields( $post_id ){

    $current_disabled = isset( $_POST['_disabled_for_coupons'] ) ? 'yes' : 'no';

    $disabled_products = get_option( '_products_disabled_for_coupons' );
    if( empty($disabled_products) ) {
        if( $current_disabled == 'yes' )
            $disabled_products = array( $post_id );
    } else {
        if( $current_disabled == 'yes' ) {
            $disabled_products[] = $post_id;
            $disabled_products = array_unique( $disabled_products );
        } else {
            if ( ( $key = array_search( $post_id, $disabled_products ) ) !== false )
                unset( $disabled_products[$key] );
        }
    }

    update_post_meta( $post_id, '_disabled_for_coupons', $current_disabled );
    update_option( '_products_disabled_for_coupons', $disabled_products );
}

// Make coupons invalid at product level
add_filter('woocommerce_coupon_is_valid_for_product', 'set_coupon_validity_for_excluded_products', 12, 4);
function set_coupon_validity_for_excluded_products($valid, $product, $coupon, $values ){

    // custom validation to override bug
    // Sale Items excluded from discount.
    if ( $coupon->get_exclude_sale_items() && !in_array( $product->get_id(), wc_get_product_ids_on_sale() ) ) {
        $valid = true;
        // if( $product->is_on_sale() ){
        //     throw new Exception( __( 'Sorry, this coupon is not applicable to selected products' . var_dump($product->product_id), 'woocommerce' ), 109 );
        //     $valid = false;
        // }
    }

    if( ! count(get_option( '_products_disabled_for_coupons' )) > 0 ) return $valid;

    $disabled_products = get_option( '_products_disabled_for_coupons' );
    if( in_array( $product->get_id(), $disabled_products ) )
        $valid = false;

    return $valid;

}

// Set the product discount amount to zero
add_filter( 'woocommerce_coupon_get_discount_amount', 'zero_discount_for_excluded_products', 12, 5 );
function zero_discount_for_excluded_products($discount, $discounting_amount, $cart_item, $single, $coupon ){
    if( ! count(get_option( '_products_disabled_for_coupons' )) > 0 ) return $discount;

    $disabled_products = get_option( '_products_disabled_for_coupons' );
    if( in_array( $cart_item['product_id'], $disabled_products ) )
        $discount = 0;

    return $discount;
}


if ( ! function_exists( 'onea_elated_child_theme_enqueue_scripts' ) ) {
	function onea_elated_child_theme_enqueue_scripts() {
		$parent_style = 'onea-elated-default-style';
		
		wp_enqueue_style( 'onea-elated-child-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ) );
	}
	
	add_action( 'wp_enqueue_scripts', 'onea_elated_child_theme_enqueue_scripts' );
}
/**
 * Remove product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;
}
/**
 * Add a custom product data tab
 */
add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
function woo_new_product_tab( $tabs ) {
	
	// Adds the new tab
	
	$tabs['test_tab'] = array(
		'title' 	=> __( 'FAQ', 'woocommerce' ),
		'priority' 	=> 50,
		'callback' 	=> 'woo_new_product_tab_content'
	);

	return $tabs;

}
function woo_new_product_tab_content() {
	// check if the repeater field has rows of data
			if( have_rows('product_faqs',get_the_ID()) ):
				$i = 1;
				?>
			<div class="eltdf-accordion-holder eltdf-ac-default  eltdf-accordion eltdf-ac-simple  clearfix ui-accordion ui-widget ui-helper-reset" role="tablist">
			<?php
			
				// loop through the rows of data
				while ( have_rows('product_faqs',get_the_ID()) ) : the_row();

					// display a sub field value
					
					?>
					<h4 class="eltdf-accordion-title ui-accordion-header ui-state-default ui-corner-all" role="tab" id="ui-id-<?=$i?>" aria-controls="ui-id-<?=$i++?>" aria-selected="false" aria-expanded="false" tabindex="-1">
						<span class="eltdf-accordion-mark">
<span class="eltdf_icon_plus icon_plus"></span>
<span class="eltdf_icon_minus icon_minus-06"></span>
</span>
						<span class="eltdf-tab-title"><?=the_sub_field('title')?></span>
					</h4>
					<div class="eltdf-accordion-content ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" id="ui-id-<?=$i++?>" aria-labelledby="ui-id-<?=$i?>" role="tabpanel" aria-hidden="true" style="display: none;">
						<div class="eltdf-accordion-content-inner">
							<div class="wpb_text_column wpb_content_element ">
								<div class="wpb_wrapper">
								<p><?=the_sub_field('description')?></p>
								</div>
							</div>
						</div>
					</div>
					<?php 
					$i++;
				endwhile;

			
			?>
		</div>
		<?php
		else :
		endif;
}

// Add a new custom grid shortcode module
// Usage [myprefix_custom_grid posts_per_page="4" term="4"]
// You can also go to Visual Composer > Shortcode Mapper to add your custom module
function myprefix_custom_grid_shortcode( $atts ) {

	// Parse your shortcode settings with it's defaults
	$atts = shortcode_atts( array(
		'posts_per_page' => '-1',
		'term'           => ''
	), $atts, 'myprefix_custom_grid' );

	// Extract shortcode atributes
	extract( $atts );

	// Define output var
	$output = '';

	// Define query
	$query_args = array(
		'post_type'      => 'presssingle', // Change this to the type of post you want to show
		'posts_per_page' => $posts_per_page,
	);

	// Query by term if defined
	if ( $term ) {

		$query_args['tax_query'] = array(
			array(
				'taxonomy' => 'category',
				'field'    => 'ID',
				'terms'    => $term,
			),
		);

	}

	// Query posts
	$custom_query = new WP_Query( $query_args );

	// Add content if we found posts via our query
	if ( $custom_query->have_posts() ) {

		// Open div wrapper around loop
		$output .= '<div >';

		// Loop through posts
		while ( $custom_query->have_posts() ) {

			// Sets up post data so you can use functions like get_the_title(), get_permalink(), etc
			$custom_query->the_post();

			// This is the output for your entry so what you want to do for each post.
			$urlPublication = get_field('publication_url'); 
			 /* grab the url for the full size featured image */
            $featured_img_url = get_the_post_thumbnail_url();
		
			$output .= '<a class="pressItem" target="_blank" href='. $urlPublication .'>';
			$output .= '<img src="'. $featured_img_url .'">';
			$output .= '<div>' . get_the_content() . '</div></a>';

		}

		// Close div wrapper around loop
		$output .= '</div>';

		// Restore data
		wp_reset_postdata();

	}

	// Return your shortcode output
	return $output;

}
add_shortcode( 'myprefix_custom_grid', 'myprefix_custom_grid_shortcode' );


// videos carousel FAQ Videos
// Usage [myprefix_custom_grid_faqVideos posts_per_page="4" term="4"]
// You can also go to Visual Composer > Shortcode Mapper to add your custom module
function myprefix_custom_grid_shortcode_faqVideos( $atts ) {

	// Parse your shortcode settings with it's defaults
	$atts = shortcode_atts( array(
		'posts_per_page' => '-1',
		'term'           => ''
	), $atts, 'myprefix_custom_grid_faqVideos' );

	// Extract shortcode atributes
	extract( $atts );

	// Define output var
	$output = '';

	// Define query
	$query_args = array(
		'post_type'      => 'faqvideo', // Change this to the type of post you want to show
		'posts_per_page' => $posts_per_page,
	);

	// Query by term if defined
	if ( $term ) {

		$query_args['tax_query'] = array(
			array(
				'taxonomy' => 'category',
				'field'    => 'ID',
				'terms'    => $term,
			),
		);

	}

	// Query posts
	$custom_query = new WP_Query( $query_args );

	// Add content if we found posts via our query
	if ( $custom_query->have_posts() ) {

		// Open div wrapper around loop
		$output .= '<div >';

		// Loop through posts
		while ( $custom_query->have_posts() ) {

			// Sets up post data so you can use functions like get_the_title(), get_permalink(), etc
			$custom_query->the_post();

			 /* grab the url for the full size featured image */
            $featured_img_url = get_the_post_thumbnail_url();
			$videoIDfield = get_field('faq_youtube_video'); 
			$vidLinkRR = get_field('videoLink');
            $postID = get_the_ID();
            
			
			$output .= '<div class="faqVideoItem"><img width="80" src="'. $featured_img_url .'" >';
			$output .= '<div>' . get_the_title() . '</div>';
			// $output .= '<div class="read-more-faqvideos"><a href="https://www.youtube.com/watch?v='. $videoIDfield.'" target="_blank">WATCH NOW</a></div></div>';
            if($vidLinkRR == ''){
            $output .= '<div class="read-more-faqvideos"><a href="#" class="js-video-button" data-video-id="'.$videoIDfield.'">WATCH NOW</a></div></div>';
            }else{
            $output .= '<div class="read-more-faqvideos"><a href="#" class="js-video-button" data-channel="video" data-video-url="'.$vidLinkRR.'">WATCH NOW</a></div></div>';
            }
            
			
		}

		// Close div wrapper around loop

		$output .= '</div>';




		
		// Restore data
		wp_reset_postdata();

	}

	// Return your shortcode output
	return $output;

}
add_shortcode( 'myprefix_custom_grid_faqVideos', 'myprefix_custom_grid_shortcode_faqVideos' );

//Exclude pages from WordPress Search
if (!is_admin()) {
function wpb_search_filter($query) {
if ($query->is_search) {

$query->set( 'post_type', array( 'product', 'post' ) );
}
return $query;
}
add_filter('pre_get_posts','wpb_search_filter');
}



add_action( 'woocommerce_single_product_summary', 'custom_action_after_single_product_title', 10 );
function custom_action_after_single_product_title() { 
    global $product; 
$productBadge = get_field('badgesimage');
if( !empty($productBadge) ){
	echo "<img class='productBadgeRRimg' src=".$productBadge['url']." />";
}
}

add_filter( 'gform_confirmation_anchor_1', '__return_false' );

// put this in functions.php, it will produce code before the form
add_action('woocommerce_checkout_before_customer_details','show_top_cart',9);

// gets the cart template and outputs it before the form
function show_top_cart( ) {
echo '<div class="mainHeading"><h2>Shipping Details</h2></div>';
}

// put this in functions.php, it will produce code before the form
add_action('woocommerce_review_order_before_payment','show_top_paymentDetails',9);

// gets the cart template and outputs it before the form
function show_top_paymentDetails( ) {
echo '<div class="mainHeading"><h2>Payment Details</h2></div>';
}

// Shortcode to output custom PHP in Visual Composer
function wpc_vc_shortcode( $atts ) {
    echo "This is my custom PHP output in Visual Composer!";
}
add_shortcode( 'my_vc_php_output', 'wpc_vc_shortcode');


// Shortcode to output Our Journal Page Our categories sliding bar 

function wpc_vc_shortcodeJournal( $atts ) {

    echo  "<div class='categoryArchiveTop'><ul>";

			$categories = get_categories();
			foreach($categories as $category) {
            	
            	$currentcat= get_queried_object();
                $cat_id= $category->term_id;
                
                if($currentcat->term_id == $cat_id ){
                    $act_class = 'currentCategory';
                }else{
                    $act_class = '';
                }
                        
				$catimg = get_field('category_image',$category);
		
	echo "<li class=' " . $act_class . " ' > ";

	echo "<a href=' " . get_category_link($category->term_id) . " '> ";
	echo "<img src=' " . $catimg . " ' alt=''> ";
	echo $category->name;
	echo "</a></li>";			
		
			}
	echo "</ul></div>";		

}

add_shortcode( 'my_vc_php_outputJournal', 'wpc_vc_shortcodeJournal');


add_action( 'woocommerce_before_shop_loop', 'before_shop_loop' );

function before_shop_loop() {

	echo '  <div id="freeShippingTruck" class="shop">
<div class="truckLeft">
<img src="https://biologi.com.au/wp-content/uploads/2020/04/Free-Shipping.jpg" width="150px">
</div>
<div class="truckRight">
on all Australian orders over $150 + all International orders over $300
</div>
</div>
';
}


add_filter('woocommerce_get_endpoint_url', 'woocommerce_hacks_endpoint_url_filter', 10, 4);
function woocommerce_hacks_endpoint_url_filter($url, $endpoint, $value, $permalink) {
    $downloads = get_option('woocommerce_myaccount_downloads_endpoint', 'downloads');
    if (empty($downloads) == false) {
        if ($endpoint == $downloads) {
            $url = 'https://www.dropbox.com/sh/emjbtll8n65evfc/AAD4NxgHZwHPB_Msk-LHuJFSa?dl=0';
        }
    }
    return $url;
}

/** DISABLED FIELDS **/

/*

function cw_custom_checkbox_fields( $checkout ) {
echo '<div class="cw_custom_class">';
    woocommerce_form_field( 'signature_required', array(
        'type'          => 'checkbox',
        'label'         => __('I am a returning customer so please don’t send the Biologi Product Brochure.'),
        'required'  => false,
    ), $checkout->get_value( 'signature_required' ));
    echo '</div>';
}
add_action('woocommerce_after_checkout_billing_form', 'cw_custom_checkbox_fields');

/*add_action('woocommerce_checkout_process', 'cw_custom_process_checkbox');
function cw_custom_process_checkbox() {
    global $woocommerce;
    if (!$_POST['custom_checkbox1'])
        wc_add_notice( __( 'Notification message.' ), 'error' );
}*/

/*

add_action('woocommerce_checkout_update_order_meta', 'cw_checkout_order_meta');
function cw_checkout_order_meta( $order_id ) {
    if ($_POST['signature_required']) 
    {
        update_post_meta( $order_id, 'custom_checkbox1', esc_attr($_POST['signature_required']));
    }
    if ($_POST['authority_to_leave']) 
    {
        update_post_meta( $order_id, 'authority_to_leave', esc_attr($_POST['authority_to_leave']));
    }
}

// Display the custom field result on the order edit page (backend) when checkbox has been checked
add_action( 'woocommerce_admin_order_data_after_billing_address', 'display_custom_field_on_order_edit_pages', 10, 1 );
function display_custom_field_on_order_edit_pages( $order ){
    $my_field_name = get_post_meta( $order->get_id(), 'custom_checkbox1', true );
    echo '<h3>Other Details</h3>';
    if( $my_field_name == 1 ) {
        echo '<p><strong>I am a returning customer so please don’t send the Biologi Product Brochure:: </strong> <span style="color:red;">Please dont send me brochures</span></p>';
}else{
echo '<p><strong>I am a returning customer so please dont send the Biologi Product Brochure:: </strong> <span style="color:red;">Send me brochures</span></p>';
}
}
*/

/**
 * added by DE to fix php code widget version
 */

add_action( 'widgets_init', function() { register_widget( "PHP_Code_Widget" ); } );

function change_view_cart_link( $params, $handle ) {
 
          switch ($handle) {
 
                case 'wc-add-to-cart':
                    $params['cart_url'] = "https://www.biologi.com.au/checkout/";
                    $params['i18n_view_cart'] = "CHECKOUT"; //chnage Name of view cart button
                break;
            }
            return $params;
}
add_filter( 'woocommerce_get_script_data', 'change_view_cart_link',10,2 );



// Modify the default WooCommerce orderby dropdown
//
// Options: menu_order, popularity, rating, date, price, price-desc
// In this example I'm changing the default "default sorting " to "filter by "
function patricks_woocommerce_catalog_orderby( $orderby ) {
	$orderby["menu_order"] = __('Filter By', 'woocommerce');
	return $orderby;
}
add_filter( "woocommerce_catalog_orderby", "patricks_woocommerce_catalog_orderby", 20 );


add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
   #azh-switch-mode{
	display: none !important; 
	}
  </style>';
}



function blogSidebar_shortcode() {
echo "<div class='blogSidebarRR'>";
$categories = get_the_category();
if ( ! empty( $categories ) ) {
    echo esc_html( $categories[0]->name );   
}
echo "<br>";
echo get_the_date( 'd . m . Y' );
echo "</div>";



}

add_shortcode( 'blogsidebar', 'blogSidebar_shortcode' );

function category_name_shortcode() {
    return get_the_category_list( ', ' );
}
add_shortcode( 'post_category', 'category_name_shortcode' );



add_action('wp_logout','auto_redirect_after_logout');

function auto_redirect_after_logout(){

  wp_redirect( home_url() );
  exit();

}

function mitash_my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(https://www.biologi.com.au/wp-content/uploads/2017/10/Logo-Black.png);
		height:194px;
		width:156px;
		background-size: 156px 194px;
		background-repeat: no-repeat;
        	padding-bottom: 0px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'mitash_my_login_logo' );

// changing the logo link from wordpress.org to your site
function mb_login_url() {  return home_url(); }
add_filter( 'login_headerurl', 'mb_login_url' );

// changing the alt text on the logo to show your site name
function mb_login_title() { return get_option( 'blogname' ); }
add_filter( 'login_headertitle', 'mb_login_title' );

/*
add_action('woocommerce_after_order_notes', 'mi_add_custom_checkbox_fields');
function mi_add_custom_checkbox_fields( $checkout ) {
    //echo '<div class="authority_to_leave_sec"><h3>'.__('Authority to leave(optional)').'</h3>';
    woocommerce_form_field( 'authority_to_leave', array(
        'type'          => 'checkbox',
        'label'         => __('Authority to leave'),
        'required'  => false,
    ), $checkout->get_value( 'authority_to_leave' ));
    //echo '</div>';
}*/

add_action('wp_footer', 'callback_data_custom_script_data');
function callback_data_custom_script_data(){ ?>
    <script>
        jQuery(document).ready(function($){
            /*updated_checkout event*/
            $(document.body).on('updated_checkout', function(){
               // $('body').trigger('update_checkout');
               $.ajax({
                    type:       'POST',
                    url:"<?php echo admin_url( 'admin-ajax.php' ); ?>",
                    data:       {action:'mi_check_for_option'},
                    success:    function( result ) {
                        if(result.success){
                            $('.woocommerce-cart-form__contents .xoo-wsc-shipping.xoo-wsc-tool').remove();
                            $('.woocommerce-cart-form__contents .xoo-wsc-discount.xoo-wsc-tool').remove();
                            $('.woocommerce-cart-form__contents .xoo-wsc-subtotal.xoo-wsc-tool .xoo-wsc-tools-value').remove();
                            var htmlSub = '<span class="xoo-wsc-tools-value">'+result.subtotal+'</span>';
                            $('.woocommerce-cart-form__contents .xoo-wsc-subtotal.xoo-wsc-tool .xoo-wsc-tools-label').after(htmlSub);
                            if(result.discount){
                                var htmldis = '<div class="xoo-wsc-discount xoo-wsc-tool"><span class="xoo-wsc-tools-label">Discount</span><span class="xoo-wsc-tools-value">'+ result.discount+'</span></div>';
                                $('.woocommerce-cart-form__contents .xoo-wsc-subtotal.xoo-wsc-tool').after(htmldis);

                                // manual total update
                                $('.xoo-wsc-total .woocommerce-Price-amount.amount').html('<span class="woocommerce-Price-currencySymbol">$</span> ' + result._total);
                            }
                            var htmlshipping = '<div class="xoo-wsc-shipping xoo-wsc-tool"><span class="xoo-wsc-tools-label">Shipping</span><span class="xoo-wsc-tools-value">'+ result.shipping+'</span></div>';
                            $('.woocommerce-cart-form__contents .xoo-wsc-subtotal.xoo-wsc-tool').after(htmlshipping);
                            
                        }
                    }
                });
            });
        });
    </script>
<?php    
}

add_action('wp_ajax_mi_check_for_option', 'mi_check_for_option');
add_action('wp_ajax_nopriv_mi_check_for_option', 'mi_check_for_option');

function mi_check_for_option(){
    global $woocommerce;
    $send_json = array();
    $send_json = array('success'=>true);
    $send_json['shipping'] = $woocommerce->cart->get_cart_shipping_total();
    $send_json['subtotal'] = $woocommerce->cart->get_cart_subtotal();
    $send_json['_total'] = $woocommerce->cart->total;

    $disTotal = 0;
    if(!empty($woocommerce->cart->coupon_discount_totals)){
        foreach($woocommerce->cart->coupon_discount_totals as $ckey=>$cprice){
            $disTotal = $disTotal + $cprice;
        }
    }
     if(!empty($woocommerce->cart->coupon_discount_tax_totals)){
        foreach($woocommerce->cart->coupon_discount_tax_totals as $cdkey=>$cdprice){
            $disTotal = $disTotal + $cdprice;
        }
    }
    if($disTotal > 0){
        $send_json['discount'] = wc_price($disTotal);
    }
    wp_send_json($send_json);
    die();
}

if( !function_exists('redirect_404_to_homepage') ){

    add_action( 'template_redirect', 'redirect_404_to_homepage' );

    function redirect_404_to_homepage(){
       if(is_404()):
            wp_safe_redirect( home_url('/') );
            exit;
        endif;
    }
}


add_filter( 'woocommerce_product_get_regular_price', 'custom_dynamic_regular_price', 10, 2 );
add_filter( 'woocommerce_product_variation_get_regular_price', 'custom_dynamic_regular_price', 10, 2 );
function custom_dynamic_regular_price( $regular_price, $product ) {
    $capply = mi_apply_coupon_date();
    if($capply){
        if( empty($regular_price) || $regular_price == 0 ){
            return $product->get_price();
        }else{
            return $regular_price;
        }
    }else{
        return $regular_price;
    }    
}


// Generating dynamically the product "sale price"
add_filter( 'woocommerce_product_get_sale_price', 'custom_dynamic_sale_price', 10, 2 );
add_filter( 'woocommerce_product_variation_get_sale_price', 'custom_dynamic_sale_price', 10, 2 );
function custom_dynamic_sale_price( $sale_price, $product ) {
    $capply = mi_apply_coupon_date();
    if($capply){
        $rate = 0.80;
        if( empty($sale_price) || $sale_price == 0 ){
            return $product->get_regular_price() * $rate;
        }else{
            return $sale_price;
        }    
    }else{
        return $sale_price;
    }        
}
// Displayed formatted regular price + sale price
add_filter( 'woocommerce_get_price_html', 'custom_dynamic_sale_price_html', 20, 2 );
function custom_dynamic_sale_price_html( $price_html, $product ) {
    $capply = mi_apply_coupon_date();
    if($capply){
        if( $product->is_type('variable') ) { 
           $variation_ids = $product->get_children();
            if(count($variation_ids) > 1 ){
                return $price_html;
            }
        }
        $price_html = wc_format_sale_price( wc_get_price_to_display( $product, array( 'price' => $product->get_regular_price() ) ), wc_get_price_to_display(  $product, array( 'price' => $product->get_sale_price() ) ) ) . $product->get_price_suffix();
        return $price_html;
    }else{
       return $price_html;
    }    
}

function mi_apply_coupon_date(){
    date_default_timezone_set("Australia/Sydney");
    $couponDate = get_post_meta(124972, '_acfw_schedule_start', true); 
    $couponExDate = get_post_meta(124972, 'expiry_date', true);
    $rolesData = get_post_meta(124972, '_acfw_role_restrictions', true);
    $current_date =  date('Y-m-d');
    $currentDateStr =  strtotime($current_date);
    $couponDateStr = strtotime($couponDate);
    $couponExDateStr = '';
    if($couponExDate !=''){
        $couponExDateStr = strtotime($couponExDate);
    }
    $loginAllow = 1;
    if(!empty($rolesData)){
        if(in_array('guest', $rolesData) && !is_user_logged_in()){
            $loginAllow = 1;
        }else{    
            $loginAllow = 0;
            if(is_user_logged_in()){
                $user = wp_get_current_user();
                foreach ($user->roles as $role) {
            		if(in_array($role,$rolesData)){
            		    $loginAllow = 1;
            		}
        	    }
            }
        }    
    }
    if($couponDateStr <= $currentDateStr && $loginAllow == 1){
        if($couponExDateStr == '' || $currentDateStr <= $couponExDateStr){
            return true;
        }
    }else{
        return false;
    }
}

add_action('wp_footer', function(){
    ?>
    <script type="text/javascript">
        jQuery( document ).ready(function() {
            jQuery(document.body).on('xoo_wsc_cart_updated added_to_cart',function() {
             
                jQuery( document.body ).trigger( 'wc_fragment_refresh' );
            });
        });
    </script>
    <?php
}, 11);

// add_filter('acf/settings/remove_wp_meta_box', '__return_false');

// Add ATL to correct variable to object passed to StarShipIt API
add_action( 'woocommerce_api_create_order', 'my_woocommerce_api_create_order', 10, 2);
function my_woocommerce_api_create_order( $order_id, $data ) {
    $data['order_meta']['authority_to_leave'] = get_post_meta($order_id, 'authority_to_leave', true);
    return $data;
}

function filter_woocommerce_api_order_response( $order_data, $order, $int ) { 
    $order_data['order_meta']['authority_to_leave'] = get_post_meta($order->get_id(), 'authority_to_leave', true);
    return $order_data; 
}; 
add_filter( 'woocommerce_api_order_response', 'filter_woocommerce_api_order_response', 10, 3 ); 

//https://gist.github.com/rameshelamathi/1e51740f6a13a05563deab10216c4c62
add_filter('woo_discount_rules_remove_event_woocommerce_before_calculate_totals', '__return_true');
add_filter('woo_discount_rules_apply_rules_repeatedly', '__return_true');

add_action( 'wp_enqueue_scripts', 'enqueue_custom_scripts' );
function enqueue_custom_scripts() {
    wp_enqueue_style( 'custom-responsive-styles', get_stylesheet_directory_uri() . '/css/responsive.css', array(), 999.9 );
    wp_enqueue_script( 'dev__custom-js', get_stylesheet_directory_uri() . '/js/dev__custom.js', array('jquery'), 99.9, true);
}
    
// Customizable SIde Cart Banner
add_action( 'custom_xoo_wsc_cart_before_head', 'wsc_add_promo_header', 10, 2);
function wsc_add_promo_header() {
    $enable_banner = get_field('enable_banner', 'option');
    $promo_text = get_field('promo_text', 'option');
    $promo_image = get_field('promo_image', 'option');
    $promo_link = get_field('promo_link', 'option');
    $overlay_image = get_field('overlay_image', 'option');

    if(!$enable_banner)
        return false;

    if( !$promo_text || !$promo_image )
        return false;
  
    ob_start();
    ?>  
        <div class="sidecart-banner-wrapper">
            <div class="sidecart-banner-content">
                <a href="<?php echo $promo_link; ?>">
                    <div class="sb-text"><?php echo $promo_text; ?></div>
                </a>
            </div>
        </div>
        <style>
            .sidecart-banner-wrapper{ background-image: url(<?php echo $promo_image; ?>); background-size: cover; background-repeat: no-repeat; }
            .sidecart-banner-content{ padding: 0 20px; min-height: 200px; display: flex; align-items: center; justify-content: center; }
            .sb-text *{ color: #fff !important; }
            <?php if($overlay_image): ?>
                .sidecart-banner-content{ background-color: #00000070; }
            <?php endif; ?>
        </style>
    <?php
    echo ob_get_clean();
}

add_action('acf/init', 'my_acf_op_init');
function my_acf_op_init() {
    // Check function exists.
    if( function_exists('acf_add_options_sub_page') ) {
        // Add parent.
        $parent = acf_add_options_page(array(
            'page_title'  => __('ACF Additional Settings'),
            'menu_title'  => __('Theme Settings'),
            'redirect'    => false,
        ));
        // Add sub page.
        $child = acf_add_options_sub_page(array(
            'page_title'  => __('Side Cart Banner'),
            'menu_title'  => __('Side Cart Banner'),
            'parent_slug' => $parent['menu_slug'],
        ));
    }
}
// @end Customizable SIde Cart Banner

// change checkout field placeholders and labels
// add_filter( 'woocommerce_default_address_fields' , 'custom_override_default_address_fields', 99 );
// function custom_override_default_address_fields( $address_fields ) {
//      $address_fields['address_1']['label'] = 'Address';
// 	    $address_fields['address_1']['placeholder'] = 'Apartment, suite, unit, etc.';
// 	    $address_fields['address_2']['placeholder'] = 'House number and street name';
// 	return $address_fields;
// }


function onea_elated_before_cart_title() {
    echo '<h2>';
    echo esc_html_e( 'Your Cart', 'onea' );
    echo '</h2>';
}