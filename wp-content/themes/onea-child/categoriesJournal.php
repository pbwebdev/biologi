<div class="categoryArchiveTop">
	<ul>
		<?php
			$categories = get_categories();
			foreach($categories as $category) {
            	
            	$currentcat= get_queried_object();
                $cat_id= $category->term_id;
                
                if($currentcat->term_id == $cat_id ){
                    $act_class = 'currentCategory';
                }else{
                    $act_class = '';
                }
                        
				$catimg = get_field('category_image',$category);
		?>
			<li class="<?=$act_class?>">
				<a href="<?=get_category_link($category->term_id)?>">
					<img src="<?=$catimg?>" alt="">
					<?=$category->name?>
				</a>
			</li>
		<?php
			}
		?>
	</ul>
</div>