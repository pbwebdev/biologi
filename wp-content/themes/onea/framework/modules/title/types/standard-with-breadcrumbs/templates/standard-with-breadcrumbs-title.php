<?php do_action('onea_elated_action_before_page_title'); ?>

<div class="eltdf-title-holder nitin <?php echo esc_attr($holder_classes); ?>" <?php onea_elated_inline_style($holder_styles); ?> <?php echo onea_elated_get_inline_attrs($holder_data); ?>>
	<?php if(!empty($title_image)) { ?>
		<div class="eltdf-title-image">
			<img itemprop="image" src="<?php echo esc_url($title_image['src']); ?>" alt="<?php echo esc_attr($title_image['alt']); ?>" />
		</div>
	<?php } ?>
	<div class="eltdf-title-wrapper" <?php onea_elated_inline_style($wrapper_styles); ?>>
		<div class="eltdf-title-inner">
			<div class="eltdf-grid">
			<?php 
				if( is_product_category() ) {
						global $wp_query;
						// get the query object
						$cat_obj = $wp_query->get_queried_object();
						$category_name = $cat_obj->name;
						$category_desc = $cat_obj->description;
						$thumbnail_id = get_term_meta( $cat_obj->term_id, 'thumbnail_id', true );
						$image = wp_get_attachment_url( $thumbnail_id );
						
						?>
						<div class='headerShopArchive' style="background-image: url(<?=$image?>);">
							<div class='headerShopArchiveInner'>
								<span class='cousine'>Products</span>
								<h1><?=$category_name?></h1>
								<div>
								<?=$category_desc?>
								</div>
							</div>
						</div>
						<?php
					} else {
					}
					if( is_shop() ){
						$imageshop = wp_get_attachment_image_src( get_post_thumbnail_id( wc_get_page_id( 'shop' ) ), 'full' );
						$shop_desc = get_field( "biologi_featured_image_title", wc_get_page_id( 'shop' ) );
						?>
						<div class='headerShopArchive' style="background-image: url(<?=$imageshop[0]?>);">
							<div class='headerShopArchiveInner'>
								<span class='cousine'>Products</span>
								<h1>Shop</h1>
								<div>
								<?=$shop_desc?>
								</div>
							</div>
						</div>
						<?php
					}
				?>
				<div class="eltdf-breadcrumbs-info">
					<?php onea_elated_custom_breadcrumbs(); ?>
				</div>
				<div class="eltdf-title-info">
					<?php if(!empty($title)) { ?>
						<<?php echo esc_attr($title_tag); ?> class="eltdf-page-title entry-title" <?php onea_elated_inline_style($title_styles); ?>><?php echo esc_html($title); ?></<?php echo esc_attr($title_tag); ?>>
					<?php } ?>
					<?php if(!empty($subtitle)){ ?>
						<<?php echo esc_attr($subtitle_tag); ?> class="eltdf-page-subtitle" <?php onea_elated_inline_style($subtitle_styles); ?>><?php echo esc_html($subtitle); ?></<?php echo esc_attr($subtitle_tag); ?>>
					<?php } ?>
				</div>
				<?php
				if( !is_product_category() && !is_shop() && is_category() ) {
               
					?>
					<div class="categoryArchiveTop">
					<ul>
						<?php
						$categories = get_categories();
						foreach($categories as $category) {
                        $currentcat= get_queried_object();
                        $cat_id= $category->term_id;
                        if($currentcat->term_id == $cat_id ){
                         $act_class = 'currentCategory';
                        }else{
                        $act_class = '';
                        }
                        
							$catimg = get_field('category_image',$category);
						?>
							<li class="<?=$act_class?>">
								<a href="<?=get_category_link($category->term_id)?>">
									<img src="<?=$catimg?>" alt="">
									<?=$category->name?>
								</a>
							</li>
						<?php
						}
						?>
					</ul>
					</div>
					<?php
				}
				?>
            </div>
	    </div>
	</div>
</div>

<?php do_action('onea_elated_action_after_page_title'); ?>