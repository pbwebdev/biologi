<?php
/**
 * This is the main template file for listing blog posts
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @since 1.0.0
 */

get_header(); ?>
	
	<?php if ( have_posts() ) : ?>
		
		<?php 
			
			$additional_class = '';
			
			if ( is_search() ) {
				$additional_class = 'search-result-list';
			}
			
			if ( is_archive() ) {
				$additional_class = 'archive-list';
			}
			
		?>
		
		<div class="blog-list <?php echo esc_attr( $additional_class ); ?>">
			
			<?php get_template_part( 'template-parts/content-top' ); // Additional top section ?>
			
			<?php while ( have_posts() ) : the_post(); // Start the Loop ?>
					
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
					<?php
					
						$no_image_class = '';
					
					?>
					
					<?php if ( ! is_search() ) : // Do not display the thumbnail on some templates ?>
						
						<?php if ( has_post_thumbnail() ) : ?>
							
							<?php
								
								$image_size = 'enso-full-width';
								$image_array = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), $image_size );
								
								$style_attr = '';
								if ( $image_array ) {
									$style_attr = 'style="height: ' . intval( $image_array[2] ) . 'px;"';
								}
								
								echo '<div class="post-image" ' . $style_attr . '>';
								echo '<a href="' . esc_url( get_permalink() ) . '">';
								the_post_thumbnail( $image_size, array( 'title' => get_the_title() ) );
								echo '</a>';
								echo '</div>';
								
							?>
							
						<?php else : $no_image_class = 'no-image'; ?>
						<?php endif; // has_post_thumbnail() ?>
						
					<?php else : $no_image_class = 'no-image'; ?>
						
					<?php endif; // ! is_search() ?>
					
					<div class="<?php echo esc_attr( implode( ' ', array( 'post-content-container clearfix', $no_image_class ) ) ); ?>">
						
						<div class="post-title-wrapper">
							<h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						</div>
						<div class="post-content-wrapper">
							<div class="post-content excerpt">
								<?php enso_the_excerpt_max_charlength( get_theme_mod( 'enso_ctmzr_general_options_blog_excerpt_length', 400 ) ); ?>
							</div>
								
							<?php if ( ! is_search() ) : // Do not display the post meta on the search result page ?>
								
								<?php get_template_part( 'template-parts/content-blog-meta' ); ?>
								
							<?php endif; // ! is_search() ?>
							
						</div>
						
					</div>
					
				</article>
					
			<?php endwhile; // End the Loop
			
				// Include the post pagination
				get_template_part( 'template-parts/pagination' );
				
			?>
		
		</div>
		<!-- .blog-list -->
		
	<?php
	// If no content, include the "No posts found" template.
	else :
		get_template_part( 'template-parts/content', 'none' );
	endif; // End if ( have_posts() )
	?>
	
<?php get_footer(); ?>