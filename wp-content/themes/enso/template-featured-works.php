<?php 
/**
 * Template Name: Featured Works
 * Description: This template is for showing only the selected works of your portfolio. You can select which categories to display by going to "Appearance > Customize > Portfolio Options".
 * 
 * @since 1.0.0
 */

get_header(); ?>
	
<?php
	
	if ( function_exists( 'uxb_port_init_plugin' ) ) {
		
		$page_content_location = get_theme_mod( 'enso_ctmzr_portfolio_styles_featured_works_show_page_title_content', 'none' );
		
		// Whether to display page title and content on this template (top location)
		if ( 'top' === $page_content_location ) {
			get_template_part( 'template-parts/portfolio-page-content' );
		}
		
		echo '<section class="portfolio-list-section">';
				
			// Display portfolio items
			get_template_part( 'template-parts/portfolio-listing' );
			
			// Additional link button
			$show_button = get_theme_mod( 'enso_ctmzr_portfolio_styles_show_additional_button', true );
			
			if ( $show_button ) {
				
				$button_text = get_theme_mod( 'enso_ctmzr_portfolio_styles_additional_button_text', esc_html__( 'View all works', 'enso' ) );
				$button_url = get_theme_mod( 'enso_ctmzr_portfolio_styles_additional_button_target_url', '#' );
				
				echo '<div class="additional-link-button-wrapper">';
				echo '<a href="' . esc_url( $button_url ) . '" class="button">' . esc_html( $button_text ) . '</a>';
				echo '</div>';
				
			}
			
		echo '</section>';
		
		// Whether to display page title and content on this template (bottom location)
		if ( 'bottom' === $page_content_location ) {
			get_template_part( 'template-parts/portfolio-page-content' );
		}
		
	}
	
?>

<?php get_footer(); ?>