<?php 
/**
 * The template for displaying all page content
 *
 * @since 1.0.0
 */

get_header(); ?>
	
	<?php while ( have_posts() ) : the_post(); // Start the loop ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
			<?php get_template_part( 'template-parts/content', 'page' ); ?>
			
			<!-- Comment Section -->
			<?php 
			
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}
				
			?>
			
		</article>
		
	<?php endwhile; // End of the loop ?>	
	
<?php get_footer(); ?>