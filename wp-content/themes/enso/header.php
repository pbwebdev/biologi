<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the content div.
 *
 * @since 1.0.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5" />
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php esc_url( bloginfo( 'pingback_url' ) ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>
		
<body <?php body_class(); ?> >

	<div id="root-container">
	
		<?php
		
			$menu_type_class = get_theme_mod( 'enso_ctmzr_menu_section_styles_menu_display', 'vertical-menu' );
		
		?>
		
		<header id="header-container" class="content-width <?php echo esc_attr( $menu_type_class ); ?>">
			
			<?php
				
				$additional_classes = '';
				$tagline = get_bloginfo( 'description' );
				$display_tagline = get_theme_mod( 'enso_ctmzr_general_options_show_tagline', true );
				
				if ( ! empty( $tagline ) && $display_tagline ) {
					$additional_classes .= ' with-tagline ';
				}
				
			?>
			
			<!-- Logo and tagline -->
			<div class="site-logo <?php echo esc_attr( $additional_classes ); ?>">
				
				<?php if ( enso_is_site_title_h1_allowed() ) : ?>
					<h1 class="site-title">
				<?php endif; ?>
				
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<?php
						
						$custom_logo_id = get_theme_mod( 'custom_logo' );
						$logo_image_class = '';
						
						if ( ! empty( $custom_logo_id ) ) {
							
							$logo_image_class = 'has-logo-image';
							$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
							$logo_url = $image[0];
							echo '<img src="' . esc_url( $logo_url ) . '" alt="' . esc_attr( get_bloginfo( 'name' ) ) . '" class="logo-image" />';
							
						} else {
							echo esc_html( get_bloginfo( 'name' ) );
						}
						
					?>
					</a>
					
				<?php if ( enso_is_site_title_h1_allowed() ) : ?>
					</h1>
				<?php endif; ?>
				
				<?php
					
					// Print out the tagline if there is any
					if ( ! empty( $tagline ) && $display_tagline ) {
						echo '<span class="tagline ' . esc_attr( $logo_image_class ) . '">' . esc_html( $tagline ) . '</span>';
					}
				
				?>
				
			</div>
			<!-- .site-logo -->
			
			<!-- Site Menu -->
			<nav class="site-menu">
				
				<?php
					
					// Main Menu
					wp_nav_menu( array(
						'container'		 => 'ul',
						'theme_location' => 'main_menu',
						'menu_class'     => 'menu-list menu-style',
					 ) );
					
				?>
				
				<!-- Mobile menu container: the items will be generated in the JS -->
				<div id="mobile-menu">
					<a id="mobile-menu-toggle" href="#mobile-menu-entity">
						<span class="mobile-menu-text"><?php esc_html_e( 'Menu', 'enso' ); ?></span>
						<i class="ion-navicon"></i></a>
					<div id="mobile-menu-entity"></div>
				</div>
				
			</nav>
			
			<?php
			
				$display_search = get_theme_mod( 'enso_ctmzr_other_styles_show_search_button', false );
				
				if ( $display_search ) :
					
					$button_location = get_theme_mod( 'enso_ctmzr_other_styles_search_button_location', 'bottom' );
			?>
			
				<a href="javascript:;" class="search-icon-button <?php echo esc_attr( $button_location ); ?>" title="<?php echo esc_attr__( 'Search', 'enso' ); ?>"><i class="ion-ios-search-strong"></i></a>
				
			<?php endif; ?>
				
		</header>
		
		<main id="content-container" class="content-width">