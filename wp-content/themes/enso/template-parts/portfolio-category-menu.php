<?php 
/**
 * This template part is for displaying portfolio category menu
 * It is called in the template-all-works.php and taxonomy-portfolio.php
 * 
 * @since 1.0.0
 */
?>

<div class="top-section post-item post-content-container clearfix all-works">
	<div class="post-content">
		
	<?php
		
		// Create portfolio categories
		$port_categories = get_terms( 'uxbarn_portfolio_tax', array( 'hide_empty'=>false ) );
		
		if ( ! empty( $port_categories ) && ! is_wp_error( $port_categories ) ) {
			
			echo '<ul class="portfolio-categories">';
			
			// Create the "All" category menu
			$all_categories_text = get_theme_mod( 'enso_ctmzr_portfolio_styles_all_works_everything_menu_text', esc_html__( 'All', 'enso' ) );
			
			if ( is_page_template( 'template-all-works.php' ) ) {
				echo '<li class="all-works active"><h1 class="active-portfolio-category-title">' . $all_categories_text . '</h1></li>';
			} else {
				
				// Find the page URL that is currently using the portfolio-listing.php template
				$page_url = '#';
				
				$pages = get_pages( array(
							'meta_key' 		=> '_wp_page_template',
							'meta_value' 	=> 'template-all-works.php'
						));
				
				// Pick only the first page in array
				if ( ! empty( $pages ) ) {
					$page_url = get_permalink( $pages[0]->ID );
				}
				
				echo '<li class="all-works"><a href="' . esc_url( $page_url ) . '">' . $all_categories_text . '</a></li>';
				
			}
			
			
			// Terms
			$selected_terms = get_theme_mod( 'enso_ctmzr_portfolio_options_all_works_select_categories', array() );
			
			
			// Create portfolio category menu
			foreach ( $port_categories as $term ) {
				
				if ( empty( $selected_terms ) || ( ! empty( $selected_terms ) && in_array( $term->term_id, $selected_terms ) ) ) {
						
					$category_link_text = '<a href="' . esc_url( get_term_link( $term->term_id ) ) . '">' . esc_html( $term->name ) . '</a>';
					$active_class = '';
					$term_class = 'port-term-' . $term->term_id;
					$active_tag_start = '';
					$active_tag_end = '';
					
					if ( is_tax( 'uxbarn_portfolio_tax' ) && $term->term_id == get_queried_object()->term_id ) {
						
						$category_link_text = esc_html( $term->name );
						$active_class = 'active';
						$active_tag_start = '<h1 class="active-portfolio-category-title">';
						$active_tag_end = '</h1>';
						
					}
					
					echo '<li class="' . esc_attr( implode( ' ', array( $active_class, $term_class ) ) ) . '">' . $active_tag_start . $category_link_text . $active_tag_end . '</li>';
					
				}
				
			}
			
			echo '</ul>';
				
		}
		
	?>
	
	</div>
</div>