<?php 
/**
 * This template part is for displaying portfolio items in the Loop
 * It is called in the portfolio-listing.php and taxonomy-portfolio.php
 * 
 * @since 1.0.0
 */
?>

<?php

	/**
	 * Note: All the $_SESSION in this file is initiated in "portfolio-listing.php" file.
	 *
	 */

	if ( has_post_thumbnail() ) {
		
		$image_size = 'enso-port-list';
		
		// If the template enables the full-width view
		if ( $_SESSION['template_full_width_enabled'] ) {
			
			$is_full_width = get_post_meta( get_the_ID(), 'uxbarn_portfolio_enable_full_width', true );
			
			if ( ! empty( $is_full_width ) && 'on' === $is_full_width ) {
				
				// If the .justified-images tag still opens, close it before printing out a new full-width block
				if ( $_SESSION['tag_previously_opened'] ) {
					
					echo '</div>';
					$_SESSION['tag_previously_opened'] = false;
					
				}
				
				echo '<div class="full-width portfolio-item-block">';
				$image_size = 'enso-full-width';
				
			} else {
				
				if ( ! $_SESSION['tag_previously_opened'] ) {
					
					echo '<div class="justified-images portfolio-item-block">';
					$_SESSION['tag_previously_opened'] = true;
					
				}
				
			}
			
		}
		
		
		
		$title_class = '';
		if ( ! get_theme_mod( 'enso_ctmzr_portfolio_styles_show_portfolio_title', true ) ) {
			$title_class = 'visually-hidden';
		}
		
		$attachment = enso_get_attachment( get_post_thumbnail_id() );
		$width = $attachment['width'];
		$height = $attachment['height'];
		$image_orientation_class = 'landscape';
		
		// Portrait orientation photo
		if ( $width/$height < 1 ) {
			$image_orientation_class = 'portrait';
		}
		
		
		echo '<article class="' . esc_attr( implode( ' ', array( 'portfolio-item', 'port-item-' . get_the_ID() ) ) ) . '" data-w="' . intval( $width ) . '" data-h="' . intval( $height ) . '">';
		
		echo '<a href="' . esc_url( get_permalink() ) . '" title="' . esc_attr( get_the_title() ) . '">';
		echo '<div class="' . esc_attr( implode( ' ', array( 'portfolio-thumbnail', $image_orientation_class ) ) ) . '">';
		echo get_the_post_thumbnail( get_the_ID(), $image_size );
		echo '</div>';
		echo '<h3 class="portfolio-title ' . esc_attr( $title_class ) . '">' . esc_html( get_the_title() ) . '</h3>';
		echo '</a>';
		
		echo '</article><!-- item -->'; // .portfolio-item
		
		
		
		// If the template enables the full-width view
		if ( $_SESSION['template_full_width_enabled'] ) {
			
			// If it's full-width or last item, close the block
			if ( ( ! empty( $is_full_width ) && 'on' === $is_full_width ) || 
				 $_SESSION['counter'] === $_SESSION['post_count'] ) {
				
				echo '</div><!-- full-width or last item close -->';
			}
			
			$_SESSION['counter'] += 1;
		
		}
		
	}
	
?>