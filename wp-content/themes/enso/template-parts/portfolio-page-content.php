<?php
/**
 * The template part for displaying page title and content for portfolio page templates
 *
 * @since 1.0.2
 */
?>

<?php while ( have_posts() ) : the_post(); // Start the loop ?>
	
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		<?php get_template_part( 'template-parts/content', 'page' ); ?>
		
	</article>
	
<?php endwhile; // End of the loop ?>