<?php
/**
 * Shared page content for page.php and portfolio-page-content.php
 *
 * @since 1.0.2
 */
?>

<?php if ( has_post_thumbnail() ) : ?>

	<?php
		
		$image_size = 'enso-full-width';
		$image_array = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), $image_size );
		
		$style_attr = '';
		if ( $image_array ) {
			$style_attr = 'style="height: ' . intval( $image_array[2] ) . 'px;"';
		}
		
		echo '<div class="post-image" ' . $style_attr . '>';
		the_post_thumbnail( $image_size );
		echo '</div>';
		
	?>

<?php endif; // has_post_thumbnail() ?>

<div class="post-content-container clearfix">
	
	<div class="post-title-wrapper">
		<?php
		
			$intro = get_post_meta( get_the_ID(), 'enso_section_intro', true );
			$intro_class = '';
			
			if ( ! empty( $intro ) ) {
				$intro_class = 'has-intro';
			}
			
		?>
		
		<?php if ( is_front_page() ) : ?>
			<h2 class="post-title <?php echo esc_attr( $intro_class ); ?>"><?php the_title(); ?></h2>
		<?php else : ?>
			<h1 class="post-title <?php echo esc_attr( $intro_class ); ?>"><?php the_title(); ?></h1>
		<?php endif; ?>
		
		<?php if ( ! empty( $intro ) ) : ?>
			<p class="post-intro">
				<?php echo enso_get_intro_output( $intro ); ?>
			</p>
		<?php endif; ?>
	</div>
	<div class="post-content-wrapper">
		<div class="post-content">
			<?php
			
				the_content();
				enso_print_post_pagination();
				
			?>
		</div>
	</div>
		
</div>
<!-- .post-content-container -->