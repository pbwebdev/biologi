<?php 
/**
 * This template part is for displaying portfolio items
 * It is called in the template-all-works.php and template-featured-works.php
 * 
 * @since 1.0.0
 */
?>

<div class="portfolio-listing clearfix">
	
	<?php
		
		enso_print_portfolio_loading();
		
		$current_template = 'all-works';
		if ( is_page_template( 'template-featured-works.php' ) ) {
			$current_template = 'featured-works';
		}
		$max_items = get_theme_mod( 'enso_ctmzr_portfolio_options_featured_works_number_of_items', 8 );
		
		// Default values to All Works Template
		$nopaging = false;
		$selected_terms = get_theme_mod( 'enso_ctmzr_portfolio_options_all_works_select_categories', array() );
		$posts_per_page = get_theme_mod( 'enso_ctmzr_portfolio_options_all_works_number_of_items', 8 );
		$orderby = get_theme_mod( 'enso_ctmzr_portfolio_options_all_works_portfolio_items_order_by', 'ID' );
		$order = get_theme_mod( 'enso_ctmzr_portfolio_options_all_works_portfolio_items_order', 'DESC' );
		
		// If it's featured works template, then get the value from its options
		if ( 'featured-works' === $current_template ) {
			
			// No pagination on the featured-works template
			$nopaging = true;
			$selected_terms = get_theme_mod( 'enso_ctmzr_portfolio_options_featured_works_select_categories', array() );
			$orderby = get_theme_mod( 'enso_ctmzr_portfolio_options_featured_works_portfolio_items_order_by', 'ID' );
			$order = get_theme_mod( 'enso_ctmzr_portfolio_options_featured_works_portfolio_items_order', 'DESC' );
			
		}
		
		
		// Create tax query array to be merged with WP_Query parameter array
		$tax_query_array = array();
		
		if ( ! empty( $selected_terms ) ) {
			
			$tax_query_array = array(
				'tax_query'		=> array( array(
					'taxonomy'	=> 'uxbarn_portfolio_tax',
					'terms'		=> $selected_terms,
					'operator' 	=> 'IN',
				) ),
			);
			
		}
		
		
		// Prepare the correct value for "paged"
		$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
		if ( is_front_page() ) {
			$paged = get_query_var( 'page' ) ? get_query_var( 'page' ) : 1;
		}
		
		// Prepare WP_Query parameter array
		$args = array_merge( array(
					'post_type'			=> 'uxbarn_portfolio',
					'nopaging'			=> $nopaging,
					'posts_per_page'	=> $posts_per_page,
					'paged'				=> $paged,
					'orderby'			=> $orderby,
					'order'				=> $order,
				), $tax_query_array );
		
		// Get posts
		$query = new WP_Query( $args );
		//echo var_dump($query->request);
		$item_counter = 1;
		
		// Pagination fix for custom query
		$temp_query = $wp_query;
		$wp_query   = NULL;
		$wp_query   = $query;
		
		// Start the custom Loop
		if ( $query->have_posts() ) {
			
			$active_post_count = $query->post_count;
			
			if ( 'featured-works' === $current_template ) {
				
				if ( $max_items < $query->post_count ) {
					$active_post_count = $max_items;
				}
				
			}
			
			// Initiate required sessions [custom-uxbarn-portfolio.php]
			enso_create_portfolio_sessions( $current_template, $active_post_count );
			
			while ( $query->have_posts() ) {
				
				$query->the_post();
				
				if ( 'all-works' === $current_template ) {
					get_template_part( 'template-parts/portfolio-listing-loop' );
				} else {
					
					if ( $item_counter <= $max_items ) {
						get_template_part( 'template-parts/portfolio-listing-loop' );
					}
					
				}
				
				$item_counter++;
				
			}
			
			
			// If the full-width view on the current template is disabled, simply close the "justified-images" div
			if ( ! $_SESSION['template_full_width_enabled'] ) {
				echo '</div><!-- no full-width div -->';
			}
			
		}
		
		// Restore original Post Data
		wp_reset_postdata();
		
	?>
	
</div>
<!-- .portfolio-listing -->

<?php
	
	// Include the post pagination
	get_template_part( 'template-parts/pagination' );

	// Reset main query object (part of pagination fix above)
	$wp_query = NULL;
	$wp_query = $temp_query;

?>