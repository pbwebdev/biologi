<?php
/**
 * Custom template that overrides the default one of UXbarn Portfolio plugin
 *
 * @since 1.0.0
 */
 
 get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	
	<?php 
		
		$post_id = get_the_ID();
		$display_post_format_content = true; 
		$post_format = get_post_meta( $post_id, 'uxbarn_portfolio_item_format', true );
		$no_image_class = '';
		
		if ( 'image-slideshow' === $post_format ) {
			$portfolio_format_content = get_post_meta( $post_id, 'uxbarn_portfolio_image_slideshow', true );
		} else if ( 'video' === $post_format ) {
			$portfolio_format_content = get_post_meta( $post_id, 'uxbarn_portfolio_video_url', true );
		} else { // For mixed format
			$portfolio_format_content = get_post_meta( $post_id, 'uxbarn_portfolio_mixed_content', true );
		}
		
		
		// If the content is empty and password required, then hide the format section
		if ( empty( $portfolio_format_content ) || post_password_required() ) {
		
			$display_post_format_content = false;
			$no_image_class = 'no-image';
				
		}
		
	?>
	
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		<div class="<?php echo esc_attr( implode( ' ', array( 'post-content-container clearfix', $no_image_class ) ) ); ?>">
			
			<div class="post-title-wrapper">
				<?php
				
					$intro = get_post_meta( get_the_ID(), 'enso_section_intro', true );
					$intro_class = '';
					
					if ( ! empty( $intro ) ) {
						$intro_class = 'has-intro';
					}
					
				?>
				<h1 class="post-title <?php echo esc_attr( $intro_class ); ?>"><?php the_title(); ?></h1>
				<?php if ( ! empty( $intro ) ) : ?>
					<p class="post-intro">
						<?php echo enso_get_intro_output( $intro ); ?>
					</p>
				<?php endif; ?>
			</div>
			<div class="post-content-wrapper">
				<div class="post-content">
					<?php
					
						the_content();
						enso_print_post_pagination();
						
					?>
				</div>
			</div>
				
		</div>
		
		<?php if ( $display_post_format_content ) : ?>
		
			<div class="port-format-content">
				<?php
				
					enso_print_portfolio_loading();
						
					// For Images format
					if ( 'image-slideshow' === $post_format ) {
						enso_print_portfolio_image_format_content( $portfolio_format_content );
					} else if ( 'video' === $post_format ) {
						enso_print_portfolio_video_format_content( $portfolio_format_content );
					} else { // For Mixed format
						
						foreach( $portfolio_format_content as $content ) {
							
							if ( 'image-slideshow' === $content['uxbarn_portfolio_mixed_content_type'] ) {
								
								$image_content = $content['uxbarn_portfolio_mixed_content_image_slideshow'];
								if ( ! empty( $image_content ) ) {
									enso_print_portfolio_image_format_content( $image_content );
								}
								
							} else {
								
								$video_content = $content['uxbarn_portfolio_mixed_content_video_url'];
								if ( ! empty( $video_content ) ) {
									enso_print_portfolio_video_format_content( $video_content );
								}
								
							}
							
						}
						
					}
					
				?>
			</div>
			
		<?php endif; // End if ( $display_post_format_content ) : ?>
		
		<?php 
			
			// Comment Section
			if ( get_theme_mod( 'enso_ctmzr_portfolio_options_enable_portfolio_comments', false ) ) {
				
				echo '<div class="post-content-container portfolio-comments clearfix">';
				comments_template(); 
				echo '</div>';
				
			}
			
		?>
		
	</article>

	<?php
		
		if ( get_theme_mod( 'enso_ctmzr_portfolio_options_enable_post_navigation', false ) ) {
				
			the_post_navigation( array(
				
				'prev_text' => '<span class="screen-reader-text">' . esc_html__( 'Previous Post', 'enso' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . esc_html__( 'Previous', 'enso' ) . '</span> <span class="nav-title"><span class="nav-title-icon-wrapper"><i class="ion-ios-arrow-thin-left"></i></span>%title</span>',
				
				'next_text' => '<span class="screen-reader-text">' . esc_html__( 'Next Post', 'enso' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . esc_html__( 'Next', 'enso' ) . '</span><span class="nav-title">%title<span class="nav-title-icon-wrapper"><i class="ion-ios-arrow-thin-right"></i></span></span>',
				
			) );
			
		}
		
	?>
	
<?php endwhile; ?>

<?php get_footer(); ?>