<?php 
/**
 * Display portfolio items of selected category
 * 
 * @since 1.0.0
 */

get_header(); ?>
	
	<?php
		
		if ( get_theme_mod( 'enso_ctmzr_portfolio_options_all_works_show_category_menu', true ) ) {
			
			// Display portfolio category menus
			get_template_part( 'template-parts/portfolio-category-menu' );
			
		}
		
	?>

	<div class="portfolio-listing clearfix">
		
		<?php 
			
			enso_print_portfolio_loading();
			
			enso_create_portfolio_sessions( 'all-works', $wp_query->found_posts );
			
			// Start the loop
			while ( have_posts() ) {
				
				the_post();
				
				// Printing out items
				get_template_part( 'template-parts/portfolio-listing-loop' );
				
			}
			
			// If the full-width view on the current template is disabled, simply close the "justified-images" div
			if ( ! $_SESSION['template_full_width_enabled'] ) {
				echo '</div>';
			}
			
		?>
	
	</div>
	<!-- .portfolio-listing -->
	
	<?php
		
		// Include the post pagination
		get_template_part( 'template-parts/pagination' );
				
	?>
	
<?php get_footer(); ?>