<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the content div and all content after
 *
 * @since 1.0.0
 */
?>
		</main>
		<!-- #content-container -->
		
		<?php
			
			// Footer Widget Area
			get_sidebar();
			
		?>
		
		<!-- Copyright text and site social icons -->
		<div class="copyright-social-wrapper content-width">
			
			<?php
				
				// Copyright Text
				$default_copyright_text = enso_wp_kses_escape( sprintf( __( '&copy; Enso Theme<br/>Designed by <a href="%s">UXBARN</a>', 'enso' ), 'https://uxbarn.com' ) );
				
				// Get the saved copyright text
				$copyright_text = get_theme_mod( 'enso_ctmzr_site_identity_copyright_text', $default_copyright_text );
			
			?>
			
			<div class="inner-copyright-social-wrapper">
			
				<?php if ( ! empty( $copyright_text ) ) : ?>
					<div class="copyright">
						<?php echo enso_wp_kses_escape( $copyright_text ); ?>
					</div>
				<?php endif; ?>
				
				<?php
					
					// Social Icon Set
					enso_display_social_icon_set();
						
				?>
				
			</div>
		</div>
		
	</div>
	<!-- #root-container -->


	<!-- Fullscreen Search Panel -->
	<div id="search-panel-wrapper">
		<div id="inner-search-panel">
			<?php get_search_form( true ); ?>
			<a id="search-close-button" href="javascript:;" title="<?php esc_attr_e( 'Close', 'enso' ); ?>"><i class="ion-ios-close-empty"></i></a>
		</div>
	</div>
	
	<?php wp_footer(); ?>
	
</body>
</html>