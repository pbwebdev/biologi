<?php
/**
 * The main file for setting up and initializing the theme
 *
 * @since 1.0.0
 */
 
 

/**
 * Set up the theme constant(s)
 *
 * @since 1.0.0
 */
define( 'ENSO_THEME_ROOT_IMAGE_URL', get_template_directory_uri() . '/images/' );
define( 'ENSO_DEFAULT_GOOGLE_FONTS', 'Poppins:300,400,500,600,700|Rubik:300,300i,400,400i,500,500i,700,700i' );



/**
 * Include all the required asset files of the theme.
 *
 * @since 1.0.0
 */
require_once( get_template_directory() . '/includes/assets.php' );
require_once( get_template_directory() . '/includes/theme-functions.php' );
require_once( get_template_directory() . '/includes/class-tgm-plugin-activation.php' );
require_once( get_template_directory() . '/includes/customizer/customizer-init.php' );
require_once( get_template_directory() . '/includes/plugin-codes/envato-market-github.php' );



/**
 * Initialize the theme.
 *
 * @since 1.0.0
 */
add_action( 'after_setup_theme', 'enso_init_theme' );

if ( ! function_exists( 'enso_init_theme' ) ) {
	
	function enso_init_theme() {
		
		// Register WP features
		if ( ! isset( $content_width ) ) {
			$content_width = 625;
		}
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		add_theme_support( 'custom-logo' );
		add_theme_support( 'custom-background', array( 'default-color' => 'ffffff' ) );
		
		/*
		 * This theme styles the visual editor on the backend to resemble the theme style,
		 * specifically font, colors, icons, and column width.
		 */
		add_editor_style( array( 'css/editor-style.css', enso_get_google_fonts_url() ) );
		
		
		
		/*
		 * Register theme scripts and styles
		 * [assets.php]
		 */
		add_action( 'wp_enqueue_scripts', 'enso_load_theme_assets' );
		add_action( 'admin_enqueue_scripts', 'enso_load_admin_assets' );
		add_action( 'customize_controls_enqueue_scripts', 'enso_load_customizer_assets' );
		
		
		
		/*
		 * Register main WP modules
		 * [theme-functions.php]
		 */
		add_action( 'init', 'enso_register_menus' );
		add_action( 'widgets_init', 'enso_register_widget_areas' );
		
		
		
		/*
		 * Others
		 * [theme-functions.php]
		 */
		
		// Customize the menu classes
		add_filter( 'nav_menu_css_class', 'enso_customize_menu_item_classes', 10, 3 );
		
		// Modify the classes in WP post_class() for various locations
		add_filter( 'post_class', 'enso_modify_post_classes', 10, 3 );
		
		// Modify the classse in WP body_class()
		add_filter( 'body_class', 'enso_modify_body_classes', 10 );
		
		// Register theme's image sizes
		add_action( 'init', 'enso_register_theme_image_sizes' );
		
		// Change the WP excerpt length (in words count)
		add_filter( 'excerpt_length', 'enso_custom_excerpt_length', 999 );
		
		// Modify page titles
		add_filter( 'the_title', 'enso_modify_page_titles' );
		
		// Adjust posts per page of the search template
		add_action( 'pre_get_posts', 'enso_adjust_search_posts_per_page' );
		
		// Register required and recommended plugins
		add_action( 'tgmpa_register', 'enso_register_additional_plugins' );
		
		
		
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 */
		load_theme_textdomain( 'enso', get_template_directory() . '/languages' );
		
	}
	
}



/*
 * Plugin Customization
 */



/**
 * Customize some options of OptionTree via its filters and actions.
 *
 * @since 1.0.0
 */

if ( class_exists( 'OT_Loader' ) ) {
	
	require_once( get_template_directory() . '/includes/meta-box-shared.php' );
	
	// Remove Settings page
	add_filter( 'ot_show_pages', '__return_false' );

	// Remove its default Theme Options menu
	add_filter( 'ot_use_theme_options', '__return_false' );
	
	// Register meta boxes
	add_action( 'current_screen', 'enso_create_shared_meta_boxes' );

}



/**
 * Customize the UXbarn Portfolio plugin by removing and overriding some parts in the theme
 *
 * @since 1.0.0
 */
if ( function_exists( 'uxb_port_init_plugin' ) ) {
	
	require_once( get_template_directory() . '/includes/plugin-codes/custom-uxbarn-portfolio.php' );

	// Remove plugin image sizes
	remove_action( 'init', 'uxb_port_register_plugin_image_sizes' );
	
	// Remove plugin CSS & JS 
	remove_action( 'wp_enqueue_scripts', 'uxb_port_load_frontend_styles' );
	remove_action( 'wp_enqueue_scripts', 'uxb_port_load_frontend_scripts' );
	remove_action( 'wp_enqueue_scripts', 'uxb_port_load_on_demand_assets' );
	remove_action( 'admin_enqueue_scripts', 'uxb_port_load_admin_assets' );
	
	// Remove plugin meta boxes
	remove_action( 'admin_init', 'uxb_port_create_item_format_setting' );
	remove_action( 'admin_init', 'uxb_port_create_alternate_content' );
	remove_action( 'admin_init', 'uxb_port_create_image_slideshow_format_content' );
	remove_action( 'admin_init', 'uxb_port_create_video_format_content' );
	remove_action( 'admin_init', 'uxb_port_create_meta_info' );
	
	// Remove plugin options
	remove_action( 'init', 'uxb_port_create_plugin_options' );
	
	// Remove plugin functions
	remove_action( 'vc_before_init', 'uxb_port_load_portfolio_element', 11 );
	remove_action( 'init', 'uxb_port_update_element_params', 11 );
	
	// Load custom code for the plugin
	// [custom-uxbarn-portfolio.php]
	add_action( 'init', 'enso_portfolio_custom' );
	
}