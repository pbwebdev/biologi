/*global jQuery:false */

jQuery( document ).ready(function( $ ) {
	"use strict";
	
	
	/* #General
	 ================================================== */
	
	var waitForFinalEvent = (function () {
	  var timers = {};
	  return function (callback, ms, uniqueId) {
		if (!uniqueId) {
		  uniqueId = "Don't call this twice without a uniqueId";
		}
		if (timers[uniqueId]) {
		  clearTimeout (timers[uniqueId]);
		}
		timers[uniqueId] = setTimeout(callback, ms);
	  };
	})();
	
	
	// Display post image after finished loading
	if ( jQuery().imagesLoaded ) {
		
		$( '.post-image' ).imagesLoaded()
		.always( function( instance ) {
			$( '.post-image img' ).css( 'visibility', 'visible' ).animate({
				opacity: 1,
			}, 300, function() {
				$( '.post-image' ).css({
					height: '',
					maxHeight: 'none',
				});
			});
		});
		
	} else {
		console.log( 'imagesLoaded JS is disabled or missing.' );
	}
	
	// Hide border if there is no Next or Prev navigation on post single pages
	if ( $( '.post-navigation .nav-next' ).length === 0 ) {
		$( '.post-navigation .nav-previous' ).css( 'border-right', '0' );
	}
	
	if ( $( '.post-navigation .nav-previous' ).length === 0 ) {
		$( '.post-navigation .nav-next' ).css( 'border-left', '0' );
	}
	
	
	
	/* #Portfolio
	 ================================================== */
	
	if ( jQuery().imagesLoaded ) {
		
		// Use imagesLoaded for both portfolio listing templates and single page
		var countItems = $( '.portfolio-item, .image-wrapper' ).length;
	
		$( '.portfolio-listing, .image-slideshow .port-format-content' ).imagesLoaded()
		.always( function( instance ) {
			
			// Justify images
			initFlexImages();
			
			// Check the height of images to make sure that they don't exceed the original's
			validateImageHeight();
			
			// Display the items one after another
			displayPortfolioItems();
			
		})
		.progress( function( instance, image ) {
			
			if ( image.isLoaded ) {
				
				$( image.img ).closest( '.portfolio-item, .image-wrapper' ).addClass( 'loaded' );
				
				var countLoadedImages = $( '.portfolio-item.loaded, .image-wrapper.loaded' ).length;
				var width = 100 * ( countLoadedImages / countItems ) + '%';
				
				$( '.progress-bar' ).css({
					'width' : width
				});
				
			}
			
		});
		
	} else {
		console.log( 'imagesLoaded JS is disabled or missing.' );
	}
	
	function validateImageHeight() {
		
		// Only do this on portfolio listing templates
		if ( $( '.portfolio-listing' ).length > 0 ) {
			
			$( '.justified-images .portfolio-item' ).each( function() {
				
				var $item = $( this ),
					originalHeight = parseInt( $item.attr( 'data-h' ), 10 ),
					imgClientHeight = parseInt( $item.find( 'img' ).height(), 10 );
				
				// Reset the height to "auto" instead of 100% if the displaying img is taller than the original
				// to prevent blurry img when it is stretched to 100%
				if ( imgClientHeight > originalHeight ) {
					$item.find( 'img' ).css( 'height', 'auto' );
				}
				
			});
			
		}
		
	}
	
	function initFlexImages() {
		
		if ( jQuery().flexImages ) {
			
			// Adjust row height a bit on lower resolutions
			var rowHeightValue = parseInt( ThemeOptions.justified_images_row_height, 10 );
			
			if ( 'undefined' !== typeof Modernizr ) {
					
				if ( Modernizr.mq( '(max-width: 1280px)' ) ) {
					
					rowHeightValue = rowHeightValue * 0.8;
					
					if ( Modernizr.mq( '(max-width: 1000px)' ) ) {
						
						rowHeightValue = rowHeightValue * 0.8;
					
					}
					
				}
				
			} else {
				console.log( 'Modernizr JS is missing.' );
			}
			
			
			$( '.justified-images' ).flexImages({
				container: '.portfolio-item',
				rowHeight: rowHeightValue,
			});
			
		} else {
			console.log( 'flexImages JS is disabled or missing.' );
		}
		
	}
	
	function displayPortfolioItems() {
		
		$( '.portfolio-loading' ).animate({
			opacity: 0,
		}, function() {
			
			$( '.portfolio-loading' ).css( 'display', 'none' );
			$( '.portfolio-listing, .port-format-content' ).css( 'height', 'auto' );
			
			$( '.portfolio-item, .image-wrapper' ).each( function( index ) {
				
				var finalMultiplier = index;
				var displayMode = 'sequential'; // random, sequential
				
				if ( 'random' === displayMode ) {
					finalMultiplier = randomizeNumberFromRange( 0, $( '.portfolio-item, .image-wrapper' ).length - 1 );
				}
				
				var speed = 1;
				if ( $( '.port-format-content' ).length > 0 ) {
					speed = 200;
				}
				
				$( this ).css( 'visibility', 'visible' ).delay( 50 * finalMultiplier ).animate({
					opacity : 1,
				}, speed, function() {
					$( this ).addClass( 'visible' );
				});
				
			});
			
		});
		
	}
	
	// Effect on hovering on portfolio items
	var portfolioEffect = ThemeOptions.portfolio_hover_effect;
	if ( 'none' !== portfolioEffect ) {
			
		// Portfolio item on mouse over/out
		$( '.portfolio-item img' ).on( 'mouseover', function() {
			$( '.portfolio-item img' ).not( $( this ) ).addClass( portfolioEffect );
		}).on( 'mouseout', function() {
			$( '.portfolio-item img' ).removeClass( portfolioEffect );
		});
		
	}
	
	
	
	/* #Site Menu
	 ================================================== */
	if ( jQuery().superfish ) {
		
		// Init the menu and submenu
		$( '.menu-list' ).superfish({
			popUpSelector: '.sub-menu, .children',
			animation: {
				opacity: 'show',
			},
			speed: 300,
			speedOut: 400,
			delay: 500	// milliseconds delay on mouseout
		});
		
	} else {
		console.log( 'superfish JS is disabled or missing.' );
	}
	
	// Adjust the position of first-level submenu (only for vertical menu)
	if ( $( '#header-container' ).hasClass( 'vertical-menu' ) ) {
		
		$( '.menu-list > li > .sub-menu, .menu-list > li > .children' ).each( function() {
			$( this ).css({
				left: $( this ).parent().children( 'a' ).width() + 20,
			});
		});
		
	}
	
	
	
	
	/* #Mobile Menu
	 ================================================== */
	createMobileMenuItems();
	
	if ( jQuery().mmenu ) {
			
		// Initialize the mobile menu
		$( '#mobile-menu-entity' ).mmenu({
			// Options
			extensions 	: [ 'pagedim-black' ],
			slidingSubmenus : false,
			offCanvas : {
				position : 'right',
			},
			navbars	: {
				content : [ 'close' ],
			}
		});
		
	} else {
		console.log( 'mmenu JS is disabled or missing.' );
	}
	
	function createMobileMenuItems() {
		
		var mobileMenuList = $( '<ul />' ).appendTo( $( '#mobile-menu-entity' ) );
		
		var clonedList = $( '.menu-list > li' ).clone();
		clonedList = getGeneratedSubmenu( clonedList );
		clonedList.appendTo( mobileMenuList );
		
	}
	
	// Recursive function for generating submenus
	function getGeneratedSubmenu( list ) {
		
		$( list ).each( function() {
			
			if ( $( this ).find( 'ul' ).length > 0 ) {
				
				var submenu = $( this ).find( 'ul' ).removeAttr( 'style' ).removeAttr( 'class' ); // To remove styles that prevents mobile menu to display properly
				getGeneratedSubmenu( submenu.find( 'li' ) );
				
			}
			
		});
		
		return list;
		
	}
	
	
	
	/* #Search 
	 ================================================== */
	 
	// Change the default placeholder text of the modal search input
	$( '#search-panel-wrapper .search-field' ).attr( 'placeholder', ThemeOptions.modal_search_input_text );
	
	showSearchButton();
	
	function showSearchButton() {
		
		var showSearchButton 	= ThemeOptions.show_search_button,
			$searchButton 		= $( '.search-button' ),
			$searchIconButton 	= $( '.search-icon-button' );
		
		// If the search button is enabled
		if ( '' !== showSearchButton && '0' !== showSearchButton ) {
			
			if ( 'undefined' !== typeof Modernizr ) {
					
				if ( Modernizr.mq( '(max-width: 768px)' ) ) {
					
					$( '#mobile-menu' ).prepend( $searchIconButton.css( 'display', 'inline' ) );
					$( '.search-menu-item' ).hide();
					
				} else {
					
					if ( $( '.menu-list' ).find( '.search-menu-item' ).length == 0 ) {
						$( '.menu-list' ).append( $( '<li class="menu-item search-menu-item"></li>' ).append( $searchButton.css( 'display', 'inline' ) ) );
					}
					
					$( '.search-menu-item' ).show();
					$searchIconButton.css( 'display', 'none' );
					
				}
				
			} else {
				console.log( 'Modernizr JS is missing.' );
			}
			
		}
		
		$( '.site-menu' ).css( 'opacity', 1 );
		
	}
	
	
	var isSearchOpened = false;
	
	$( '.search-button, .search-icon-button' ).on( 'click', function() {
		
		$( '#search-panel-wrapper' ).css( 'display', 'block' ).stop().animate({
			opacity: 1,
		}, 300, function() {
			
			$( '#search-panel-wrapper .search-field' ).focus();
			isSearchOpened = true;
			
		});
		
	});
	
	$( '#search-close-button' ).on( 'click', function() {
		closeSearchPanel();
	});
	
	$( document ).on( 'keyup', function( e ) {
		
		// Escape key
		if ( 27 === e.keyCode ) {
			closeSearchPanel();
		}
		
	});
	
	function closeSearchPanel() {
		
		if ( isSearchOpened ) {
			
			$( '#search-panel-wrapper' ).stop().animate({
				opacity: 0,
			}, 300, function() {
				
				$( this ).css( 'display', 'none' );
				isSearchOpened = false;
				
			});
	
		}
		
	}
	
	
	
	/* #Fancybox 
	 ================================================== */
	 
	var enableLightbox = ThemeOptions.enable_lightbox_wp_gallery;
	if ( enableLightbox === '0' ) {
		enableLightbox = false;
	} else {
		enableLightbox = true;
	}
	
	// Add FancyBox feature to WP gallery and WP images
	if ( enableLightbox ) {
		
		registerFancyBoxToWPGallery();
		registerFancyBoxToWPImage();
		
	}
	 
	function registerFancyBoxToWPGallery() {
		// WP Gallery shortcode
		var $wpGallery = $( '.gallery' );

		$wpGallery.each( function() {
			
			var mainId = $( this ).attr( 'id' );
			var items = $( this ).find( '.gallery-item' ).find( 'a' );

			items.each( function() {

				var href = $( this ).attr( 'href' );
				
				// Check the target file extension, if it is one of the image extension then add Fancybox class
				if ( href.toLowerCase().indexOf( '.jpg' ) >= 0 || href.toLowerCase().indexOf( '.jpeg' ) >= 0 || href.toLowerCase().indexOf( '.png' ) >= 0 || href.toLowerCase().indexOf( '.gif' ) >= 0) {

					$( this ).addClass( 'image-box' );
					$( this ).attr( 'data-fancybox-group', mainId );

				}

			});

		});
	}
	
	function registerFancyBoxToWPImage() {
		
		// Run through WP images on the page
		$( 'img[class*="wp-image-"]' ).each( function() {
			
			// If the image has an anchor tag
			var $parentAnchor = $( this ).closest( 'a' );
			
			if ( $parentAnchor.length > 0 ) {
				
				var href = $parentAnchor.attr( 'href' );
				
				// Check the target file extension, if it is one of the image extension then add Fancybox class
				if (href.toLowerCase().indexOf( '.jpg' ) >= 0 || href.toLowerCase().indexOf( '.jpeg' ) >= 0 || href.toLowerCase().indexOf( '.png' ) >= 0 || href.toLowerCase().indexOf( '.gif' ) >= 0) {

					$parentAnchor.addClass( 'image-box no-slideshow' );

				}
				
			}
			
		});
		
	}
	
	
	callFancyBoxScript();
	
	function callFancyBoxScript() {
		
		if ( jQuery().fancybox ) {
			
			// For portfolio and WP gallery
			$( '.image-box' ).fancybox({
				mouseWheel: false,
				padding: 0,
				closeBtn: false,
				nextEffect: 'fade',
				prevEffect: 'fade',
				//fitToView: false,
				helpers : {
					thumbs : {
						width : 40,
						height : 40,
					},
					overlay: {
						locked: true, // to prevent page jumping to the top when clicking on the object
						//css: { 'background': 'rgba(33,38,31, 0.8)' },
						css: { 'background': 'rgba(255,255,255,1)' },
					},
					title: {
						type : 'outside',
					},
					buttons: {},
				},
				beforeLoad: function() {
					this.title = getImageCaptionText( $( this.element ) );
				},
			});
			
			
			
			// For WP images
			$( '.image-box.no-slideshow' ).fancybox({
				padding: 0,
				helpers : {
					overlay: {
						locked: true, // to prevent page jumping to the top when clicking on the object
						css: { 'background': 'rgba(255,255,255,0.9)' },
					},
					title: {
						type : 'outside',
					},
				},
				beforeLoad: function() {
					this.title = getImageCaptionText( $( this.element ) );
				},
			});
			
		} else {
			console.log( 'Fancybox JS is disabled or missing.' );
		}
		
	}
	
	function getImageCaptionText( $element ) {
		
		// For WP gallery
		if ( $element.closest( '.gallery-item' ).length > 0 ) {
			return $element.closest( '.gallery-item' ).find( '.wp-caption-text' ).html();
		
		// For theme image
		} else if ( $element.closest( '.image-wrapper' ).length > 0 ) {
			return $element.closest( '.image-wrapper' ).find( '.image-caption' ).html();
			
		// For any other cases... it can be normal WP media file (image)
		} else {
			return $element.closest( '.wp-caption' ).find( '.wp-caption-text' ).html();
		}
		
	}
	
	
	
	/* #Misc
	 ================================================== */
	 
	// Make the embed video fit its container
	if ( jQuery().fitVids ) {
		$( '.video-wrapper' ).fitVids();
	} else {
		console.log( 'FitVids JS is disabled or missing.' );
	}
	
	// Hide the underline of the link that wraps around img
	var $wpImages = $( 'img[class*="wp-image-"], img[class*="attachment-"], .widget-item img' );
	if ( $wpImages.closest( 'a' ).length > 0 ) {
		$wpImages.closest( 'a' ).addClass( 'no-border' );
	}
	
	function randomizeNumberFromRange( min, max ) {
		return Math.floor( Math.random() * ( max - min + 1 ) + min );
	}
	
	function log( x ) {
		console.log( x );
	}
	
	
	
	/* #Responsive Related
	 ================================================== */
	moveBlogMeta();
	
	function moveBlogMeta() {
		
		if ( 'undefined' !== typeof Modernizr ) {
				
			if ( Modernizr.mq( '(max-width: 768px)' ) ) {
				
				$( '.single-post .post-content' ).after( $( '.single-post .post-meta-wrapper' ) );
					
			} else {
				
				$( '.single-post .post-title-wrapper' ).append( $( '.single-post .post-meta-wrapper' ) );
					
			}
			
			$( '.post-meta-wrapper' ).css( 'display', 'block' );
			
		} else {
			console.log( 'Modernizr JS is missing.' );
		}
		
	}
	
	
	
	calculatePortfolioItemSpacing();
	
	function calculatePortfolioItemSpacing() {
		
		if ( 'undefined' !== typeof Modernizr ) {
			
			var initSpacing = parseInt( ThemeOptions.portfolio_item_spacing, 10 ),
				halfSpacing = initSpacing / 2,
				finalSpacing = initSpacing;
				
			if ( Modernizr.mq( '(max-width: 1400px)' ) ) {
				
				if ( initSpacing > 30 ) {
					finalSpacing = initSpacing * 0.7143;
				}
				
				halfSpacing = finalSpacing / 2;
				
			}
			
			if ( Modernizr.mq( '(max-width: 1000px)' ) ) {
				
				if ( initSpacing > 30 ) {
					finalSpacing = initSpacing * 0.6;
				}
				
				halfSpacing = finalSpacing / 2;
				
			}
			
			if ( Modernizr.mq( '(max-width: 600px)' ) ) {
				
				finalSpacing = 50;
				
			}
			
			
		
			$( '.justified-images' ).css({
				marginLeft: halfSpacing * -1,
				marginRight: halfSpacing * -1,
			});
			
			$( '.portfolio-item' ).css({
				marginBottom: finalSpacing,
			});
			
			$( '.justified-images .portfolio-item' ).css({
				marginLeft: halfSpacing,
				marginRight: halfSpacing,
			});
			
			
		} else {
			console.log( 'Modernizr JS is missing.' );
		}
		
	}
	
	
	$( window ).on( 'resize', function() {
		
		calculatePortfolioItemSpacing();
		
		waitForFinalEvent(function() {
			initFlexImages();
		}, 500, 'adjust_flex_grid');
		
		moveBlogMeta();
		
	});
		
});