<?php
/**
 * Customizing UXbarn Portfolio plugin
 *
 * @since 1.0.0
 */



if ( ! function_exists( 'enso_portfolio_custom' ) ) {
	
	function enso_portfolio_custom() {
		
		if ( class_exists( 'OT_Loader' ) ) {
			
			// Register custom meta boxes
			add_action( 'admin_init', 'enso_custom_port_create_item_format_setting' );
			add_action( 'admin_init', 'enso_custom_port_create_other_options_meta_boxes' );
			
		}
		
		// Filter to override the plugin's shortcode output
		add_filter( 'uxb_port_load_portfolio_shortcode_filter', 'enso_custom_port_load_portfolio_shortcode', 10, 2 );
		
		// Filter to override the plugin's CPT argument
		add_filter( 'uxb_port_register_cpt_args_filter', 'enso_custom_port_cpt_args' );
		
		// Filter to override the plugin's taxonomy argument
		add_filter( 'uxb_port_register_tax_args_filter', 'enso_custom_port_tax_args' );
		
		// Filter to override the plugin's item list in the back end
		add_filter( 'uxb_port_create_columns_header_filter', 'enso_custom_port_create_columns_header', 10, 3 );
			
		// Modify the query when displaying portfolio items
		add_action( 'pre_get_posts', 'enso_modify_portfolio_query' );
		
	}
	
}




if ( ! function_exists( 'enso_modify_portfolio_query' ) ) {

	function enso_modify_portfolio_query( $query ) {
		
		// This will be applied to portfolio taxonomy template
		if ( $query->is_tax( 'uxbarn_portfolio_tax' ) && 
			 ! is_page_template( 'template-all-works.php' ) && 
			 ! is_page_template( 'template-featured-works.php' ) ) {
			
			$query_values = enso_get_portfolio_query_values();
			
			// Posts per page
			$query->set( 'posts_per_page', $query_values['posts_per_page'] );
			
			// Order by
			$query->set( 'orderby', $query_values['orderby'] );
			
			// Order
			$query->set( 'order', $query_values['order'] );
			
		}
		
	}

}



if ( ! function_exists( 'enso_get_portfolio_query_values' ) ) {

	function enso_get_portfolio_query_values() {
		
		return array(
			'posts_per_page'	=> get_theme_mod( 'enso_ctmzr_portfolio_options_all_works_number_of_items', 8 ),
			'orderby'			=> get_theme_mod( 'enso_ctmzr_portfolio_options_all_works_portfolio_items_order_by', 'ID' ),
			'order'				=> get_theme_mod( 'enso_ctmzr_portfolio_options_all_works_portfolio_items_order', 'DESC' ),
		);
		
	}

}
	



if ( ! function_exists( 'enso_custom_port_cpt_args' ) ) {

	function enso_custom_port_cpt_args( $args ) {
	
		$args = array(
					'label' 			=> esc_html__( 'Portfolio', 'enso' ),
					'labels' 			=> array(
												'singular_name'		=> esc_html__( 'Portfolio', 'enso' ),
												'add_new' 			=> esc_html__( 'Add New Portfolio Item', 'enso' ),
												'add_new_item' 		=> esc_html__( 'Add New Portfolio Item', 'enso' ),
												'new_item' 			=> esc_html__( 'New Portfolio Item', 'enso' ),
												'edit_item' 		=> esc_html__( 'Edit Portfolio Item', 'enso' ),
												'all_items' 		=> esc_html__( 'All Portfolio Items', 'enso' ),
												'view_item' 		=> esc_html__( 'View Portfolio', 'enso' ),
												'search_items' 		=> esc_html__( 'Search Portfolio', 'enso' ),
												'not_found' 		=> esc_html__( 'Nothing found', 'enso' ),
												'not_found_in_trash' => esc_html__( 'Nothing found in Trash', 'enso' ),
											),
					'menu_icon' 		=> ENSO_THEME_ROOT_IMAGE_URL . 'admin/uxbarn-admin-s.jpg',
					'description' 		=> '',
					'public' 			=> true,
					'show_ui' 			=> true,
					'capability_type' 	=> 'post',
					'hierarchical' 		=> false,
					'has_archive' 		=> false,
					'supports' 			=> array( 'title', 'editor', 'thumbnail', 'revisions', 'comments' ),
					'rewrite' 			=> array( 'slug' => esc_html( get_theme_mod( 'enso_ctmzr_portfolio_options_portfolio_slug', esc_html__( 'portfolio', 'enso' ) ) ), 'with_front' => false )
				);
		
		return $args;
		
	}

}



if ( ! function_exists( 'enso_custom_port_tax_args' ) ) {

	function enso_custom_port_tax_args( $args ) {
	
		$args = array(
					'label' 			=> esc_html__( 'Portfolio Categories', 'enso' ), 
					'labels' 			=> array(
											'singular_name' => esc_html__( 'Portfolio Category', 'enso' ),
											'search_items' 	=> esc_html__( 'Search Categories', 'enso' ),
											'all_items' 	=> esc_html__( 'All Categories', 'enso' ),
											'edit_item' 	=> esc_html__( 'Edit Category', 'enso' ), 
											'update_item' 	=> esc_html__( 'Update Category', 'enso' ),
											'add_new_item' 	=> esc_html__( 'Add New Category', 'enso' ),
										),
					'singular_label' 	=> esc_html__( 'Portfolio Category', 'enso' ),
					'hierarchical' 		=> true, 
					'rewrite' 			=> array( 'slug' => esc_html( get_theme_mod( 'enso_ctmzr_portfolio_options_portfolio_category_slug', esc_html__( 'portfolio-category', 'enso' ) ) ) ),
				);
				
		return $args;
	
	}
	
}



if ( ! function_exists( 'enso_custom_port_create_columns_header' ) ) {
	
	function enso_custom_port_create_columns_header( $merged_columns, $defaults, $custom_columns ) {
		
		$custom_columns = array(
			'cb' 			=> '<input type=\"checkbox\" />',
			'title' 		=> esc_html__( 'Title', 'enso' ),
			'cover_image' 	=> esc_html__( 'Featured Image', 'enso' ),
			'item_format' 	=> esc_html__( 'Item Format', 'enso' ),
			'terms' 		=> esc_html__( 'Categories', 'enso' ),
		);
		
		$merged_columns = array_merge( $custom_columns, $defaults );
		
		return $merged_columns;
		
	}
	
}



if ( ! function_exists( 'enso_custom_port_create_columns_content') ) {
	
	function enso_custom_port_create_columns_content( $column ) {
		
		global $post;
		switch ( $column )
		{
			case 'cover_image':
				the_post_thumbnail( 'thumbnail' );
				break;
			case 'item_format':
				echo ucwords( get_post_meta( $post->ID, 'uxbarn_portfolio_item_format', true ) );
				break;
			case 'terms':
				$terms = get_the_terms( $post->ID, 'uxbarn_portfolio_tax' );
				
				if ( ! empty( $terms ) ) {
					$out = array();
					foreach ( $terms as $term )
						$out[] = '<a href="' . esc_url( get_term_link( $term->slug, 'uxbarn_portfolio_tax' ) ) .'">' . $term->name . '</a>';
						echo join( ', ', $out );
				
				} else {
					echo ' ';
				}
				break;
		}
		
	}
	
}



if ( ! function_exists( 'enso_custom_port_create_item_format_setting' ) ) {
		
	function enso_custom_port_create_item_format_setting() {
		
		$args = array(
			'id'          => 'uxbarn_portfolio_item_format_meta_box',
			'title'       => esc_html__( 'Portfolio Item Format Setting', 'enso' ),
			'desc'        => '',
			'pages'       => array( 'uxbarn_portfolio' ),
			'context'     => 'normal',
			'priority'    => 'high',
			'fields'      => array(
				array(
					'id'          => 'uxbarn_portfolio_item_format',
					'label'       => esc_html__( 'Portfolio Item Format', 'enso' ),
					'desc'        => esc_html__( 'Select the format for this item. Then you can manage its content using the option below.', 'enso' ),
					'std'         => 'image-slideshow',
					'type'        => 'select',
					'section'     => 'uxbarn_portfolio_item_format_sec',
					'rows'        => '',
					'post_type'   => '',
					'taxonomy'    => '',
					'class'       => '',
					'choices'     => array( 
					  array(
						'value'       => 'image-slideshow',
						'label'       => esc_html__( 'Image', 'enso' ),
						'src'         => ''
					  ),
					  array(
						'value'       => 'video',
						'label'       => esc_html__( 'Video', 'enso' ),
						'src'         => ''
					  ),
					  array(
						'value'       => 'mixed',
						'label'       => esc_html__( 'Mixed', 'enso' ),
						'src'         => ''
					  ),
					),
				),
				
				
				array(
					'id'          => 'uxbarn_portfolio_image_slideshow',
					'label'       => esc_html__( 'Images', 'enso' ),
					'desc'        => esc_html__( 'The uploaded images will be displayed on the portfolio single page vertically.', 'enso' ),
					'std'         => '',
					'type'        => 'gallery',
					'section'     => 'uxbarn_portfolio_slideshow_format_sec',
					'rows'        => '',
					'post_type'   => '',
					'taxonomy'    => '',
					'class'       => '',
					'condition'		=> 'uxbarn_portfolio_item_format:is(image-slideshow)',
			  	),
			  	
			  	array(
					'id'          => 'uxbarn_portfolio_video_url',
					'label'       => esc_html__( 'Video URL', 'enso' ),
					'desc'        => enso_wp_kses_escape( __( 'Enter the URL to your YouTube or Vimeo video here. <br/><br/>For example, <em>"http://www.youtube.com/watch?v=G_G8SdXktHg"</em> for YouTube. Or, <em>"http://vimeo.com/7449107"</em> for Vimeo.', 'enso' ) ),
					'std'         => '',
					'type'        => 'text',
					'rows'        => '',
					'post_type'   => '',
					'taxonomy'    => '',
					'class'       => '',
					'condition'		=> 'uxbarn_portfolio_item_format:is(video)',
		 	 	),
				
				array(
					'id'          => 'uxbarn_portfolio_mixed_content',
					'label'       => esc_html__( 'Mixed Content', 'enso' ),
					'desc'        => enso_wp_kses_escape( __( 'Click to add new content and choose what content type you want to use. Note that the "title" field will not be used on the front end.', 'enso' ) ),
					'std'         => '',
					'type'        => 'list-item',
					'rows'        => '',
					'post_type'   => '',
					'taxonomy'    => '',
					'class'       => '',
					'condition'		=> 'uxbarn_portfolio_item_format:is(mixed)',
					'settings'		=> array(
						array(
							'id'          => 'uxbarn_portfolio_mixed_content_type',
							'label'       => esc_html__( 'Content Type', 'enso' ),
							'desc'        => '',
							'std'         => 'image-slideshow',
							'type'        => 'select',
							'section'     => 'uxbarn_portfolio_item_format_sec',
							'rows'        => '',
							'post_type'   => '',
							'taxonomy'    => '',
							'class'       => '',
							'choices'     => array( 
								  array(
									'value'       => 'image-slideshow',
									'label'       => esc_html__( 'Image', 'enso' ),
									'src'         => ''
								  ),
								  array(
									'value'       => 'video',
									'label'       => esc_html__( 'Video', 'enso' ),
									'src'         => ''
								  ),
							),
						),
						array(
							'id'          => 'uxbarn_portfolio_mixed_content_image_slideshow',
							'label'       => esc_html__( 'Images', 'enso' ),
							'desc'        => esc_html__( 'The uploaded images will be displayed on the portfolio single page vertically.', 'enso' ),
							'std'         => '',
							'type'        => 'gallery',
							'section'     => 'uxbarn_portfolio_slideshow_format_sec',
							'rows'        => '',
							'post_type'   => '',
							'taxonomy'    => '',
							'class'       => '',
							'condition'		=> 'uxbarn_portfolio_mixed_content_type:is(image-slideshow)',
					  	),
					  	
					  	array(
							'id'          => 'uxbarn_portfolio_mixed_content_video_url',
							'label'       => esc_html__( 'Video URL', 'enso' ),
							'desc'        => enso_wp_kses_escape( __( 'Enter the URL to your YouTube or Vimeo video here. <br/><br/>For example, <em>"http://www.youtube.com/watch?v=G_G8SdXktHg"</em> for YouTube. Or, <em>"http://vimeo.com/7449107"</em> for Vimeo.', 'enso' ) ),
							'std'         => '',
							'type'        => 'text',
							'rows'        => '',
							'post_type'   => '',
							'taxonomy'    => '',
							'class'       => '',
							'condition'		=> 'uxbarn_portfolio_mixed_content_type:is(video)',
				 	 	),
						
					),
			  	),
				
				
			)
		);
		
		ot_register_meta_box( $args );
		
	}

}



if ( ! function_exists( 'enso_custom_port_create_other_options_meta_boxes' ) ) {

	function enso_custom_port_create_other_options_meta_boxes() {
		
		$args = array(
			'id'          => 'uxbarn_portfolio_other_options_meta_box',
			'title'       => esc_html__( 'Other Options', 'enso' ),
			'desc'        => '',
			'pages'       => array( 'uxbarn_portfolio' ),
			'context'     => 'normal',
			'priority'    => 'high',
			'fields'      => array(
				array(
					'id'          => 'uxbarn_portfolio_enable_full_width',
					'label'       => esc_html__( 'Make it full-width on portfolio templates?', 'enso' ),
					'desc'        => enso_wp_kses_escape( __( 'Whether to make the uploaded Featured Image full-width in the content area when displaying on the portfolio listing templates.<p>* To make this works, also make sure that you enabled the full-width view for portfolio templates in "Appearance > Customize > Portfolio Options".</p>', 'enso' ) ),
					'std'         => 'off',
					'type'        => 'on-off',
					'rows'        => '',
					'post_type'   => '',
					'taxonomy'    => '',
					'class'       => '',
				  ),
			)
		);
		
		ot_register_meta_box( $args );
		
	}

}



if ( ! function_exists( 'enso_print_portfolio_loading' ) ) {

	function enso_print_portfolio_loading() {
		?>
		
		<div class="portfolio-loading">
			<span class="loading-text"><?php echo esc_html__( 'Loading', 'enso' ); ?></span>
			<div class="loading-bar">
				<div class="progress-bar"></div>
			</div>
		</div>
		
		<?php
	}
	
}



if ( ! function_exists( 'enso_create_portfolio_sessions' ) ) {

	function enso_create_portfolio_sessions( $current_template, $post_count ) {
		
		// All sessions are to be used in the "portfolio-listing-loop.php" file
		
		$featured_works_full_width_enabled = get_theme_mod( 'enso_ctmzr_portfolio_options_featured_works_enable_full_width_view', true );
		$all_works_full_width_enabled = get_theme_mod( 'enso_ctmzr_portfolio_options_all_works_enable_full_width_view', false );
		
		
		if ( ( 'featured-works' === $current_template && ! $featured_works_full_width_enabled ) ||
			 ( 'all-works' === $current_template && ! $all_works_full_width_enabled ) ) {
			
			$_SESSION['template_full_width_enabled'] = false;
			echo '<div class="justified-images portfolio-item-block">';
			
		} else {
			
			$_SESSION['template_full_width_enabled'] = true;
			$_SESSION['tag_previously_opened'] = false;
			$_SESSION['post_count'] = intval( $post_count );
			$_SESSION['counter'] = 1;
			
		}
		
	}
	
}



if ( ! function_exists( 'enso_print_portfolio_image_format_content' ) ) {

	function enso_print_portfolio_image_format_content( $image_id_list_string ) {
		
		$image_id_array = explode( ',', $image_id_list_string );
		
		foreach( $image_id_array as $image_id ) {
			
			$full_size_attachment = enso_get_attachment( $image_id );
			
			if ( ! empty( $full_size_attachment ) ) {
				
				$width = $full_size_attachment['width'];
				$height = $full_size_attachment['height'];
				$image_size = 'enso-full-width';
				$orientation_class = 'landscape';
				
				// Portrait photo
				if ( $width/$height < 1 ) {
					
					$image_size = 'enso-port-single-portrait';
					$orientation_class = 'portrait';
					
				}
				
				$display_size_attachment = enso_get_attachment( $image_id, $image_size );
				
				if ( ! empty( $display_size_attachment ) ) {
					
					// srcset and sizes attributes
					$img_srcset_attr = wp_get_attachment_image_srcset( $display_size_attachment['id'], $image_size );
					$img_sizes_attr = wp_get_attachment_image_sizes( $display_size_attachment['id'], $image_size );
					$width = $display_size_attachment['width'];
					$height = $display_size_attachment['height'];
					
					echo '<div class="port-format-item image-wrapper ' . esc_attr( $orientation_class ) . '">';
					echo '<div class="inner-image-wrapper">';
					
					$img_code = '<img src="' . esc_url( $display_size_attachment['src'] ) . '" alt="' . esc_attr( $display_size_attachment['alt'] ) . '" width="' . intval( $width ) . '" height="' . intval( $height ) . '" srcset="' . esc_attr( $img_srcset_attr ) . '" sizes="' . esc_attr( $img_sizes_attr ) . '" />';
					
					// Enable lightbox
					$enable_lightbox = get_theme_mod( 'enso_ctmzr_portfolio_options_enable_lightbox', true );
					
					if ( $enable_lightbox ) {
						echo '<a href="' . esc_url( $full_size_attachment['src'] ) . '" class="image-box" data-fancybox-group="portfolio-images">' . $img_code . '</a>';
					} else {
						echo ' ' . $img_code;
					}
					
					// Image caption
					$caption = $display_size_attachment['caption'];
					if ( ! empty( $caption ) ) {
						echo '<div class="image-caption">' . esc_html( $caption ) . '</div>';
					}
					
					echo '</div>'; // .inner-image-wrapper
					echo '</div>'; // .image-wrapper
			
				}
				
			}
			
		}
		
	}
	
}



if ( ! function_exists( 'enso_print_portfolio_video_format_content' ) ) {

	function enso_print_portfolio_video_format_content( $video_url ) {
		
		echo '<div class="port-format-item video-wrapper">' . wp_oembed_get( esc_url( $video_url ) ) . '</div>';
		
	}
	
}