<?php
/**
 * Customizer: Content Styles
 *
 * @since 1.0.0
 */

Kirki::add_section( 'enso_ctmzr_content_styles', array(
	'title'      => esc_attr__( 'Content Styles', 'enso' ),
	'priority'   => 20,
	'capability' => 'edit_theme_options',
) );



/**
 * H1 Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_h1_size',
	'label'    			=> esc_attr__( 'Heading 1 Size (h1)', 'enso' ),
	'tooltip'			=> esc_attr__( 'You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '1.8em',
	'output' 			=> array(
								array(
									'element' => 'h1',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * H2 Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_h2_size',
	'label'    			=> esc_attr__( 'Heading 2 Size (h2)', 'enso' ),
	'tooltip'			=> esc_attr__( 'You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '1.5em',
	'output' 			=> array(
								array(
									'element' => 'h2',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * H3 Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_h3_size',
	'label'    			=> esc_attr__( 'Heading 3 Size (h3)', 'enso' ),
	'tooltip'			=> esc_attr__( 'You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '1.375em',
	'output' 			=> array(
								array(
									'element' => 'h3',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * H4 Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_h4_size',
	'label'    			=> esc_attr__( 'Heading 4 Size (h4)', 'enso' ),
	'tooltip'			=> esc_attr__( 'You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '1.25em',
	'output' 			=> array(
								array(
									'element' => 'h4',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * H5 Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_h5_size',
	'label'    			=> esc_attr__( 'Heading 5 Size (h5)', 'enso' ),
	'tooltip'			=> esc_attr__( 'You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '1.125em',
	'output' 			=> array(
								array(
									'element' => 'h5',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * H6 Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_h6_size',
	'label'    			=> esc_attr__( 'Heading 6 Size (h6)', 'enso' ),
	'tooltip'			=> esc_attr__( 'You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '1em',
	'output' 			=> array(
								array(
									'element' => 'h6',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * Heading Font Weight
 * 
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_content_heading_font_weight',
	'label'    			=> esc_attr__( 'Heading Font Weight', 'enso' ),
	'description' 		=> '',
	'tooltip'      		=> esc_attr__( 'It also depends on the typeface if it supports the selected weight or not.', 'enso' ),
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '600',
	'multiple'			=> 1,
	'choices'     		=> enso_ctmzr_get_font_weight_list(),
	'output' 			=> array(
								array(
									'element' => 'h1, h2, h3, h4, h5, h6, .post-title, .post-item .post-title a, .section-title, .post-navigation .nav-subtitle',
									'property' => 'font-weight',
									'suffix'  => '',
								),
							),
) );



/**
 * Heading Letter Spacing
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_content_heading_letter_spacing',
	'label'    			=> esc_attr__( 'Heading Letter Spacing (px)', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'number',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '0.5',
	'choices'     		=> array(
								'min'  => 0,
								'step' => 0.1,
							),
	'output' 			=> array(
								array(
									'element' => 'h1, h2, h3, h4, h5, h6, .post-title, .post-item .post-title a, .section-title',
									'property' => 'letter-spacing',
									'suffix'  => 'px',
								),
							),
) );


// Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_content_styles_separator0',
	'section'     => 'enso_ctmzr_content_styles',
	'description'     => '<br/><br/>',
) );


/**
 * Content Text Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_content_text_size',
	'label'    			=> esc_attr__( 'Content Text Size', 'enso' ),
	'tooltip'			=> esc_attr__( 'You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '15px',
	'output' 			=> array(
								array(
									'element' => 'body, .post-content, .post-excerpt, .section-content',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * Content Text Font Weight
 * 
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_content_text_font_weight',
	'label'    			=> esc_attr__( 'Content Text Font Weight', 'enso' ),
	'description' 		=> '',
	'tooltip'      		=> esc_attr__( 'It also depends on the typeface if it supports the selected weight or not.', 'enso' ),
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '300',
	'multiple'			=> 1,
	'choices'     		=> enso_ctmzr_get_font_weight_list(),
	'output' 			=> array(
								array(
									'element' => 'body, .post-content, .post-excerpt, .image-caption, .section-content, .post-navigation .nav-title',
									'property' => 'font-weight',
									'suffix'  => '',
								),
							),
) );



/**
 * Content Link Font Weight
 * 
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_content_link_font_weight',
	'label'    			=> esc_attr__( 'Content Link Font Weight', 'enso' ),
	'description' 		=> '',
	'tooltip'      		=> esc_attr__( 'It also depends on the typeface if it supports the selected weight or not.', 'enso' ),
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '500',
	'multiple'			=> 1,
	'choices'     		=> enso_ctmzr_get_font_weight_list(),
	'output' 			=> array(
								array(
									'element' => '.blog-list a, .post-item a, .content-section-wrapper a, .numbers-pagination a, .portfolio-categories a',
									'property' => 'font-weight',
									'suffix'  => '',
								),
							),
) );



// Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_content_styles_separator1',
	'section'     => 'enso_ctmzr_content_styles',
	'description'     => '<br/><br/>',
) );



/**
 * Blog Title Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_blog_title_size',
	'label'    			=> esc_attr__( 'Blog and Page Title Size', 'enso' ),
	'tooltip'			=> esc_attr__( 'It will be applied to the title on the blog and pages. You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '1.6em',
	'output' 			=> array(
								array(
									'element' => '.post-title',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * Intro Font Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_intro_size',
	'label'    			=> esc_attr__( 'Intro Font Size', 'enso' ),
	'tooltip'			=> esc_attr__( 'It will be applied to the intro on the blog and pages. You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '1.2em',
	'output' 			=> array(
								array(
									'element' => '.post-intro',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * Intro Font Weight
 * 
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_intro_font_weight',
	'label'    			=> esc_attr__( 'Intro Font Weight', 'enso' ),
	'description' 		=> '',
	'tooltip'      		=> esc_attr__( 'It also depends on the typeface if it supports the selected weight or not.', 'enso' ),
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '300',
	'multiple'			=> 1,
	'choices'     		=> enso_ctmzr_get_font_weight_list(),
	'output' 			=> array(
								array(
									'element' => '.post-intro',
									'property' => 'font-weight',
									'suffix'  => '',
								),
							),
) );



/**
 * Blog Meta Info Font Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_blog_meta_info_text_size',
	'label'    			=> esc_attr__( 'Blog Meta Info Font Size', 'enso' ),
	'tooltip'			=> esc_attr__( 'It will be applied to the meta info text on the blog template. You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '10px',
	'output' 			=> array(
								array(
									'element' => '.post-meta',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * Page Section Title Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_page_section_title_size',
	'label'    			=> esc_attr__( 'Page Section Title Size', 'enso' ),
	'tooltip'			=> esc_attr__( 'It will be applied to the section title on pages (eg. author profile and comment sections). You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '1.125em',
	'output' 			=> array(
								array(
									'element' => '.section-title',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



// Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_content_styles_separator11',
	'section'     => 'enso_ctmzr_content_styles',
	'description'     => '<br/><br/>',
) );



/**
 * Heading Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_heading_color',
	'label'    			=> esc_attr__( 'Heading Color', 'enso' ),
	'tooltip'			=> esc_attr__( 'The color will be applied to most heading tags in the content section.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '#000000',
	'output' 			=> array(
								array(
									'element' => 'h1, h2, h3, h4, h5, h6, .post-title, .post-item .post-title a, .section-title',
									'property' => 'color',
									'suffix'  => '',
								),
								array(
									'element' => '.post-title::after, .single-post .post-intro::after',
									'property' => 'border-color',
									'suffix'  => '',
								),
							),
) );



/**
 * Intro Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_intro_color',
	'label'    			=> esc_attr__( 'Intro Color', 'enso' ),
	'tooltip'			=> esc_attr__( 'The color will be applied to the intro on posts and pages.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '#989898',
	'output' 			=> array(
								array(
									'element' => '.post-intro',
									'property' => 'color',
									'suffix'  => '',
								),
							),
) );



/**
 * Text Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_text_color',
	'label'    			=> esc_attr__( 'Text Color', 'enso' ),
	'tooltip'			=> esc_attr__( 'The color will be applied to most text in the content section.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '#050505',
	'output' 			=> array(
								array(
									'element' => '#content-container, b, strong',
									'property' => 'color',
									'suffix'  => '',
								),
							),
) );



/**
 * Link Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_link_color',
	'label'    			=> esc_attr__( 'Link Color', 'enso' ),
	'tooltip'			=> esc_attr__( 'The color will be applied to most links in the content section.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '#000000',
	'output' 			=> array(
								array(
									'element' => '.post-item a, .post-meta a, .content-section-wrapper a, .portfolio-categories a',
									'property' => 'color',
									'suffix'  => '',
								),
								array(
									'element' => '.post-item a, .post-meta a, .content-section-wrapper a',
									'property' => 'border-bottom-color',
									'suffix'  => '',
								),
							),
) );



/**
 * Content Background Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_content_bg_color',
	'label'    			=> esc_attr__( 'Content Background Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '#fafafa',
	'output' 			=> array(
								array(
									'element' => '.single-uxbarn_portfolio .post-content-container, .post-item',
									'property' => 'background-color',
									'suffix'  => '',
								),
							),
) );



/**
 * Featured Image Background Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_content_styles_featured_image_bg_color',
	'label'    			=> esc_attr__( 'Featured Image Background Color', 'enso' ),
	'tooltip' 			=> esc_attr__( 'You will see this background color while a featured image is loading.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_content_styles',
	'default'  			=> '#f1f1f1',
	'output' 			=> array(
								array(
									'element' => '.post-image',
									'property' => 'background-color',
									'suffix'  => '',
								),
							),
) );