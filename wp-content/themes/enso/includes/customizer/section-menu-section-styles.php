<?php
/**
 * Customizer: Menu Section Styles
 *
 * @since 1.0.0
 */

Kirki::add_section( 'enso_ctmzr_menu_section_styles', array(
	'title'      => esc_attr__( 'Header Styles', 'enso' ),
	'priority'   => 15,
	'capability' => 'edit_theme_options',
) );


// Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_menu_section_styles_logo_desc',
	'section'     => 'enso_ctmzr_menu_section_styles',
	'description' => esc_attr__( 'Adjust the site title attributes below when you are not using a logo image. If you want to upload a logo image, please go to the "Site Identity" section.', 'enso' ),
) );


/**
 * Site Title Font Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_logo_font_size',
	'label'    			=> esc_attr__( 'Site Title Font Size', 'enso' ),
	'tooltip'	 		=> esc_attr__( 'You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '36px',
	'output' 			=> array(
								array(
									'element' => '.site-logo, .site-logo a, .site-title',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * Site Title Font Weight
 * 
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_logo_font_weight',
	'label'    			=> esc_attr__( 'Site Title Font Weight', 'enso' ),
	'description' 		=> '',
	'tooltip'      		=> esc_attr__( 'It also depends on the typeface if it supports the selected weight or not.', 'enso' ),
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '600',
	'multiple'			=> 1,
	'choices'     		=> enso_ctmzr_get_font_weight_list(),
	'output' 			=> array(
								array(
									'element' => '.site-logo, .site-logo a, .site-title',
									'property' => 'font-weight',
									'suffix'  => '',
								),
							),
) );



/**
 * Site Title Letter Spacing
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_logo_letter_spacing',
	'label'    			=> esc_attr__( 'Site Title Letter Spacing (px)', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'number',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '0.5',
	'choices'     		=> array(
								'min'  => 0,
								'step' => 0.1,
							),
	'output' 			=> array(
								array(
									'element' => '.site-logo, .site-logo a, .site-title',
									'property' => 'letter-spacing',
									'suffix'  => 'px',
								),
							),
) );



/**
 * Site Title Text Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_logo_text_color',
	'label'    			=> esc_attr__( 'Site Title Text Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '#000',
	'output' 			=> array(
								array(
									'element' => '.site-logo, .site-logo a, .site-title',
									'property' => 'color',
									'suffix'  => '',
								),
							),
) );



// Blank Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_portfolio_options_blank1',
	'section'     => 'enso_ctmzr_menu_section_styles',
	'description' => '<br/><br/>',
) );



/**
 * Show Tagline
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_general_options_show_tagline',
	'label'    			=> esc_attr__( 'Show Tagline?', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'switch',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '1',
	'choices' 			=> array(
								'on' 	=> esc_attr__( 'Yes', 'enso' ),
								'off' 	=> esc_attr__( 'No', 'enso' ),
							),
) );



/**
 * Tagline Font Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_tagline_font_size',
	'label'    			=> esc_attr__( 'Tagline Font Size', 'enso' ),
	'tooltip' 			=> esc_attr__( 'You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '15px',
	'output' 			=> array(
								array(
									'element' => '.tagline',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * Tagline Font Weight
 * 
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_tagline_font_weight',
	'label'    			=> esc_attr__( 'Tagline Font Weight', 'enso' ),
	'description' 		=> '',
	'tooltip'      		=> esc_attr__( 'It also depends on the typeface if it supports the selected weight or not.', 'enso' ),
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '300',
	'multiple'			=> 1,
	'choices'     		=> enso_ctmzr_get_font_weight_list(),
	'output' 			=> array(
								array(
									'element' => '.tagline',
									'property' => 'font-weight',
									'suffix'  => '',
								),
							),
) );



/**
 * Tagline Letter Spacing
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_tagline_letter_spacing',
	'label'    			=> esc_attr__( 'Tagline Letter Spacing (px)', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'number',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '0.5',
	'choices'     		=> array(
								'min'  => 0,
								'step' => 0.1,
							),
	'output' 			=> array(
								array(
									'element' => '.tagline',
									'property' => 'letter-spacing',
									'suffix'  => 'px',
								),
							),
) );



/**
 * Tagline Text Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_tagline_text_color',
	'label'    			=> esc_attr__( 'Tagline Text Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '#050505',
	'output' 			=> array(
								array(
									'element' => '.tagline',
									'property' => 'color',
									'suffix'  => '',
								),
							),
) );



// Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_menu_section_styles_separator0',
	'section'     => 'enso_ctmzr_menu_section_styles',
	'description' => '<br/><hr /><br/>',
) );



/**
 * Menu Display
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_menu_display',
	'label'    			=> esc_attr__( 'Menu Display', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> 'vertical-menu',
	'choices' 			=> array(
								'vertical-menu' 	=> esc_attr__( 'Vertically', 'enso' ),
								'horizontal-menu' 	=> esc_attr__( 'Horizontally', 'enso' ),
							),
) );



/**
 * Menu Text Style
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_menu_text_style',
	'label'    			=> esc_attr__( 'Menu Text Style', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> 'uppercase',
	'multiple'			=> 1,
	'choices'     		=> array(
								'none'		=> esc_attr__( 'Normal', 'enso' ),
								'uppercase' => esc_attr__( 'Uppercase', 'enso' ),
								'lowercase' => esc_attr__( 'Lowercase', 'enso' ),
							),
	'output' 			=> array(
								array(
									'element' => '.menu-style',
									'property' => 'text-transform',
									'suffix'  => '',
								),
							),
) );



/**
 * Menu Font Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_menu_font_size',
	'label'    			=> esc_attr__( 'Menu Font Size', 'enso' ),
	'tooltip' 			=> esc_attr__( 'You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '18px',
	'output' 			=> array(
								array(
									'element' => '.menu-style',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * Menu Font Weight
 * 
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_menu_font_weight',
	'label'    			=> esc_attr__( 'Menu Font Weight', 'enso' ),
	'description' 		=> '',
	'tooltip'      		=> esc_attr__( 'It also depends on the typeface if it supports the selected weight or not.', 'enso' ),
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '400',
	'multiple'			=> 1,
	'choices'     		=> enso_ctmzr_get_font_weight_list(),
	'output' 			=> array(
								array(
									'element' => '.menu-style',
									'property' => 'font-weight',
									'suffix'  => '',
								),
							),
) );



/**
 * Menu Letter Spacing
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_menu_letter_spacing',
	'label'    			=> esc_attr__( 'Menu Letter Spacing (px)', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'number',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '2',
	'choices'     		=> array(
								'min'  => 0,
								'step' => 0.1,
							),
	'output' 			=> array(
								array(
									'element' => '.menu-style',
									'property' => 'letter-spacing',
									'suffix'  => 'px',
								),
							),
) );



/**
 * Menu Text Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_menu_text_color',
	'label'    			=> esc_attr__( 'Menu Text Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '#050505',
	'output' 			=> array(
								array(
									'element' => '.menu-list > li > a, #mobile-menu-toggle, .numbers-pagination a, .numbers-pagination .current, .post-navigation a',
									'property' => 'color',
									'suffix'  => '',
								),
							),
) );



/**
 * Active Menu Line Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_active_menu_icon_color',
	'label'    			=> esc_attr__( 'Active Menu Line Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '#050505',
	'output' 			=> array(
								array(
									'element' => '.menu-list > li > a::after',
									'property' => 'border-color',
									'suffix'  => '',
								),
							),
) );



// Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_menu_section_styles_separator2',
	'section'     => 'enso_ctmzr_menu_section_styles',
	'description'     => '<br/><br/>',
) );



/**
 * Submenu Font Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_submenu_font_size',
	'label'    			=> esc_attr__( 'Submenu Font Size', 'enso' ),
	'tooltip' 			=> esc_attr__( 'You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '14px',
	'output' 			=> array(
								array(
									'element' => '.sub-menu',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * Submenu Font Weight
 * 
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_submenu_font_weight',
	'label'    			=> esc_attr__( 'Submenu Font Weight', 'enso' ),
	'description' 		=> '',
	'tooltip'      		=> esc_attr__( 'It also depends on the typeface if it supports the selected weight or not.', 'enso' ),
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '300',
	'multiple'			=> 1,
	'choices'     		=> enso_ctmzr_get_font_weight_list(),
	'output' 			=> array(
								array(
									'element' => '.sub-menu',
									'property' => 'font-weight',
									'suffix'  => '',
								),
							),
) );



/**
 * Submenu Letter Spacing
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_submenu_letter_spacing',
	'label'    			=> esc_attr__( 'Submenu Letter Spacing (px)', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'number',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '1',
	'choices'     		=> array(
								'min'  => 0,
								'step' => 0.1,
							),
	'output' 			=> array(
								array(
									'element' => '.sub-menu',
									'property' => 'letter-spacing',
									'suffix'  => 'px',
								),
							),
) );



/**
 * Submenu Width
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_submenu_width',
	'label'    			=> esc_attr__( 'Submenu Width (px)', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'number',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> 250,
	'choices'     		=> array(
								'min'  => 0,
								'step' => 1,
							),
	'output' 			=> array(
								array(
									'element' => '.sub-menu',
									'property' => 'width',
									'suffix'  => 'px',
								),
							),
) );



/**
 * Submenu Text Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_submenu_text_color',
	'label'    			=> esc_attr__( 'Submenu Text Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '#ffffff',
	'output' 			=> array(
								array(
									'element' => '.menu-list .sub-menu a',
									'property' => 'color',
									'suffix'  => '',
								),
							),
) );



/**
 * Submenu Level 1 Background Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_submenu_lv1_background_color',
	'label'    			=> esc_attr__( 'Submenu Level 1 Background Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '#050505',
	'output' 			=> array(
								array(
									'element' => '.sub-menu',
									'property' => 'background-color',
									'suffix'  => '',
								),
							),
) );



/**
 * Submenu Level 2 Background Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_menu_section_styles_submenu_lv2_background_color',
	'label'    			=> esc_attr__( 'Submenu Level 2 Background Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_menu_section_styles',
	'default'  			=> '#383838',
	'output' 			=> array(
								array(
									'element' => '.sub-menu .sub-menu',
									'property' => 'background-color',
									'suffix'  => '',
								),
							),
) );