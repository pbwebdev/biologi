<?php
/**
 * Customizer: Portfolio Options
 *
 * @since 1.0.0
 */

Kirki::add_section( 'enso_ctmzr_portfolio_options', array(
	'title'      => esc_attr__( 'Portfolio Options', 'enso' ),
	'priority'   => 45,
	'capability' => 'edit_theme_options',
) );



// Featured Works Template
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_portfolio_options_featured_works_title2',
	'section'     => 'enso_ctmzr_portfolio_options',
	'description' => '<h2 class="custom-heading">' . esc_html__( 'Featured Works Template Options', 'enso' ) . '</h2>',
) );



/**
 * Select Categories
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_options_featured_works_select_categories',
	'label'    			=> esc_attr__( 'Select Categories', 'enso' ),
	'tooltip'	 		=> esc_attr__( 'Select which portfolio categories to display on the featured works template. Unchecking all will display the works from all categories.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'multicheck',
	'section'  			=> 'enso_ctmzr_portfolio_options',
	'default'  			=> array(),
	'choices'     		=> Kirki_Helper::get_terms( array( 'taxonomy' => 'uxbarn_portfolio_tax', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) ),
) );



/**
 * Number of Posts on Featured Works Template
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_options_featured_works_number_of_items',
	'label'    			=> esc_attr__( 'Number of Items to Display', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'number',
	'section'  			=> 'enso_ctmzr_portfolio_options',
	'default'  			=> 8,
	'choices'     		=> array(
								'min'  => 1,
								'step' => 1,
							),
) );



/**
 * Portfolio Items Order by
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_options_featured_works_portfolio_items_order_by',
	'label'    			=> esc_attr__( 'Portfolio Items Order by', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_portfolio_options',
	'default'  			=> 'ID',
	'multiple'			=> 1,
	'choices'     		=> array(
								'ID'  		=> esc_attr__( 'ID', 'enso' ),
								'title'  	=> esc_attr__( 'Title', 'enso' ),
								'name'  	=> esc_attr__( 'Slug', 'enso' ),
								'date'  	=> esc_attr__( 'Published Date', 'enso' ),
								'modified'  => esc_attr__( 'Modified Date', 'enso' ),
								'rand'  	=> esc_attr__( 'Random', 'enso' ),
							),
) );



/**
 * Portfolio Items Order
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_options_featured_works_portfolio_items_order',
	'label'    			=> esc_attr__( 'Portfolio Items Order', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_portfolio_options',
	'default'  			=> 'DESC',
	'multiple'			=> 1,
	'choices'     		=> array(
								'ASC'  		=> esc_attr__( 'Ascending', 'enso' ),
								'DESC'  	=> esc_attr__( 'Descending', 'enso' ),
							),
) );



/**
 * Enable Item Full-width View
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_options_featured_works_enable_full_width_view',
	'label'    			=> esc_attr__( 'Enable Item Full-width View?', 'enso' ),
	'tooltip'	 		=> esc_attr__( 'When it is enabled, any portfolio items that are set to use the full-width view will use the entire row to show the featured image. Note that you can set the full-width view on the edit screen of portfolio items.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'switch',
	'section'  			=> 'enso_ctmzr_portfolio_options',
	'default'  			=> '1',
	'choices' 			=> array(
								'on' 	=> esc_attr__( 'Yes', 'enso' ),
								'off' 	=> esc_attr__( 'No', 'enso' ),
							),
) );



// Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_portfolio_options_separator1',
	'section'     => 'enso_ctmzr_portfolio_options',
	'description' => '<br/><hr /><br/>',
) );



// All Works Template
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_portfolio_options_all_works_title2',
	'section'     => 'enso_ctmzr_portfolio_options',
	'description' => '<h2 class="custom-heading">' . esc_html__( 'All Works Template Options', 'enso' ) . '</h2>',
) );



/**
 * Select Categories
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_options_all_works_select_categories',
	'label'    			=> esc_attr__( 'Select Categories', 'enso' ),
	'tooltip' 			=> esc_attr__( 'Select which portfolio categories to display on the all works template. Unchecking all will display the works from all categories.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'multicheck',
	'section'  			=> 'enso_ctmzr_portfolio_options',
	'default'  			=> array(),
	'choices'     		=> Kirki_Helper::get_terms( array( 'taxonomy' => 'uxbarn_portfolio_tax', 'hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC' ) ),
) );



/**
 * Number of Posts on All Works Template
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_options_all_works_number_of_items',
	'label'    			=> esc_attr__( 'Number of Items per Page', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'number',
	'section'  			=> 'enso_ctmzr_portfolio_options',
	'default'  			=> 8,
	'choices'     		=> array(
								'min'  => 1,
								'step' => 1,
							),
) );



/**
 * Portfolio Items Order by
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_options_all_works_portfolio_items_order_by',
	'label'    			=> esc_attr__( 'Portfolio Items Order by', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_portfolio_options',
	'default'  			=> 'ID',
	'multiple'			=> 1,
	'choices'     		=> array(
								'ID'  		=> esc_attr__( 'ID', 'enso' ),
								'title'  	=> esc_attr__( 'Title', 'enso' ),
								'name'  	=> esc_attr__( 'Slug', 'enso' ),
								'date'  	=> esc_attr__( 'Published Date', 'enso' ),
								'modified'  => esc_attr__( 'Modified Date', 'enso' ),
								'rand'  	=> esc_attr__( 'Random', 'enso' ),
							),
) );



/**
 * Portfolio Items Order
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_options_all_works_portfolio_items_order',
	'label'    			=> esc_attr__( 'Portfolio Items Order', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_portfolio_options',
	'default'  			=> 'DESC',
	'multiple'			=> 1,
	'choices'     		=> array(
								'ASC'  		=> esc_attr__( 'Ascending', 'enso' ),
								'DESC'  	=> esc_attr__( 'Descending', 'enso' ),
							),
) );



/**
 * Enable Item Full-width View
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_options_all_works_enable_full_width_view',
	'label'    			=> esc_attr__( 'Enable Item Full-width View?', 'enso' ),
	'tooltip'	 		=> esc_attr__( 'When it is enabled, any portfolio items that are set to use the full-width view will use the entire row to show the featured image. Note that you can set the full-width view on the edit screen of portfolio items.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'switch',
	'section'  			=> 'enso_ctmzr_portfolio_options',
	'default'  			=> '0',
	'choices' 			=> array(
								'on' 	=> esc_attr__( 'Yes', 'enso' ),
								'off' 	=> esc_attr__( 'No', 'enso' ),
							),
) );



// Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_portfolio_options_separator2',
	'section'     => 'enso_ctmzr_portfolio_options',
	'description' => '<br/><hr /><br/>',
) );



// Others
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_portfolio_options_others_title2',
	'section'     => 'enso_ctmzr_portfolio_options',
	'description' => '<h2 class="custom-heading">' . esc_html__( 'Other Options', 'enso' ) . '</h2>',
) );



/**
 * Portfolio Menu Text
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_options_portfolio_menu_text_active_status',
	'label'    			=> esc_attr__( 'Portfolio Menu Text', 'enso' ),
	'tooltip'	 		=> esc_attr__( 'By default, when viewing a portfolio single page, WP will not set the active status on the menu. Use this option to tell WP what text on the menu to be set as active for most portfolio-related pages.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'enso_ctmzr_portfolio_options',
	'default'  			=> esc_html__( 'Portfolio', 'enso' ),
) );



/**
 * Enable Lightbox on Portfolio Single Page
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_options_enable_lightbox',
	'label'    			=> esc_attr__( 'Enable Lightbox on Portfolio Single Pages?', 'enso' ),
	'tooltip'	 		=> esc_attr__( 'Whether to enable the lightbox link on the images on portfolio single pages.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'switch',
	'section'  			=> 'enso_ctmzr_portfolio_options',
	'default'  			=> '1',
	'choices' 			=> array(
								'on' 	=> esc_attr__( 'Yes', 'enso' ),
								'off' 	=> esc_attr__( 'No', 'enso' ),
							),
) );



/**
 * Enable Comments on Portfolio Single Page
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_options_enable_portfolio_comments',
	'label'    			=> esc_attr__( 'Enable Comment Section on Portfolio Single Pages?', 'enso' ),
	'tooltip' 			=> esc_attr__( 'Whether to show or hide the entire comment section on portfolio single pages.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'switch',
	'section'  			=> 'enso_ctmzr_portfolio_options',
	'default'  			=> '0',
	'choices' 			=> array(
								'on' 	=> esc_attr__( 'Yes', 'enso' ),
								'off' 	=> esc_attr__( 'No', 'enso' ),
							),
) );



/**
 * Enable Next/Previous Post Navigation
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_options_enable_post_navigation',
	'label'    			=> esc_attr__( 'Enable Next/Previous Post Navigation?', 'enso' ),
	'tooltip'	 		=> esc_attr__( 'Whether to show or hide the post navigation at the bottom of each portfolio item on its single page.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'switch',
	'section'  			=> 'enso_ctmzr_portfolio_options',
	'default'  			=> '0',
	'choices' 			=> array(
								'on' 	=> esc_attr__( 'Yes', 'enso' ),
								'off' 	=> esc_attr__( 'No', 'enso' ),
							),
) );



// Blank Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_portfolio_options_blank3',
	'section'     => 'enso_ctmzr_portfolio_options',
	'description' => '<br/><br/>',
) );



/**
 * Portfolio Category Slug
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_options_portfolio_category_slug',
	'label'    			=> esc_attr__( 'Portfolio Category Slug', 'enso' ),
	'description' 		=> enso_wp_kses_escape( __( '<p>The slug is displayed in the URL when viewing portfolio category template.</p><p>IMPORTANT: Make sure that the slug is not the same as any page slug.</p><p>*After saving, go to "Settings > Permalinks" and click save to make the new slug work.</p>', 'enso' ) ),
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'enso_ctmzr_portfolio_options',
	'default'  			=> esc_html__( 'portfolio-category', 'enso' ),
) );



/**
 * Portfolio Slug
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_options_portfolio_slug',
	'label'    			=> esc_attr__( 'Portfolio Slug', 'enso' ),
	'description' 		=> enso_wp_kses_escape( __( '<p>The slug is displayed in the URL when viewing portfolio items.</p><p>IMPORTANT: Make sure that the slug is not the same as any page slug.</p><p>*After saving, go to "Settings > Permalinks" and click save to make the new slug work.</p>', 'enso' ) ),
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'enso_ctmzr_portfolio_options',
	'default'  			=> esc_html__( 'portfolio', 'enso' ),
) );



