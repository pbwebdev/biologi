<?php
/**
 * Customizer: Footer Widget Area Styles
 *
 * @since 1.0.0
 */

Kirki::add_section( 'enso_ctmzr_footer_widget_area_styles', array(
	'title'      => esc_attr__( 'Footer Widget Area Styles', 'enso' ),
	'priority'   => 25,
	'capability' => 'edit_theme_options',
) );



/**
 * Show Footer Widget Area?
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_show_footer_widget_area',
	'label'    			=> esc_attr__( 'Show Footer Widget Area?', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'switch',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '1',
	'choices' 			=> array(
								'on' 	=> esc_attr__( 'Yes', 'enso' ),
								'off' 	=> esc_attr__( 'No', 'enso' ),
							),
) );



/**
 * Number of Columns for Footer Widget Area
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_footer_widget_area_column_number',
	'label'    			=> esc_attr__( 'Number of Columns for Footer Widget Area', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '4',
	'multiple'			=> 1,
	'choices'     		=> array(
								'1'  => esc_attr__( '1 Column', 'enso' ),
								'2'  => esc_attr__( '2 Columns', 'enso' ),
								'3'  => esc_attr__( '3 Columns', 'enso' ),
								'4'  => esc_attr__( '4 Columns', 'enso' ),
							),
) );



// Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_footer_widget_area_styles_separator10',
	'section'     => 'enso_ctmzr_footer_widget_area_styles',
	'description' => '<br/><br/>',
) );



/**
 * Widget Title Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_widget_title_size',
	'label'    			=> esc_attr__( 'Widget Title Size', 'enso' ),
	'tooltip' 			=> esc_attr__( 'You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '1.16em',
	'output' 			=> array(
								array(
									'element' => '.widget-title',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * Widget Title Font Weight
 * 
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_widget_title_font_weight',
	'label'    			=> esc_attr__( 'Widget Title Font Weight', 'enso' ),
	'description' 		=> '',
	'tooltip'      		=> esc_attr__( 'It also depends on the typeface if it supports the selected weight or not.', 'enso' ),
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '600',
	'multiple'			=> 1,
	'choices'     		=> enso_ctmzr_get_font_weight_list(),
	'output' 			=> array(
								array(
									'element' => '.widget-title',
									'property' => 'font-weight',
									'suffix'  => '',
								),
							),
) );



/**
 * Widget Title Letter Spacing
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_widget_title_letter_spacing',
	'label'    			=> esc_attr__( 'Widget Title Letter Spacing (px)', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'number',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '2',
	'choices'     		=> array(
								'min'  => 0,
								'step' => 0.1,
							),
	'output' 			=> array(
								array(
									'element' => '.widget-title',
									'property' => 'letter-spacing',
									'suffix'  => 'px',
								),
							),
) );



/**
 * Widget Title Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_widget_title_color',
	'label'    			=> esc_attr__( 'Widget Title Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '#ffffff',
	'output' 			=> array(
								array(
									'element' => '.widget-title',
									'property' => 'color',
									'suffix'  => '',
								),
							),
) );



// Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_footer_widget_area_styles_separator0',
	'section'     => 'enso_ctmzr_footer_widget_area_styles',
	'description' => '<br/><br/>',
) );



/**
 * Text Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_text_size',
	'label'    			=> esc_attr__( 'Widget Text Size', 'enso' ),
	'tooltip'			=> esc_attr__( 'You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '12px',
	'output' 			=> array(
								array(
									'element' => '.theme-widget-area',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * Widget Text Font Weight
 * 
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_widget_text_font_weight',
	'label'    			=> esc_attr__( 'Widget Text Font Weight', 'enso' ),
	'description' 		=> '',
	'tooltip'      		=> esc_attr__( 'It also depends on the typeface if it supports the selected weight or not.', 'enso' ),
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '400',
	'multiple'			=> 1,
	'choices'     		=> enso_ctmzr_get_font_weight_list(),
	'output' 			=> array(
								array(
									'element' => '.theme-widget-area',
									'property' => 'font-weight',
									'suffix'  => '',
								),
							),
) );



/**
 * Widget Link Font Weight
 * 
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_widget_link_font_weight',
	'label'    			=> esc_attr__( 'Widget Link Font Weight', 'enso' ),
	'description' 		=> '',
	'tooltip'      		=> esc_attr__( 'It also depends on the typeface if it supports the selected weight or not.', 'enso' ),
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '500',
	'multiple'			=> 1,
	'choices'     		=> enso_ctmzr_get_font_weight_list(),
	'output' 			=> array(
								array(
									'element' => '.theme-widget-area a',
									'property' => 'font-weight',
									'suffix'  => '',
								),
							),
) );



/**
 * Widget Text Letter Spacing
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_widget_text_letter_spacing',
	'label'    			=> esc_attr__( 'Widget Text Letter Spacing (px)', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'number',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '0.5',
	'choices'     		=> array(
								'min'  => 0,
								'step' => 0.1,
							),
	'output' 			=> array(
								array(
									'element' => '.theme-widget-area',
									'property' => 'letter-spacing',
									'suffix'  => 'px',
								),
							),
) );



/**
 * Text Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_text_color',
	'label'    			=> esc_attr__( 'Widget Text Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '#cccccc',
	'output' 			=> array(
								array(
									'element' => '.theme-widget-area',
									'property' => 'color',
									'suffix'  => '',
								),
							),
) );



/**
 * Link Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_link_color',
	'label'    			=> esc_attr__( 'Widget Link Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '#ffffff',
	'output' 			=> array(
								array(
									'element' => '.theme-widget-area a',
									'property' => 'color',
									'suffix'  => '',
								),
								array(
									'element' => '.theme-widget-area a',
									'property' => 'border-color',
									'suffix'  => '',
								),
							),
) );



/**
 * Footer Background Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_bg_color',
	'label'    			=> esc_attr__( 'Footer Background Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '#cccccc',
	'output' 			=> array(
								array(
									'element' => '.theme-widget-area',
									'property' => 'background',
									'suffix'  => '',
								),
							),
) );



/**
 * Footer Border Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_bg_color',
	'label'    			=> esc_attr__( 'Footer Border Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '#e1e1e1',
	'output' 			=> array(
								array(
									'element' => '.theme-widget-area-border',
									'property' => 'border-color',
									'suffix'  => '',
								),
							),
) );



// Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_footer_widget_area_styles_separator3',
	'section'     => 'enso_ctmzr_footer_widget_area_styles',
	'description' => '<br/><hr /><br/>',
) );



/**
 * Copyright Font Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_copyright_font_size',
	'label'    			=> esc_attr__( 'Copyright Font Size', 'enso' ),
	'tooltip'			=> esc_attr__( 'Adjust the copyright font size. You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '12px',
	'output' 			=> array(
								array(
									'element' => '.copyright',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * Copyright Font Weight
 * 
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_copyright_font_weight',
	'label'    			=> esc_attr__( 'Copyright Text Font Weight', 'enso' ),
	'description' 		=> '',
	'tooltip'      		=> esc_attr__( 'It also depends on the typeface if it supports the selected weight or not.', 'enso' ),
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '400',
	'multiple'			=> 1,
	'choices'     		=> enso_ctmzr_get_font_weight_list(),
	'output' 			=> array(
								array(
									'element' => '.copyright',
									'property' => 'font-weight',
									'suffix'  => '',
								),
							),
) );



/**
 * Copyright Link Font Weight
 * 
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_copyright_link_font_weight',
	'label'    			=> esc_attr__( 'Copyright Link Font Weight', 'enso' ),
	'description' 		=> '',
	'tooltip'      		=> esc_attr__( 'It also depends on the typeface if it supports the selected weight or not.', 'enso' ),
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '600',
	'multiple'			=> 1,
	'choices'     		=> enso_ctmzr_get_font_weight_list(),
	'output' 			=> array(
								array(
									'element' => '.copyright a, .copyright a:hover',
									'property' => 'font-weight',
									'suffix'  => '',
								),
							),
) );



/**
 * Copyright Letter Spacing
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_copyright_letter_spacing',
	'label'    			=> esc_attr__( 'Copyright Letter Spacing (px)', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'number',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '0.5',
	'choices'     		=> array(
								'min'  => 0,
								'step' => 0.1,
							),
	'output' 			=> array(
								array(
									'element' => '.copyright',
									'property' => 'letter-spacing',
									'suffix'  => 'px',
								),
							),
) );



/**
 * Copyright Text Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_copyright_text_color',
	'label'    			=> esc_attr__( 'Copyright Text Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '#050505',
	'output' 			=> array(
								array(
									'element' => '.copyright',
									'property' => 'color',
									'suffix'  => '',
								),
							),
) );



/**
 * Copyright Link Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_copyright_link_color',
	'label'    			=> esc_attr__( 'Copyright Link Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '#000000',
	'output' 			=> array(
								array(
									'element' => '.copyright a, .copyright a:hover',
									'property' => 'color',
									'suffix'  => '',
								),
								array(
									'element' => '.copyright a, .copyright a:hover',
									'property' => 'border-color',
									'suffix'  => '',
								),
							),
) );



// Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_footer_widget_area_styles_separator4',
	'section'     => 'enso_ctmzr_footer_widget_area_styles',
	'description' => '<br/><br/>',
) );



/**
 * Social Icon Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_social_icon_size',
	'label'    			=> esc_attr__( 'Social Icon Size', 'enso' ),
	'tooltip'			=> esc_attr__( 'Adjust the social icon size. You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '16px',
	'output' 			=> array(
								array(
									'element' => '.social-icons',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * Social Icon Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_footer_widget_area_styles_social_icon_color',
	'label'    			=> esc_attr__( 'Social Icon Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_footer_widget_area_styles',
	'default'  			=> '#000000',
	'output' 			=> array(
								array(
									'element' => '.social-icons a',
									'property' => 'color',
									'suffix'  => '',
								),
							),
) );