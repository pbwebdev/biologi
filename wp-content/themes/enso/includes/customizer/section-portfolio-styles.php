<?php
/**
 * Customizer: Portfolio Styles
 *
 * @since 1.0.0
 */

Kirki::add_section( 'enso_ctmzr_portfolio_styles', array(
	'title'      => esc_attr__( 'Portfolio Styles', 'enso' ),
	'priority'   => 40,
	'capability' => 'edit_theme_options',
) );



// General Portfolio Styles
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_portfolio_styles_title1',
	'section'     => 'enso_ctmzr_portfolio_styles',
	'description' => '<h2 class="custom-heading">' . esc_html__( 'General Portfolio Styles', 'enso' ) . '</h2>',
) );



/**
 * Show Portfolio Title
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_show_portfolio_title',
	'label'    			=> esc_attr__( 'Show Portfolio Title?', 'enso' ),
	'tooltip'	 		=> esc_attr__( 'Whether to show the portfolio title that displays under the featured image on the portfolio listing templates.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'switch',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '1',
	'choices' 			=> array(
								'on' 	=> esc_attr__( 'Yes', 'enso' ),
								'off' 	=> esc_attr__( 'No', 'enso' ),
							),
) );



/**
 * Portfolio Title Text Style
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_title_text_style',
	'label'    			=> esc_attr__( 'Portfolio Title Text Style', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> 'uppercase',
	'multiple'			=> 1,
	'choices'     		=> array(
								'none'		=> esc_attr__( 'Normal', 'enso' ),
								'uppercase' => esc_attr__( 'Uppercase', 'enso' ),
								'lowercase' => esc_attr__( 'Lowercase', 'enso' ),
							),
	'output' 			=> array(
								array(
									'element' => '.portfolio-title',
									'property' => 'text-transform',
									'suffix'  => '',
								),
							),
) );



/**
 * Portfolio Title Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_title_text_size',
	'label'    			=> esc_attr__( 'Portfolio Title Size', 'enso' ),
	'tooltip'			=> esc_attr__( 'It will be applied to the portfolio title on the Featured Works and All Works templates. You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '14px',
	'output' 			=> array(
								array(
									'element' => '.portfolio-title',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * Portfolio Title Font Weight
 * 
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_title_font_weight',
	'label'    			=> esc_attr__( 'Portfolio Title Font Weight', 'enso' ),
	'description' 		=> '',
	'tooltip'      		=> esc_attr__( 'It also depends on the typeface if it supports the selected weight or not.', 'enso' ),
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '600',
	'multiple'			=> 1,
	'choices'     		=> enso_ctmzr_get_font_weight_list(),
	'output' 			=> array(
								array(
									'element' => '.portfolio-title',
									'property' => 'font-weight',
									'suffix'  => '',
								),
							),
) );



/**
 * Portfolio Title Letter Spacing
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_title_letter_spacing',
	'label'    			=> esc_attr__( 'Portfolio Title Letter Spacing (px)', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'number',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '1.5',
	'choices'     		=> array(
								'min'  => 0,
								'step' => 0.1,
							),
	'output' 			=> array(
								array(
									'element' => '.portfolio-title',
									'property' => 'letter-spacing',
									'suffix'  => 'px',
								),
							),
) );



/**
 * Portfolio Title Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_title_color',
	'label'    			=> esc_attr__( 'Portfolio Title Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '#000',
	'output' 			=> array(
								array(
									'element' => '.portfolio-title',
									'property' => 'color',
									'suffix'  => '',
								),
							),
) );


// Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_portfolio_styles_separator4',
	'section'     => 'enso_ctmzr_portfolio_styles',
	'description'     => '<br/><br/>',
) );



/**
 * Portfolio Loading Text Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_loading_text_color',
	'label'    			=> esc_attr__( 'Portfolio Loading Text Color', 'enso' ),
	'tooltip' 			=> esc_attr__( 'The loading text and bar display on the portfolio Featured Works and All Works templates.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '#050505',
	'output' 			=> array(
								array(
									'element' => '.loading-text',
									'property' => 'color',
									'suffix'  => '',
								),
							),
) );



/**
 * Portfolio Loading Bar Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_loading_bar_color',
	'label'    			=> esc_attr__( 'Portfolio Loading Bar Color', 'enso' ),
	'tooltip' 			=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '#050505',
	'output' 			=> array(
								array(
									'element' => '.progress-bar',
									'property' => 'background',
									'suffix'  => '',
								),
							),
) );



/**
 * Portfolio Loading Bar Background Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_loading_bar_bg_color',
	'label'    			=> esc_attr__( 'Portfolio Loading Bar Background Color', 'enso' ),
	'tooltip' 			=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '#f1f1f1',
	'output' 			=> array(
								array(
									'element' => '.loading-bar',
									'property' => 'background',
									'suffix'  => '',
								),
							),
) );



// Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_portfolio_styles_separator10',
	'section'     => 'enso_ctmzr_portfolio_styles',
	'description'     => '<br/><br/>',
) );



/**
 * Spacing Between Items
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_item_spacing',
	'label'    			=> esc_attr__( 'Spacing Between Items (px)', 'enso' ),
	'tooltip' 			=> '',
	'help'        		=> '',
	'type'     			=> 'number',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '70',
	'choices'     		=> array(
								'min'  => 0,
								'step' => 1,
							),
) );



/**
 * Max Row Height
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_max_row_height',
	'label'    			=> esc_attr__( 'Max Row Height (px)', 'enso' ),
	'tooltip' 			=> esc_attr__( 'The theme will try to arrange portfolio images within this maximum row height. Note that this will NOT have any effect on portfolio items that are using the full-width view.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'number',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '500',
	'choices'     		=> array(
								'min'  => 0,
								'step' => 1,
							),
) );



/**
 * Hover Effect
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_hover_effect',
	'label'    			=> esc_attr__( 'Hover Effect', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> 'blur',
	'multiple'			=> 1,
	'choices'     		=> array(
								'none' => esc_attr__( 'None', 'enso' ),
								'blur' => esc_attr__( 'Blur', 'enso' ),
								'grayscale' => esc_attr__( 'Grayscale', 'enso' ),
								'blur-grayscale' => esc_attr__( 'Blur + Grayscale', 'enso' ),
							),
) );



// Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_portfolio_styles_separator55',
	'section'     => 'enso_ctmzr_portfolio_styles',
	'description'     => '<br/><hr/><br/>',
) );



// Featured Works Template Styles
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_portfolio_styles_title2',
	'section'     => 'enso_ctmzr_portfolio_styles',
	'description' => '<h2 class="custom-heading">' . esc_html__( 'Featured Works Template Styles', 'enso' ) . '</h2>',
) );



/**
 * Show Page Content
 *
 * @since 1.0.2
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_featured_works_show_page_title_content',
	'label'    			=> esc_attr__( 'Show Page Content?', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> 'none',
	'multiple'			=> 1,
	'choices'     		=> array(
								'none' => esc_attr__( 'No', 'enso' ),
								'top' => esc_attr__( 'Above Portfolio', 'enso' ),
								'bottom' => esc_attr__( 'Below Portfolio', 'enso' ),
							),
) );



/**
 * Show Button
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_show_additional_button',
	'label'    			=> esc_attr__( 'Show Additional Button?', 'enso' ),
	'tooltip'	 		=> esc_attr__( 'This is the button that displays at the bottom of the portfolio Featured Works template.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'switch',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '1',
	'choices' 			=> array(
								'on' 	=> esc_attr__( 'Yes', 'enso' ),
								'off' 	=> esc_attr__( 'No', 'enso' ),
							),
) );



/**
 * Button Text
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_additional_button_text',
	'label'    			=> esc_attr__( 'Button Text', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> esc_html__( 'View all works', 'enso' ),
) );



/**
 * Button Target URL
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_additional_button_target_url',
	'label'    			=> esc_attr__( 'Button Target URL', 'enso' ),
	'tooltip'	 		=> esc_attr__( 'Users will be navigated to the specified URL.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '',
) );



// Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_portfolio_styles_separator66',
	'section'     => 'enso_ctmzr_portfolio_styles',
	'description'     => '<br/><br/>',
) );



/**
 * Button Text Style
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_additional_button_text_style',
	'label'    			=> esc_attr__( 'Button Text Style', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> 'uppercase',
	'multiple'			=> 1,
	'choices'     		=> array(
								'none'		=> esc_attr__( 'Normal', 'enso' ),
								'uppercase' => esc_attr__( 'Uppercase', 'enso' ),
								'lowercase' => esc_attr__( 'Lowercase', 'enso' ),
							),
	'output' 			=> array(
								array(
									'element' => '.additional-link-button-wrapper .button',
									'property' => 'text-transform',
									'suffix'  => '',
								),
							),
) );



/**
 * Button Text Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_additional_button_text_size',
	'label'    			=> esc_attr__( 'Button Text Size', 'enso' ),
	'tooltip'			=> '',
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '18px',
	'output' 			=> array(
								array(
									'element' => '.additional-link-button-wrapper .button',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * Button Font Weight
 * 
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_additional_button_font_weight',
	'label'    			=> esc_attr__( 'Button Font Weight', 'enso' ),
	'description' 		=> '',
	'tooltip'      		=> esc_attr__( 'It also depends on the typeface if it supports the selected weight or not.', 'enso' ),
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '600',
	'multiple'			=> 1,
	'choices'     		=> enso_ctmzr_get_font_weight_list(),
	'output' 			=> array(
								array(
									'element' => '.additional-link-button-wrapper .button',
									'property' => 'font-weight',
									'suffix'  => '',
								),
							),
) );



/**
 * Button Letter Spacing
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_additional_button_letter_spacing',
	'label'    			=> esc_attr__( 'Button Letter Spacing (px)', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'number',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '2',
	'choices'     		=> array(
								'min'  => 0,
								'step' => 0.1,
							),
	'output' 			=> array(
								array(
									'element' => '.additional-link-button-wrapper .button',
									'property' => 'letter-spacing',
									'suffix'  => 'px',
								),
							),
) );



/**
 * Button Text Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_additional_button_color',
	'label'    			=> esc_attr__( 'Button Text Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '#050505',
	'output' 			=> array(
								array(
									'element' => '.additional-link-button-wrapper .button',
									'property' => 'color',
									'suffix'  => '',
								),
							),
) );



/**
 * Button Background Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_additional_button_bg_color',
	'label'    			=> esc_attr__( 'Button Background Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '#ffffff',
	'output' 			=> array(
								array(
									'element' => '.additional-link-button-wrapper .button',
									'property' => 'background',
									'suffix'  => '',
								),
							),
) );



/**
 * Button Border Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_additional_button_border_color',
	'label'    			=> esc_attr__( 'Button Border Color', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '#888888',
	'output' 			=> array(
								array(
									'element' => '.additional-link-button-wrapper .button',
									'property' => 'border-color',
									'suffix'  => '',
								),
							),
) );



// Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_portfolio_styles_separator3',
	'section'     => 'enso_ctmzr_portfolio_styles',
	'description'     => '<br/><hr/><br/>',
) );



// All Works Template Styles
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_portfolio_styles_title3',
	'section'     => 'enso_ctmzr_portfolio_styles',
	'description' => '<h2 class="custom-heading">' . esc_html__( 'All Works Template Styles', 'enso' ) . '</h2>',
) );



/**
 * Show Page Content
 *
 * @since 1.0.2
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_all_works_show_page_title_content',
	'label'    			=> esc_attr__( 'Show Page Content?', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> 'none',
	'multiple'			=> 1,
	'choices'     		=> array(
								'none' => esc_attr__( 'No', 'enso' ),
								'top' => esc_attr__( 'Above Portfolio', 'enso' ),
								'bottom' => esc_attr__( 'Below Portfolio', 'enso' ),
							),
) );



Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_portfolio_styles_desc3',
	'section'     => 'enso_ctmzr_portfolio_styles',
	'description' => '<p class="custom-description">' . esc_html__( 'Note that the colors of the category menu are inherited from the text and link colors of the content styles. You can adjust them in the Content Styles section.', 'enso' ) . '</p>',
) );



/**
 * Show Portfolio Category Menu
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_show_portfolio_category_menu',
	'label'    			=> esc_attr__( 'Show Portfolio Category Menu?', 'enso' ),
	'tooltip'	 		=> esc_attr__( 'Whether to show the portfolio category menu that displays on the portfolio All Works template.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'switch',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '1',
	'choices' 			=> array(
								'on' 	=> esc_attr__( 'Yes', 'enso' ),
								'off' 	=> esc_attr__( 'No', 'enso' ),
							),
) );



/**
 * Portfolio Category Menu Text Style
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_category_menu_text_style',
	'label'    			=> esc_attr__( 'Portfolio Category Menu Text Style', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> 'none',
	'multiple'			=> 1,
	'choices'     		=> array(
								'none'		=> esc_attr__( 'Normal', 'enso' ),
								'uppercase' => esc_attr__( 'Uppercase', 'enso' ),
								'lowercase' => esc_attr__( 'Lowercase', 'enso' ),
							),
	'output' 			=> array(
								array(
									'element' => '.portfolio-categories',
									'property' => 'text-transform',
									'suffix'  => '',
								),
							),
) );



/**
 * Portfolio Category Menu Size
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_category_menu_text_size',
	'label'    			=> esc_attr__( 'Portfolio Category Menu Size', 'enso' ),
	'tooltip'			=> esc_attr__( 'It will be applied to the category menu on the portfolio All Works template. You can use either em or px unit here.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'dimension',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '18px',
	'output' 			=> array(
								array(
									'element' => '.portfolio-categories, .active-portfolio-category-title',
									'property' => 'font-size',
									'suffix'  => '',
								),
							),
) );



/**
 * Portfolio Category Menu Letter Spacing
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_portfolio_category_menu_letter_spacing',
	'label'    			=> esc_attr__( 'Portfolio Category Menu Letter Spacing (px)', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'number',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> '0.5',
	'choices'     		=> array(
								'min'  => 0,
								'step' => 0.1,
							),
	'output' 			=> array(
								array(
									'element' => '.portfolio-categories, .active-portfolio-category-title',
									'property' => 'letter-spacing',
									'suffix'  => 'px',
								),
							),
) );



/**
 * "All" Menu Text
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_portfolio_styles_all_works_everything_menu_text',
	'label'    			=> esc_attr__( '"All" Menu Text', 'enso' ),
	'tooltip'	 		=> esc_attr__( 'This category menu automatically links to a page that is using the All Works template. You can use this option to change its text.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'enso_ctmzr_portfolio_styles',
	'default'  			=> esc_html__( 'All', 'enso' ),
) );