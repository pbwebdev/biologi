<?php
/**
 * Customizer: Other Styles
 *
 * @since 1.0.0
 */

Kirki::add_section( 'enso_ctmzr_other_styles', array(
	'title'      => esc_attr__( 'Other Styles', 'enso' ),
	'priority'   => 30,
	'capability' => 'edit_theme_options',
) );



/**
 * Lightbox Overlay Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_other_styles_lightbox_overlay_color',
	'label'    			=> esc_attr__( 'Image Lightbox Overlay Color', 'enso' ),
	'tooltip' 			=> esc_attr__( 'A lightbox is what you see when clicking on images on the portfolio single page.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_other_styles',
	'default'  			=> 'rgba(255,255,255,1)',
	'alpha'				=> true,
	'transport' 		=> 'postMessage',
	'output' 			=> array(
								array(
									'element' => '.fancybox-overlay',
									'property' => 'background',
									'suffix'  => ' !important',
								),
							),
	'js_vars'   		=> array(
								array(
									'element'  => '.fancybox-overlay',
									'function' => 'css',
									'property' => 'background',
									'suffix'  => ' !important',
								),
							),
) );



/**
 * Lightbox Caption Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_other_styles_lightbox_caption_color',
	'label'    			=> esc_attr__( 'Image Lightbox Text Color', 'enso' ),
	'tooltip' 			=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_other_styles',
	'default'  			=> '#050505',
	'alpha'				=> true,
	'transport' 		=> 'postMessage',
	'output' 			=> array(
								array(
									'element' => '.fancybox-title-outside-wrap, .fancybox-next span::after, .fancybox-prev span::after',
									'property' => 'color',
									'suffix'  => '',
								),
							),
	'js_vars'   		=> array(
								array(
									'element'  => '.fancybox-title-outside-wrap, .fancybox-next span::after, .fancybox-prev span::after',
									'function' => 'css',
									'property' => 'color',
									'suffix'  => '',
								),
							),
) );



// Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_other_styles_separator1',
	'section'     => 'enso_ctmzr_other_styles',
	'description' => '<br/><br/>',
) );



/**
 * Show Search Button
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_other_styles_show_search_button',
	'label'    			=> esc_attr__( 'Show Search Button?', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'switch',
	'section'  			=> 'enso_ctmzr_other_styles',
	'default'  			=> '0',
	'choices' 			=> array(
								'on' 	=> esc_attr__( 'Yes', 'enso' ),
								'off' 	=> esc_attr__( 'No', 'enso' ),
							),
) );



/**
 * Search Button Location
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_other_styles_search_button_location',
	'label'    			=> esc_attr__( 'Search Button Location', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'select',
	'section'  			=> 'enso_ctmzr_other_styles',
	'default'  			=> 'bottom',
	'multiple'			=> 1,
	'choices'     		=> array(
								'bottom'  => esc_attr__( 'Bottom', 'enso' ),
								'top'  => esc_attr__( 'Top', 'enso' ),
							),
) );



/**
 * Search Button Icon Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_other_styles_search_icon_color',
	'label'    			=> esc_attr__( 'Search Button Icon Color', 'enso' ),
	'tooltip' 			=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_other_styles',
	'default'  			=> '#050505',
	'alpha'				=> true,
	'transport' 		=> 'postMessage',
	'output' 			=> array(
								array(
									'element' => '.search-icon-button',
									'property' => 'color',
									'suffix'  => '',
								),
							),
	'js_vars'   		=> array(
								array(
									'element'  => '.search-icon-button',
									'function' => 'css',
									'property' => 'color',
									'suffix'  => '',
								),
							),
) );



/**
 * Search Button Background Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_other_styles_search_bg_color',
	'label'    			=> esc_attr__( 'Search Button Background Color', 'enso' ),
	'tooltip' 			=> '',
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_other_styles',
	'default'  			=> '#e1e1e1',
	'alpha'				=> true,
	'transport' 		=> 'postMessage',
	'output' 			=> array(
								array(
									'element' => '.search-icon-button',
									'property' => 'background',
									'suffix'  => '',
								),
							),
	'js_vars'   		=> array(
								array(
									'element'  => '.search-icon-button',
									'function' => 'css',
									'property' => 'background',
									'suffix'  => '',
								),
							),
) );



/**
 * Search Overlay Color
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_other_styles_search_overlay_color',
	'label'    			=> esc_attr__( 'Search Overlay Color', 'enso' ),
	'tooltip' 			=> esc_attr__( 'Adjust the color of the search overlay that you see when clicking on the search button.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'color',
	'section'  			=> 'enso_ctmzr_other_styles',
	'default'  			=> 'rgba(255,255,255,1)',
	'alpha'				=> true,
	'transport' 		=> 'postMessage',
	'output' 			=> array(
								array(
									'element' => '#search-panel-wrapper',
									'property' => 'background',
									'suffix'  => '',
								),
							),
	'js_vars'   		=> array(
								array(
									'element'  => '#search-panel-wrapper',
									'function' => 'css',
									'property' => 'background',
									'suffix'  => '',
								),
							),
) );