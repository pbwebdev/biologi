<?php
/**
 * Customizer: General Options
 *
 * @since 1.0.0
 */

Kirki::add_section( 'enso_ctmzr_general_options', array(
	'title'      => esc_attr__( 'General Options', 'enso' ),
	'priority'   => 35,
	'capability' => 'edit_theme_options',
) );



/**
 * Search Placeholder Text
 *
 * @since 1.0.2
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_general_options_search_placeholder_text',
	'label'    			=> esc_attr__( 'Search Placeholder Text', 'enso' ),
	'tooltip'	 		=> '',
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'enso_ctmzr_general_options',
	'default'  			=> esc_attr__( 'Type and hit enter', 'enso' ),
) );




/**
 * Number of Posts on Search Results
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_general_options_number_of_posts_search_results',
	'label'    			=> esc_attr__( 'Number of Posts on Search Results', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'number',
	'section'  			=> 'enso_ctmzr_general_options',
	'default'  			=> 10,
	'choices'     		=> array(
								'min'  => 1,
								'step' => 1,
							),
) );



/**
 * Enable Lightbox on WP Images
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_general_options_enable_lightbox_wp_images',
	'label'    			=> esc_attr__( 'Enable Lightbox on WP Images and Gallery?', 'enso' ),
	'description' 		=> esc_attr__( 'To make it works, also make sure that you set the "Link To" option to "Media File" when adding WP image/gallery.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'switch',
	'section'  			=> 'enso_ctmzr_general_options',
	'default'  			=> '1',
	'choices' 			=> array(
								'on' 	=> esc_attr__( 'Yes', 'enso' ),
								'off' 	=> esc_attr__( 'No', 'enso' ),
							),
) );



// Separator
Kirki::add_field( 'uxbarn_enso', array(
	'type'        => 'custom',
	'settings'    => 'enso_ctmzr_general_options_separator2',
	'section'     => 'enso_ctmzr_general_options',
	'default'     => '<br /><br/>',
) );



/**
 * Enable Blog Comment Section
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_general_options_enable_blog_comments',
	'label'    			=> esc_attr__( 'Enable Comment Section on Blog Single Pages?', 'enso' ),
	'tooltip'	 		=> esc_attr__( 'Whether to show or hide the entire comment section on blog single pages.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'switch',
	'section'  			=> 'enso_ctmzr_general_options',
	'default'  			=> '1',
	'choices' 			=> array(
								'on' 	=> esc_attr__( 'Yes', 'enso' ),
								'off' 	=> esc_attr__( 'No', 'enso' ),
							),
) );



/**
 * Enable Next/Previous Post Navigation
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_general_options_enable_post_navigation',
	'label'    			=> esc_attr__( 'Enable Next/Previous Post Navigation?', 'enso' ),
	'tooltip'	 		=> esc_attr__( 'Whether to show or hide the post navigation at the bottom of each blog post on its single page.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'switch',
	'section'  			=> 'enso_ctmzr_general_options',
	'default'  			=> '0',
	'choices' 			=> array(
								'on' 	=> esc_attr__( 'Yes', 'enso' ),
								'off' 	=> esc_attr__( 'No', 'enso' ),
							),
) );



/**
 * Blog Excerpt Length
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_general_options_blog_excerpt_length',
	'label'    			=> esc_attr__( 'Blog Excerpt Length', 'enso' ),
	'tooltip' 			=> esc_attr__( 'Set the number of character for the excerpt that displays on the blog list page.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'number',
	'section'  			=> 'enso_ctmzr_general_options',
	'default'  			=> 220,
	'choices'     		=> array(
								'min'  => 1,
								//'max'  => 200,
								'step' => 1,
							),
) );



/**
 * Blog Excerpt Length
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_general_options_blog_excerpt_length',
	'label'    			=> esc_attr__( 'Blog Excerpt Length', 'enso' ),
	'tooltip' 			=> esc_attr__( 'Set the number of character for the excerpt that displays on the blog list page.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'number',
	'section'  			=> 'enso_ctmzr_general_options',
	'default'  			=> 400,
	'choices'     		=> array(
								'min'  => 1,
								'step' => 1,
							),
) );



/**
 * Blog Menu Text for Displaying Active Status
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_general_options_blog_menu_text_active_status',
	'label'    			=> esc_attr__( 'Blog Menu Text for Displaying Active Status', 'enso' ),
	'tooltip'	 		=> esc_attr__( 'By default, when viewing a blog single page, WP will not set the active status on the blog menu. Use this option to tell WP what text on the menu to be set as active for most blog-related pages.', 'enso' ),
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'enso_ctmzr_general_options',
	'default'  			=> esc_attr__( 'Blog', 'enso' ),
) );
