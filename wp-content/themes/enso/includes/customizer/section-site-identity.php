<?php
/**
 * Customizer: Site Identity Section
 *
 * @since 1.0.0
 */



/**
 * Copyright Text
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_site_identity_copyright_text',
	'label'    			=> esc_attr__( 'Copyright Text', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'textarea',
	'section'  			=> 'title_tagline',
	'priority' 			=> 10,
	'default'  			=> sprintf( __( '&copy; Enso Theme<br/>Designed by <a href="%s">UXBARN</a>', 'enso' ), 'https://uxbarn.com' ),
	'sanitize_callback' => 'enso_ctmzr_sanitize_with_theme_wpkes',
	'transport' 		=> 'postMessage',
	'js_vars'   		=> array(
								array(
									'element'  => '.copyright',
									'function' => 'html',
								),
							),
) );



/**
 * Social Icon Set
 *
 * @since 1.0.0
 */
Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_site_identity_facebook_url',
	'label'    			=> esc_attr__( 'Facebook URL', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'title_tagline',
	'default'  			=> '',
	'sanitize_callback' => 'esc_url_raw',
) );

Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_site_identity_twitter_url',
	'label'    			=> esc_attr__( 'Twitter URL', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'title_tagline',
	'default'  			=> '',
	'sanitize_callback' => 'esc_url_raw',
) );

Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_site_identity_google_plus_url',
	'label'    			=> esc_attr__( 'Google+ URL', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'title_tagline',
	'default'  			=> '',
	'sanitize_callback' => 'esc_url_raw',
) );

Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_site_identity_instagram_url',
	'label'    			=> esc_attr__( 'Instagram URL', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'title_tagline',
	'default'  			=> '',
	'sanitize_callback' => 'esc_url_raw',
) );

Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_site_identity_flickr_url',
	'label'    			=> esc_attr__( 'Flickr URL', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'title_tagline',
	'default'  			=> '',
	'sanitize_callback' => 'esc_url_raw',
) );

Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_site_identity_500px_url',
	'label'    			=> esc_attr__( '500px URL', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'title_tagline',
	'default'  			=> '',
	'sanitize_callback' => 'esc_url_raw',
) );

Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_site_identity_pinterest_url',
	'label'    			=> esc_attr__( 'Pinterest URL', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'title_tagline',
	'default'  			=> '',
	'sanitize_callback' => 'esc_url_raw',
) );

Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_site_identity_linkedin_url',
	'label'    			=> esc_attr__( 'LinkedIn URL', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'title_tagline',
	'default'  			=> '',
	'sanitize_callback' => 'esc_url_raw',
) );

Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_site_identity_dribbble_url',
	'label'    			=> esc_attr__( 'Dribbble URL', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'title_tagline',
	'default'  			=> '',
	'sanitize_callback' => 'esc_url_raw',
) );

Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_site_identity_behance_url',
	'label'    			=> esc_attr__( 'Behance URL', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'title_tagline',
	'default'  			=> '',
	'sanitize_callback' => 'esc_url_raw',
) );

Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_site_identity_vimeo_url',
	'label'    			=> esc_attr__( 'Vimeo URL', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'title_tagline',
	'default'  			=> '',
	'sanitize_callback' => 'esc_url_raw',
) );

Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_site_identity_youtube_url',
	'label'    			=> esc_attr__( 'YouTube URL', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'title_tagline',
	'default'  			=> '',
	'sanitize_callback' => 'esc_url_raw',
) );

Kirki::add_field( 'uxbarn_enso', array(
	'settings' 			=> 'enso_ctmzr_site_identity_soundcloud_url',
	'label'    			=> esc_attr__( 'SoundCloud URL', 'enso' ),
	'description' 		=> '',
	'help'        		=> '',
	'type'     			=> 'text',
	'section'  			=> 'title_tagline',
	'default'  			=> '',
	'sanitize_callback' => 'esc_url_raw',
) );