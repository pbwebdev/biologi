<?php
/**
 * Meta boxes for posts and pages
 *
 * @since 1.0.0
 */



if ( ! function_exists( 'enso_create_shared_meta_boxes' ) ) {
	
	function enso_create_shared_meta_boxes( $current_screen ) {
		
		enso_create_intro_meta_box();
		
	}
	
}



if ( ! function_exists( 'enso_create_intro_meta_box' ) ) {

	function enso_create_intro_meta_box() {
		
		$args = array(
			'id'          => 'enso_intro_meta_box',
			'title'       => esc_html__( 'Intro Settings', 'enso' ),
			'desc'        => '',
			'pages'       => array( 'post', 'page', 'uxbarn_portfolio' ),
			'context'     => 'normal',
			'priority'    => 'default',
			'fields'      => array(
							  array(
								'id'          => 'enso_section_intro',
								'label'       => esc_html__( 'Intro', 'enso' ),
								'desc'        => esc_html__( 'The intro will be displayed right below the title on post single pages.', 'enso' ),
								'std'         => '',
								'type'        => 'textarea-simple',
								'section'     => 'enso_sec_intro',
								'rows'        => '',
								'post_type'   => '',
								'taxonomy'    => '',
								'class'       => '',
							  ),
							),
		);
		
		ot_register_meta_box( $args );
		
	}

}