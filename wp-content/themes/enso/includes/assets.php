<?php
/**
 * Collection of the functions for loading JS and CSS in the theme
 *
 * @since 1.0.0
 */



if ( ! function_exists( 'enso_load_theme_assets' ) ) {
	
	function enso_load_theme_assets() {
		
		enso_load_theme_css();
		enso_load_theme_js();
		
	}
	
}



if ( ! function_exists( 'enso_load_theme_css' ) ) {
	
	function enso_load_theme_css() {
		
		// Enqueue all CSS
		wp_enqueue_style( 'enso-google-fonts', enso_get_google_fonts_url(), array(), null );
		wp_enqueue_style( 'ionicon', get_template_directory_uri() . '/css/ionicons.min.css', array(), null );
		wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), null );
		wp_enqueue_style( 'jquery-mmenu', get_template_directory_uri() . '/css/jquery.mmenu.all.css', array(), null );
		wp_enqueue_style( 'jquery-fancybox', get_template_directory_uri() . '/css/jquery.fancybox.css', array(), null );
		wp_enqueue_style( 'jquery-fancybox-helpers-thumbs', get_template_directory_uri() . '/css/fancybox/helpers/jquery.fancybox-thumbs.css', array(), null );
		wp_enqueue_style( 'jquery-fancybox-helpers-buttons', get_template_directory_uri() . '/css/fancybox/helpers/jquery.fancybox-buttons.css', array(), null );
		wp_enqueue_style( 'enso-theme', get_template_directory_uri() . '/style.css', array(), enso_get_theme_version() );
		
	}
	
}



if ( ! function_exists( 'enso_load_theme_js' ) ) {
	
	function enso_load_theme_js() {
		
		wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr-custom.js', array( 'jquery' ), null );
		wp_enqueue_script( 'superfish', get_template_directory_uri() . '/js/superfish.min.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'jquery-fitvids', get_template_directory_uri() . '/js/jquery.fitvids.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'jquery-mmenu', get_template_directory_uri() . '/js/jquery.mmenu.all.min.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'jquery-flex-images', get_template_directory_uri() . '/js/jquery.flex-images.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'jquery-fancybox', get_template_directory_uri() . '/js/jquery.fancybox.pack.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'jquery-fancybox-helpers-thumbs', get_template_directory_uri() . '/js/fancybox-helpers/jquery.fancybox-thumbs.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'jquery-fancybox-helpers-buttons', get_template_directory_uri() . '/js/fancybox-helpers/jquery.fancybox-buttons.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'enso-theme', get_template_directory_uri() . '/js/enso.js', array( 'jquery' ), enso_get_theme_version(), true );
		
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
		
		$params = array(
					'portfolio_item_spacing'			=> intval( get_theme_mod( 'enso_ctmzr_portfolio_styles_portfolio_item_spacing', 70 ) ),
					'justified_images_row_height'		=> intval( get_theme_mod( 'enso_ctmzr_portfolio_styles_portfolio_max_row_height', 500 ) ),
					'portfolio_hover_effect'			=> get_theme_mod( 'enso_ctmzr_portfolio_styles_portfolio_hover_effect', 'blur' ),
					'modal_search_input_text'			=> get_theme_mod( 'enso_ctmzr_general_options_search_placeholder_text', esc_html__( 'Type and hit enter', 'enso' ) ),
					'show_search_button'				=> get_theme_mod( 'enso_ctmzr_general_options_show_search_button', false ),
					'enable_lightbox_wp_gallery' 		=> get_theme_mod( 'enso_ctmzr_general_options_enable_lightbox_wp_images', true ),
				);
		
		wp_localize_script( 'enso-theme', 'ThemeOptions', $params );
		
	}
	
}



if ( ! function_exists( 'enso_load_admin_assets' ) ) {
	
	function enso_load_admin_assets( $page ) {
		
		wp_enqueue_style( 'enso-google-fonts', enso_get_google_fonts_url(), array(), null );
		wp_enqueue_style( 'ionicon', get_template_directory_uri() . '/css/ionicons.min.css', array(), null );
		
		if ( 'post.php' === $page || 'post-new.php' === $page ) {
			wp_enqueue_style( 'enso-admin', get_template_directory_uri() . '/css/admin.css', array(), null );
		}
		
	}
	
}



if ( ! function_exists( 'enso_load_customizer_assets' ) ) {
	
	function enso_load_customizer_assets() {
		
		wp_enqueue_style( 'enso-customizer', get_template_directory_uri() . '/css/customizer.css', array(), null );
		
	}
	
}