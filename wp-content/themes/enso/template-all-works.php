<?php 
/**
 * Template Name: All Works with Category Menu
 * Description: This template is for showing all of your works with category menu
 * 
 * @since 1.0.0
 */

get_header(); ?>

<?php
	
	if ( function_exists( 'uxb_port_init_plugin' ) ) {
		
		$page_content_location = get_theme_mod( 'enso_ctmzr_portfolio_styles_all_works_show_page_title_content', 'none' );
		
		// Whether to display page title and content on this template (top location)
		if ( 'top' === $page_content_location ) {
			get_template_part( 'template-parts/portfolio-page-content' );
		}
		
		echo '<section class="portfolio-list-section">';
			
			if ( get_theme_mod( 'enso_ctmzr_portfolio_styles_show_portfolio_category_menu', true ) ) {
				
				// Display portfolio category menus
				get_template_part( 'template-parts/portfolio-category-menu' );
				
			}
			
			// Display portfolio items
			get_template_part( 'template-parts/portfolio-listing' );
			
		echo '</section>';
		
		// Whether to display page title and content on this template (bottom location)
		if ( 'bottom' === $page_content_location ) {
			get_template_part( 'template-parts/portfolio-page-content' );
		}
		
	}
	
?>
	
<?php get_footer(); ?>